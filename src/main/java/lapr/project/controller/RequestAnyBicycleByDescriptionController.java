package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;

import java.util.Date;

public class RequestAnyBicycleByDescriptionController {

    private ParkDB parkDB;
    private BicycleDB bicycleDB;
    private UserDB userDB;
    private RequisitionDB requisitionDB;
    private static final int ERROR = -1;

    /**
     * Public RequestAnyBicycleByDescriptionController constructor
     */
    public RequestAnyBicycleByDescriptionController(){
        this.parkDB=new ParkDB();
        this.userDB=new UserDB();
        this.requisitionDB = new RequisitionDB();
        this.bicycleDB = new BicycleDB();
    }

    /**
     * Method to requesty any bicycle by description.
     *
     * @param bicycleDescription
     * @param username
     * @return long number with the requisition time in seconds
     */
    public long requestyAnyBicycleByDescription(String bicycleDescription, String username){
        Bicycle b = getBicycleDB().getBicycleByDescription(bicycleDescription);
        User u = getUserDB().getUserByUsername(username);
        Park p = getParkDB().getPark(b.getParkID());
        return isBicycleAvailable(b) ? addRequisition(u.getUserId(), b, p).getTime() : ERROR;
    }

    /**
     * Private ParkDB getter
     * @return connection of ParkDB
     */
    private ParkDB getParkDB(){
        return parkDB;
    }

    /**
     * Private BicycleDB getter
     * @return connection of BicycleDB
     */
    private BicycleDB getBicycleDB(){
        return bicycleDB;
    }

    /**
     *
     * @param b Bicycle to check availability.
     * @return true or false
     */
    private boolean isBicycleAvailable(Bicycle b){
        return b.isAvaliability();
    }

    /**
     *  Private UserDB getter
     * @return connection of UserDB
     */
    private UserDB getUserDB(){
        return this.userDB;
    }

    /**
     * Private RequisitionDB getter
     * @return connection of RequisitionDB
     */
    private RequisitionDB getRequisitionDb(){
        return this.requisitionDB;
    }

    /**
     * AddRequisition for any bicycle by description without destination park.
     *
     * @param userID
     * @param bicycle
     * @param pickUpPark
     * @return
     */
    private Date addRequisition(int userID, Bicycle bicycle, Park pickUpPark){
        Date today = new Date();
        getRequisitionDb().addRequisition(new Requisition(userID,bicycle,pickUpPark,pickUpPark, today));
        return today;
    }
}
