
package lapr.project.controller;

import lapr.project.data.InvoiceDB;
import lapr.project.data.UserDB;
import lapr.project.model.Invoice;


public class InvoiceIssuingController {
    private InvoiceDB invoiceDb;
    private Invoice invoice;
    private UserDB userDb;
    
    public InvoiceIssuingController(){
        this.invoiceDb= new InvoiceDB();
        this.userDb= new UserDB();
    }
    
    private InvoiceDB getInvoiceDB(){
        return invoiceDb;
    }
    
    public double getSumMonthlyPayments(int month, int userId){
        double sum=getInvoiceDB().getMonthlyPayments(month,userId);
        return sum;
    }
    
    public Invoice addInvoice(double total, int userId){
        double discount=getDiscountAvaliable(userId);
        this.invoice = new Invoice(total, discount, userId);
        return getInvoiceDB().addInvoice(total, discount,userId);
    } 
    
    
      /**
     * UserDB getter
     *
     * @return
     */
    private UserDB getUserDb() {
        return userDb;
    }
    
     /**
     * Get discount avaliable by user ID
     *
     * @param userId User Id
     * @return discount Avaliable for User ID
     */
    public double getDiscountAvaliable(int userId){
        int discountAvaliable;
        int userPoints = getUserDb().getUserPoints(userId);
        discountAvaliable =  userPoints / 10;
        return (double) discountAvaliable;
    }
    
    /**
     * Get User points by user ID
     *
     * @param userId User Id
     * @return points Avaliable for User ID
     */
    public int getUserPoints(int userId) {
        return getUserDb().getUserPoints(userId);
    }
    
     /**
     * Update User points by user ID
     *
     * @param userId User Id
     * @param userPoints User points
     * @return points Avaliable for User ID
     */
    public boolean updateUserPoints(int userId, int userPoints){
        return getUserDb().updateUserPoints(userId,userPoints);
    }
}
