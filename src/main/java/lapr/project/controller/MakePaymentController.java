/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.sql.Date;
import lapr.project.data.PaymentDB;
import lapr.project.model.Payment;


public class MakePaymentController {

    private Payment payment; 
    private PaymentDB makePaymentDB;

    /**
     * MakePaymentController constructor
     */
    public MakePaymentController(){
            this.makePaymentDB =  makePaymentDB;      
    }
   
    /**
     * Method that creates and adds a new payment
     * 
     * @param requisitionId
     * @param userId
     * @param serviceTypeId
     * @param invoiceId
     * @param date
     * @return 
     */
    public Payment makePayment( int requisitionId, int userId, int serviceTypeId, int invoiceId, Date date ){
        payment = new Payment(userId, serviceTypeId, invoiceId, date);
        return makePaymentDB.addPayment(requisitionId, payment); 
    }
}
