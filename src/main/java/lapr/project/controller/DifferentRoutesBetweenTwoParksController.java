package lapr.project.controller;

import lapr.project.data.*;
import lapr.project.graphbase.Graph;
import lapr.project.graphbase.GraphAlgorithms;
import lapr.project.model.*;
import lapr.project.utils.AuxMethods;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class DifferentRoutesBetweenTwoParksController {

    private ParkDB parkDB;
    private PoiDB poiDB;
    private WindDB windDB;
    private UserDB userDB;
    private Graph<Location, String> graph;
    private Park pickUpPark, dropOffPark;
    private List<Location> intermediateLocationArray;
    private int numberOfAlternativeRoutes = 0;
    private int ascendingDescending = 0;

    //Constant to convert cal to Kwh
    private static final double CONVERTION_FACTOR = 860420.65;
    private static final int DISTANCE_EDGE = 1;
    private static final int ENERGY_EDGE = 2;


    public DifferentRoutesBetweenTwoParksController() {
        this.parkDB = new ParkDB();
        this.poiDB = new PoiDB();
        this.windDB = new WindDB();
        this.userDB = new UserDB();
        this.intermediateLocationArray = new ArrayList<>();
    }

    private ParkDB getParkDB() {
        return parkDB;
    }

    private PoiDB getPoiDB() {
        return poiDB;
    }

    private WindDB getWindDB() {
        return windDB;
    }

    private UserDB getUserDB() {
        return userDB;
    }

    public void setIntermediateLocationArray(List<Location> intermediateLocationArray) {
        this.intermediateLocationArray = intermediateLocationArray;
    }

    public Map<Location[], Double> mostEnergeticallyEfficientRoute(User user, int pickUp, int dropOff, Double bikeWeight, Double bikeRatio, int choice) {

        List<Location> shortPath = new LinkedList<>();
        Map<Location[], Double> permutationsWithWeight = new LinkedHashMap<>();
        this.pickUpPark = this.parkDB.getPark(pickUp);
        this.dropOffPark = this.parkDB.getPark(dropOff);


        if (choice == DISTANCE_EDGE || choice == ENERGY_EDGE) {

            graph = createGraph();
            Set<Park> allParks = this.getParkDB().getAllParks();
            Set<Poi> allPois = this.getPoiDB().getAllPoi();

            graph = addVertex(graph, allParks, allPois);

            graph = addEdges(graph, user, bikeWeight, bikeRatio, choice);

            if (intermediateLocationArray.size() != 0) {
                Location[] locs = AuxMethods.listToArray(intermediateLocationArray);
                permutationsWithWeight = AuxMethods.bestRouteWithIntermediatePOI(graph, locs, pickUpPark, dropOffPark);
                permutationsWithWeight = AuxMethods.routeSorting(permutationsWithWeight, this.ascendingDescending, this.numberOfAlternativeRoutes);
            } else {
                Double size = shortestPath(graph, pickUpPark, dropOffPark, shortPath);
                Location[] aux = AuxMethods.listToArray(shortPath);
                permutationsWithWeight.put(aux, size);
            }
        }

        return permutationsWithWeight;
    }

    /**
     * Adds locations to the intermediate POI ArrayList
     *
     * @param poiId POI id
     */
    public void addIntermediatePOI(int poiId) {

        this.intermediateLocationArray.add(getPoiDB().getPoi(poiId));
    }

    /**
     * Adds number of alternative routes selected by user
     *
     * @param number number of alternative routes to display
     */
    public void numberOfRoutes(int number) {
        this.numberOfAlternativeRoutes = number;
    }

    private static double shortestPath(Graph<Location, String> graph, Location pickup, Location dropOff, List<Location> shortPath) {

        return GraphAlgorithms.shortestPath(graph, pickup, dropOff, shortPath);

    }

    /**
     * Adds criteria for route sorting
     *
     * @param criteria ascending or descending
     */
    public void addSortingCriteria(int criteria) {

        this.ascendingDescending = criteria;
    }


    private static Graph<Location, String> createGraph() {

        return new Graph<Location, String>(true);

    }

    private Graph<Location, String> addVertex(Graph<Location, String> graph, Set<Park> allParks, Set<Poi> allPois) {

        for (Park p : allParks) {
            graph.insertVertex(p);
        }

        for (Poi p : allPois) {
            graph.insertVertex(p);
        }

        return graph;

    }

    private Graph<Location, String> addEdges(Graph<Location, String> graph, User user, Double bikeWeight, Double bikeRatio, int choice) {

        int numLoc = graph.numVertices();
        Location lOri;
        Location lDest;
        String pathType = "";
        Double windVel = 0.0;
        Double windFact = 0.0;

        Set<Wind> allWind = this.getWindDB().getAllWind();

        for (int i = 0; i < numLoc; i++) {
            for (int j = 0; j < numLoc; j++) {

                lOri = graph.getVertex(i);
                lDest = graph.getVertex(j);

                for (Wind wind : allWind) {
                    boolean flag = false;
                    int ini = wind.getInitiallocationId();
                    int dest = wind.getFinallocationId();

                    if (lOri.getLocationId() == ini && lDest.getLocationId() == dest) {
                        pathType = wind.getPathDirection();
                        windVel = wind.getWindspeed();
                        windFact = wind.getWindfactor();
                        flag = true;
                    }

                    if ("uni".equalsIgnoreCase(pathType) && flag) {
                        Double energy = AuxMethods.routeSelection(user, lOri, lDest, bikeWeight, bikeRatio, windVel, windFact, choice);
                        graph.insertEdge(lOri, lDest, "EnergySpent", energy);
                    } else if ("bi".equalsIgnoreCase(pathType) && flag) {
                        Double energy = AuxMethods.routeSelection(user, lOri, lDest, bikeWeight, bikeRatio, windVel, windFact, choice);
                        graph.insertEdge(lOri, lDest, "EnergySpent", energy);
                        energy = AuxMethods.routeSelection(user, lDest, lOri, bikeWeight, bikeRatio, windVel, windFact, choice);
                        graph.insertEdge(lDest, lOri, "EnergySpent", energy);
                    }

                }
            }
        }

        return graph;
    }

    public double calculateEletricalEnergyToTravelFromOneLocationToAnother(
            double originLatitudeInDegrees,
            double originLongitudeInDegrees,
            double destinationLatitudeInDegrees,
            double destinationLongitudeInDegrees,
            String username) {

        User usr = getUserDB().getUserByUsername(username);
        int park1 = getParkDB().getParkByCoordinates(originLatitudeInDegrees,originLongitudeInDegrees);
        int park2 = getParkDB().getParkByCoordinates(destinationLatitudeInDegrees,destinationLongitudeInDegrees);


        Map<Location[],Double> map = mostEnergeticallyEfficientRoute(usr,park1,park2,15d,0.9,2);
        double necessaryEnergy = map.values().iterator().next() / CONVERTION_FACTOR;

        BigDecimal bd = new BigDecimal(Double.toString(necessaryEnergy));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}


