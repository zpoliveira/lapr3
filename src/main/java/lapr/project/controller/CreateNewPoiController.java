package lapr.project.controller;

import lapr.project.data.PoiDB;
import lapr.project.model.Poi;
import lapr.project.utils.ImportFiles;

/**
 *
 * @author Bruno Patrão <1151052@isep.ipp.pt>
 */
public class CreateNewPoiController {
    
    private Poi poi;
    private PoiDB PoiDB;
    private ImportFiles importFiles;

    /**
     * Controller constructor
     */
    public CreateNewPoiController() {
        this.PoiDB = new PoiDB();
        this.importFiles = new ImportFiles();
    }

    /**
     * returns PoiDB
     *
     * @return PoiDB
     */
    private PoiDB getPoiDb() {
        return PoiDB;
    }

    /**
     * Sets Poi data and add to DB
     *
     * @param name      Poi name
     * @param latitude  Poi latitude
     * @param longitude Poi longitude
     * @param altitude  Poi altitude
     */
    public Poi setData(String name, double latitude, double longitude, double altitude) {
        this.poi = new Poi(name, latitude, longitude, altitude);
        return getPoiDb().addPoi(poi);
    }

    /**
     *
     * @param fileName
     * @return count of number of pois added.
     */
    public int importPois(String fileName){
        return importFiles.importPois(fileName);
    }

}
