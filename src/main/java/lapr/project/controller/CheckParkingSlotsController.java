/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;

/**
 *
 * @author Ferreira
 */
public class CheckParkingSlotsController {

    private ParkDB pDB;
    private Park park;

    /**
     * CheckParkingSlotsController constructor
     */
    public CheckParkingSlotsController(){
            this.pDB = new ParkDB();
    }

    public ParkDB getParkDB(){
        return this.pDB;
    }

    /**
     * Get park by its coordinates
     * @param latitude
     * @param longitude
     * @return
     */
    private Park getPark(double latitude, double longitude){
        int parkId = getParkDB().getParkByCoordinates(latitude, longitude);
        return getParkDB().getPark(parkId);
    }

    /**
     *  Check the number of free parking places at a given park for the loaned
     *  bicycle.
     * @param parkLatitudeInDegrees
     * @param parkLongitudeInDegrees
     * @param username
     * @return
     */
    public int checkFreeSlotsAtPark(double parkLatitudeInDegrees, double parkLongitudeInDegrees, String username){

        int bicType = getParkDB().avaliableSlotsInPark(username);
        this.park = getPark(parkLatitudeInDegrees, parkLongitudeInDegrees);
        return bicType==1?park.getAvailableElectricalSlots():park.getAvailableNonElectricalSlots();

    }

}
