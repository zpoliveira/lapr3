package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.ImportFiles;

public class CreateNewParkController {


    private Park park;
    private ParkDB parkDB;
    private ImportFiles importFiles;

    public CreateNewParkController(){
        this.parkDB = new ParkDB();
        this.importFiles= new ImportFiles();
    }

    /**
     * returns parkDB
     *
     * @return parkDB
     */
    private ParkDB getParkDb() {
        return parkDB;
    }

    /**
     * Sets Park data and add to DB
     *
     * @param name      Park name
     * @param latitude  Park latitude
     * @param longitude Park longitude
     * @param altitude  Park altitude
     */
    public Park setData(String name, double latitude, double longitude, double altitude,
                        int maxElectricalLotation, int maxNonElectricalLotation, double inputVoltage, double inputCurrent) {

        this.park = new Park(name, latitude, longitude, altitude, maxElectricalLotation, maxNonElectricalLotation,
                inputVoltage, inputCurrent);

        return getParkDb().addPark(park);
    }

    /**
     * Add Parks by file
     * @param inputParksFile
     * @return number of Parks Successfully imported
     */
    public int importParks (String inputParksFile){
        return importFiles.importParks(inputParksFile);
    }
}
