package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;


public class CheckDistandBetweenParksController {

    private ParkDB parkDB;

    /**
     * CheckDistandBetweenParksController constructor
     */
    public CheckDistandBetweenParksController(){
        this.parkDB = new ParkDB();
    }

    /**
     * Get Park by park Id
     * @param parkId park Id
     * @return Park object
     */
    private Park getPark(int parkId){
        return this.parkDB.getPark(parkId);
    }

    public double getDistanceBetweenParks(int parkId1, int parkId2){
        return getDistanceBetweenParks(getPark(parkId1), getPark(parkId2));
    }

    /**
     * Calls method that calculates distance between a given point and a Park
     * @param parkId1 origin Park
     * @param parkId2 destiny Park
     * @return distance in meters
     */
    private double getDistanceBetweenParks(Park parkId1, Park parkId2){
        return AuxMethods
                .getDistanceWithHeight(parkId1.getLatitude(), parkId1.getLongitude(), parkId1.getAltitude(),parkId2);
    }

    public int getDistanceFromLocationToAnother (double latitudeA, double longitudeA, double latitudeB, double longitudeB){

        return (int)(AuxMethods.getDistance(latitudeA,longitudeA,latitudeB,longitudeB) * 1000);
    }
}
