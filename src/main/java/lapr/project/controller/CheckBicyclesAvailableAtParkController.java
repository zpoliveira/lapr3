/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.HashSet;
import java.util.Set;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.User;
import lapr.project.utils.AuxMethods;


public class CheckBicyclesAvailableAtParkController {
    
        private ParkDB parkDB;
        private BicycleDB bicycleDB;
        private UserDB userDB;

    /**
     * Controller constructor
     */
    public CheckBicyclesAvailableAtParkController() {
        this.parkDB = new ParkDB();
        this.bicycleDB = new BicycleDB();
        this.userDB = new UserDB();
    }
    
     /**
     * returns ParkDB
     * @return ParkDB
     */
    private ParkDB getParkDb() {
        return parkDB;
    }

    /**
     * returns BicycleDB
     * @return BicycleDB
     */
    private BicycleDB getBicycleDB() {
        return bicycleDB;
    }

    /**
     * returns UserDB
     * @return UserDB
     */
    private UserDB getUserDB() {
        return userDB;
    }


    /**
     *
     * @return AllParks
     */
    public Set <Park> getAllParks() {
        return getParkDb().getAllParks();
    }

    /**
     * Get Available Bicycles at Park by its Coordinates
     * @param latitude
     * @param longitude
     * @return
     */
    public Set<Bicycle> getAllAvailableBicyclesAtParkByCoordinates(double latitude, double longitude){
        int parkId = getParkDb().getParkByCoordinates(latitude, longitude);
        return getAllAvailableBicyclesAtPark(parkId);
    }

    /**
     * 
     * @param parkId
     * @return Available Bicycles at park ID
     */
    public Set<Bicycle> getAllAvailableBicyclesAtPark(int parkId){
        Set<Bicycle> AllAvailableBicycles = getBicycleDB().getAllAvailableBicycles();
        Set<Bicycle> AllAvailableBicyclesAtPark = new HashSet<>();
            for (Bicycle next : AllAvailableBicycles) {
                if(next.getParkID() == parkId){
                    AllAvailableBicyclesAtPark.add(next);
                }
            }
        return AllAvailableBicyclesAtPark;
    }

    /**
     * Get a Bicycle by its description
     * @param description
     * @return
     */
    private Bicycle getBicycleByDescription(String description){

        Set<Bicycle> allBicycle = getBicycleDB().getAllBicycles();
        for(Bicycle b : allBicycle){
            if(b.getBicycleDescription().equals(description)){
                return b;
            }
        }
        return null;
    }

    /**
     * Check for how long a given Bicycle was unlocked
     * @param bicycleDescription Bicycle description
     * @return time in seconds
     */
    public long getUnlockedBicTime(String bicycleDescription){
        Bicycle bic = getBicycleByDescription(bicycleDescription);
        if (bic!=null){
            double time = getBicycleDB().getBicycleTimeUnlocked(bic.getBicycleID());
            return (long)AuxMethods.convertDaysToSeconds(time);
        }
        return 0;
    }

    public Set<Bicycle> suggestEletricalBicyclesToGoFromOneParkToAnother (double originParkLatitudeInDegrees,
                                                                 double originParkLongitudeInDegrees,
                                                                 double destinationParkLatitudeInDegrees,
                                                                 double destinationParkLongitudeInDegrees,
                                                                 String username,
                                                                 String outputFileName){

        User user = getUserDB().getUserByUsername(username);
        int p1 = getParkDb().getParkByCoordinates(originParkLatitudeInDegrees,originParkLongitudeInDegrees);
        int p2 = getParkDb().getParkByCoordinates(destinationParkLatitudeInDegrees,destinationParkLongitudeInDegrees);

        Park u1 = this.parkDB.getPark(p1);
        Park u2 = this.parkDB.getPark(p2);

        Set<Bicycle> bikesWithPower = this.bicycleDB.getAllAvailableBicyclesWithGivenPower(user,u1,u2,0d,0d);

        return bikesWithPower;
    }
}