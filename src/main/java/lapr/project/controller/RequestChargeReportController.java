package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static lapr.project.utils.Configs.DEBUG_MODE;

public class RequestChargeReportController {

    private BicycleDB bicycleDB;

    /**
     * Public RequestChargeReportController constructor
     */
    public RequestChargeReportController(){
        this.bicycleDB = new BicycleDB();
    }

    public Map<Bicycle, String> requestChargeReport(Park park){
        Set<Bicycle> reportSet = bicycleDB.getAllAvailableBicyclesOrderdByHigherBatteryLevel(park.getParkId());
        return AuxMethods.calculateChargingTime(reportSet, park.getInputCurrent(), park.getInputVoltage());
    }

    public Map<Bicycle, String> requestBicycleThatAreChargingReport(Map<Bicycle, String> parkChargingReport){

        Map<Bicycle, String> newMap = new TreeMap<>();

        for(Map.Entry<Bicycle,String> en : parkChargingReport.entrySet()){

            if(!"0:0:0".equalsIgnoreCase(en.getValue())){
                newMap.put(en.getKey(),en.getValue());
            }
        }
        return newMap;
    }

}
