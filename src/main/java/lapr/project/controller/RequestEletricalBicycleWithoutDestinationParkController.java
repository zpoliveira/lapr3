
package lapr.project.controller;

import java.util.Date;
import java.util.Set;

import lapr.project.data.BicycleDB;
import lapr.project.data.RequisitionDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;


public class RequestEletricalBicycleWithoutDestinationParkController {

    /**
     *  RequisitionDB connection
     */
    private RequisitionDB requisitionDB;

    /**
     *  BicycleDB connection
     */
    private BicycleDB bicycleDB;

    /**
     * Choosen bicycle to be unlocked.
     */
    private Bicycle bicycle;

    /**
     * Public RequestEletricalBicycleWithoutDestinationParkController constructor
     */
    public RequestEletricalBicycleWithoutDestinationParkController() {
        this.requisitionDB = new RequisitionDB();
        this.bicycleDB = new BicycleDB();
    }

    /**
     * @return RequisitionDB connection
     */
    private RequisitionDB getRequisitionDb(){
        return this.requisitionDB;
    }

    private BicycleDB getBicycleDB(){
        return this.bicycleDB;
    }

     /**
      * setData for a eletrical bicycle requisition without destination park
      * @param userID
      * @param pickUpPark
      * @return choosen unlocked bicycle.
      */
     public Bicycle setData(int userID, Park pickUpPark){
         Set<Bicycle> setBikes = getBicycleDB().getAllAvailableBicyclesOrderdByHigherBatteryLevel(pickUpPark.getParkId());
         bicycle = setBikes.iterator().next();
         getRequisitionDb().addRequisition(new Requisition(userID,bicycle,pickUpPark,pickUpPark, new Date()));
        return bicycle;
    }

}
