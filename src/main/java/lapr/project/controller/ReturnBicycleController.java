package lapr.project.controller;

import javafx.util.Pair;
import lapr.project.data.*;
import lapr.project.model.Bicycle;
import lapr.project.model.User;
import lapr.project.utils.Email;

import java.util.Date;
import java.util.Set;

public class ReturnBicycleController {

    private BicycleDB bicycleDB;
    private ParkDB parkDB;
    private UserDB userDB;
    private int parkId = -1;
    private int bicycleId = -1;

    /**
     * Public ReturnBicycleController constructor
     */
    public ReturnBicycleController(){
        this.bicycleDB = new BicycleDB();
        this.parkDB = new ParkDB();
        this.userDB = new UserDB();
    }

    /**
     * Public complete ReturnBicycleController constructor
     */
    public Pair<Integer, Boolean> returnBicycle(int bicycleId, int parkId){
        int userId = getBicycleDb().returnBicycle(bicycleId, parkId);
        User user = getUserDb().getUser(userId);
        new Email().sendEmail(user);
        Pair<Integer, Boolean> p = new Pair<>(userId, true);
        return p;
    }

    /**
     * Public complete ReturnBicycleController constructor with latitude and longitude
     */
    public long returnBicycle(String bicycleDesc, double latitude, double longitude){
        parkId = getParkDb().getParkByCoordinates(latitude, longitude);
        Set<Bicycle> set = getBicycleDb().getAllBicycles();
        for(Bicycle b : set){
            if(b.getBicycleDescription().equals(bicycleDesc)){
                bicycleId = b.getBicycleID();
            }
        }
        returnBicycle(parkId, bicycleId);
        return bicycleId != -1 ? new Date().getTime() : -1;
    }

    /**
     *
     * @return connection of BicycleDb
     */
    private BicycleDB getBicycleDb() {
        return bicycleDB;
    }

    /**
     *
     * @return connection of BicycleDb
     */
    private ParkDB getParkDb() {
        return parkDB;
    }

    /**
     *
     * @return connection of UserDb
     */
    private UserDB getUserDb() {
        return userDB;
    }

}
