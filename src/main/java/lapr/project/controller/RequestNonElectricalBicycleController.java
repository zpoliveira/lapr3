package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

public class RequestNonElectricalBicycleController {

    private RequisitionDB requisitionDB;
    private UserDB userDb;

    private ParkDB parkDb;

    /**
     * Public RequestNonElectricalBicycleController constructor
     */
    public RequestNonElectricalBicycleController() {
        this.requisitionDB = new RequisitionDB();
        this.userDb = new UserDB();
        this.parkDb = new ParkDB();
    }

    /**
     * Sets new Requisition
     *
     * @return Requisition
     */
    public Requisition setData(int userID, Bicycle bicycle, Park pickUpPark, Park dropOffPark) {

        Date today = new Date();
        Date today2 = new Date();

        Requisition req = new Requisition(userID, bicycle, pickUpPark, dropOffPark, today, today2);
        return getRequisitionDB().addRequisition(req);
    }

    /**
     * Unlock any non-electrical bicycle at a given Park The time in milisendons
     * at which the bicycle was unlocked matches the exact time that the new
     * Requisition was issued (system date).
     *
     * @param allBicycles Set of bicycles available at a given park
     * @param userName Username
     * @return - Null if no bicycles are available or username is invalid or no
     * non-electrical Bicycle is available at the Park. - New filled requisition
     * if info received is valid
     */
    public Requisition unlockAnyBicycleAtPark(Set<Bicycle> allBicycles, String userName) {

        User usr = getUserDb().getUserByUsername(userName);
        Bicycle bicycle;

        if (allBicycles.isEmpty()) {
            return null;
        }

        if (usr.getUserId() == 0) {
            return null;
        }

        Iterator<Bicycle> bicIterator = allBicycles.iterator();

        while (bicIterator.hasNext()) {
            bicycle = bicIterator.next();
            if (bicycle.getTypeDescription().compareToIgnoreCase("electric") != 0) {
                Park pickUpPark = getParkDb().getPark(bicycle.getParkID());
                return setData(usr.getUserId(), bicycle, pickUpPark, pickUpPark);
            }

        }
        return null;
    }

    /**
     * RequisitionDB getter
     *
     * @return
     */
    private RequisitionDB getRequisitionDB() {
        return requisitionDB;
    }

    /**
     * UserDB getter
     *
     * @return
     */
    private UserDB getUserDb() {
        return userDb;
    }

    /**
     * ParkDB getter
     *
     * @return
     */
    private ParkDB getParkDb() {
        return parkDb;
    }
}
