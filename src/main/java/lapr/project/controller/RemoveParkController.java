package lapr.project.controller;

import lapr.project.data.ParkDB;

public class RemoveParkController {

    private ParkDB parkDb;

    /**
     * RemoveParkController constructor
     */
    public RemoveParkController() {
        this.parkDb = new ParkDB();
    }

    /**
     * ParkDB getter
     *
     * @return
     */
    private ParkDB getParkDb() {
        return parkDb;
    }

    /**
     * Remove park by ID
     *
     * @param parkId park Id
     * @return true if park has been removed; false if has not been removed
     */
    public boolean removePark(int parkId) {
        return getParkDb().removePark(parkId);
    }

}