package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;
import lapr.project.utils.ImportFiles;


public class CreateNewBicycleController {

    private Bicycle bicycle;
    private BicycleDB bDB;
    private ImportFiles importFiles;


    public CreateNewBicycleController() {
        this.bDB = new BicycleDB();
        this.importFiles = new ImportFiles();
    }

    /**
     * returns BicycleDB
     * @return BicycleDB
     */
    private BicycleDB getBicycleDB() {
        return bDB;
    }

    /**
     *
     * @param description
     * @param batteryLevel
     * @param fullBatteryCapacity
     * @param weigth
     * @param aerodynamic_coef
     * @param typeName
     * @param latitude
     * @param longitude
     * @return new bicycle added.
     */
    public Bicycle addBicycle(String description, double batteryLevel, double fullBatteryCapacity, double weigth,
                              double aerodynamic_coef, String typeName, double latitude, double longitude) {
        bicycle = new Bicycle(description, batteryLevel, fullBatteryCapacity, weigth, aerodynamic_coef);

        return getBicycleDB().addBicycle(bicycle, typeName, latitude, longitude);
    }

    /**
     *
     * @param fileName
     * @return count of number of bicycles added.
     */
    public int importBicycles(String fileName){
        return importFiles.importBicycles(fileName);
    }
}
