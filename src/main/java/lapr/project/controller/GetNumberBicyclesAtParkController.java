package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;

import java.util.Set;
import java.util.TreeSet;

public class GetNumberBicyclesAtParkController {

    private BicycleDB bicycleDB;
    private ParkDB parkDB;

    public GetNumberBicyclesAtParkController() {
        this.bicycleDB = new BicycleDB();
        this.parkDB = new ParkDB();
    }

    public int getNumberOfBicyclesAtPark(double parkLatitudeInDegrees,
                                         double parkLongitudeInDegrees,
                                         String outputFileName){

        Park park = getParkDB().getPark(getParkByCoordinates(parkLatitudeInDegrees, parkLongitudeInDegrees));
        Set<Bicycle> setOfBikes =  getBicyclesInPark(park);

        //ExportFiles.export(setBicycles);
        return setOfBikes.size();
    }

    private ParkDB getParkDB(){
        return parkDB;
    }

    private BicycleDB getBicycleDB(){
        return bicycleDB;
    }

    private int getParkByCoordinates(double parkLatitudeInDegrees,
                                     double parkLongitudeInDegrees){
       return parkDB.getParkByCoordinates(parkLatitudeInDegrees, parkLongitudeInDegrees);
    }

    private Set<Bicycle> getBicyclesInPark(Park park){
        Set<Bicycle> bicycleSet = getBicycleDB().getAllBicycles();
        Set<Bicycle> returningBicycleSet = new TreeSet<>();
        for(Bicycle b : bicycleSet){
            if(b.getParkID() == park.getParkId()){
                returningBicycleSet.add(b);
            }
        }
        return returningBicycleSet;
    }

}
