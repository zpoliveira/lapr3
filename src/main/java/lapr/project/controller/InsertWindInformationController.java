package lapr.project.controller;

import lapr.project.data.WindDB;
import lapr.project.model.Wind;
import lapr.project.utils.ImportFiles;


public class InsertWindInformationController {
    
    private Wind wind;
    private WindDB WindDB;
    private ImportFiles importFiles;

    /**
     * Controller constructor
     */
    public InsertWindInformationController() {
        this.importFiles=new ImportFiles();
        this.WindDB = new WindDB();
    }

    /**
     * returns WindBD
     * @return WindDb
     */
    private WindDB getWindDb() {
        return WindDB;
    }

    /**
     * Sets Wind data and add to DB
     *
     * @param initiallocationId Wind  initiallocationId
     * @param finallocationId Wind finallocationId
     * @param windspeed Wind longitude
     * @param windfactor  wind altitude
     * @return  wind information
     */
    public Wind setData(int initiallocationId, int finallocationId, double windspeed, double windfactor,
                        double aerodynamicCoefficient, String pathDirection) {

        this.wind = new Wind(initiallocationId, finallocationId, windspeed, windfactor,
                aerodynamicCoefficient, pathDirection);

        return getWindDb().addWind(wind);
    }

    /**
     *
     * @param fileName
     * @return count of number of wind information added.
     */
    public int importPaths(String fileName){
        return importFiles.importPaths(fileName);
    }

}