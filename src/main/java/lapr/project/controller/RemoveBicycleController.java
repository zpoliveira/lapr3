/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.Set;
import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;


public class RemoveBicycleController {
    
     private BicycleDB bicycleDb;

    /**
     * RemoveBicycleController's constructor
     */
    public RemoveBicycleController() {
        this.bicycleDb = new BicycleDB();
    }

    /**
     * Method to get BicycleDB
     *
     * @return bicycleDB
     */
    private BicycleDB getBicycleDB() {
        return bicycleDb;
    }

    /**
     *  method to get all available bicycles from the database
     * 
     * @return set allavailablebicycles
     */
    public Set<Bicycle> getAllAvailableBicycles(){
        return getBicycleDB().getAllAvailableBicycles();
    }

    /**
     * Remove Bicycle by ID
     * @param bicycleID bicycle Id
     * @return true if bicycle has been removed; false if has not been removed
     */
    public boolean removeBicycle(int bicycleID) {
       return getBicycleDB().removeBicycle(bicycleID);
    }

}
