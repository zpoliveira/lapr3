package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;


public class UpdateParkController {

    private ParkDB parkDB;
    private Park park;

    /**
     * Public UpdateParkController constructor
     */
    public UpdateParkController(){
        this.parkDB = new ParkDB();
    }

    /**
     * @return connection of ParkDb
     */
    private ParkDB getParkDb() {
        return parkDB;
    }


    /**
     * Method for update Park information with all attributtes
     * @param parkId
     * @param name
     * @param latitude
     * @param longitude
     * @param altitude
     * @param electricalLotation
     * @param nonElectricalLotation
     * @param maxElectricalLotation
     * @param maxNonElectricalLotation
     * @param inputVoltage
     * @param inputCurrent
     * @return
     */
    public boolean updatePark(int parkId, String name, double latitude, double longitude, double altitude,
                              int electricalLotation, int nonElectricalLotation, int maxElectricalLotation,
                              int maxNonElectricalLotation, double inputVoltage, double inputCurrent){
        park = new Park(name, latitude, longitude, altitude,maxElectricalLotation,maxNonElectricalLotation,
                inputVoltage,inputCurrent);
        park.setElectricalLotation(electricalLotation);
        park.setNonElectricalLotation(nonElectricalLotation);
        park.setParkId(parkId);
        getParkDb().updatePark(park);
        return park.isValid();
    }
}
