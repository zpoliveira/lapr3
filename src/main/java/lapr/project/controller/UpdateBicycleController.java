/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.BicycleTypeDB;
import lapr.project.model.Bicycle;

public class UpdateBicycleController {

    private Bicycle bicycle;
    private BicycleDB bicycleDB;
    private BicycleTypeDB bicycleTypeDB;

     /**
     * Public UpdateBicycleController constructor
     */
    public UpdateBicycleController() {
        this.bicycleDB = new BicycleDB();
        this.bicycleTypeDB = new BicycleTypeDB();
    }
    
    /**
     * Method that returns the BicycleDB object
     * @return connection of BicycleDB
     */
    private BicycleDB getBicycleDb() {
        return bicycleDB;
    }
    /**
     * Method that invokes getBicycleById from BicycleDB class
     * @param id
     * @return a bicycle 
     */
    public Bicycle getBicycleById(int id) {
        return bicycleDB.getBicycleById(id);
    }
    /**
     * Method that invokes getBicycleType from BicycleTypeDB class  
     * @param description
     * @return the type of the bicycle
     */
    public int getBicycleType(String description) {
        return bicycleTypeDB.getBicycleType(description).getTypeId();
    }
    
    /**
     * Method that sets the id of the bicycle to be updated
     * @param bicycleId - bicycleId to be updated
     * @return success of operation
     */
    private boolean setBicycle(int bicycleId, String description, double aerodynamicCoef) {
        bicycle.setBicycleID(bicycleId);
        bicycle.setBicycleDescription(description);
        bicycle.setAerodynamicCoefficient(aerodynamicCoef);
        return bicycle.getBicycleID() == bicycleId &&
                bicycle.getBicycleDescription().equals(description)&&
                Double.compare(bicycle.getAerodynamicCoefficient(),aerodynamicCoef) == 0;
    }

    /**
     * Method for update Bicycle information
     *
     * @param bicycleId
     * @param typeID
     * @param parkId
     * @param bateryLevel
     * @param fullBateryCapacity
     * @param weight
     * @return true in case successeful update
     */
    public boolean updateBicycle(int bicycleId, int typeID, int parkId, double bateryLevel, double fullBateryCapacity,
                                 double weight, String description, double aerodynamic_coef) {
        bicycle = new Bicycle(typeID, parkId, bateryLevel, fullBateryCapacity, weight);
        return (setBicycle(bicycleId, description, aerodynamic_coef) && getBicycleDb().updateBicycle(bicycle));
    }
}
