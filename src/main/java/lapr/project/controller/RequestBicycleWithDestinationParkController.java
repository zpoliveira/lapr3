/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;


import java.util.Date;
import java.util.Set;
import javafx.util.Pair;
import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.data.WindDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;


public class RequestBicycleWithDestinationParkController {
   
    private Requisition requisition;
    
    private RequisitionDB requisitionDB;
    private ParkDB parkDB;
    private BicycleDB bicycleDB;
    private WindDB windDB;
    private UserDB userDB;
    
    private Pair<Double, Double> windInfo;
    private User user;
    private Park originPark, dropPark;

    
    /**
     * Public RequestBicycleWithDestinationParkController constructor
     */
    public RequestBicycleWithDestinationParkController(){
        this.parkDB=new ParkDB();
        this.userDB=new UserDB();
        this.windDB =new WindDB();
        this.requisitionDB = new RequisitionDB();
        this.bicycleDB = new BicycleDB();
    }
    
    /**
     * Get all parks from DB
     * @return the parks from DB
     */
    public Set<Park> getAllParks (){
        return parkDB.getAllParks();
        
    }
    
    /**
     *  Private ParkDB getter
     * @return ParkDB
     */
    private ParkDB getParkDB(){
        return this.parkDB;
    }

    /**
     *  Private UserDB getter
     * @return
     */
    private UserDB getUserDB(){
        return this.userDB;
    }

    /**
     * Private WindDB getter
     * @return
     */
    private WindDB getWindDB() {
        return this.windDB;
    }

    /**
     * Return User object by its Id
     * @param userId
     * @return an User 
     */
    public User getUser(int userId){
        return getUserDB().getUser(userId);
    }
    
    /**
     * Return Park object by its Id
     * @param parkId
     * @return a Parks
     */
    public Park getPark(int parkId){
        return getParkDB().getPark(parkId);
    }

    /**
     * Method that returns the wind parametrs between two parks
     * @param pid1
     * @param pid2
     * @return Pair containing info on wind velocity and direction (Key: velocity; Value: Direction)
     *                  Positive direction value means tailwind;
     *                  Negative direction value means headwind.
     */
    Pair<Double, Double> getWindInfo(int pid1, int pid2){
        return getWindDB().getWindInfo(pid1, pid2);
    }
    
    /**
     * Method that gets all electrical available bicycles from a park with 
     * enough battery to travel between that park and other specified by the user
     * 
     * @param userId
     * @param originParkId
     * @param dropParkId
     * @return 
     */
    public Set<Bicycle> getAvailableBicycle(int userId, int originParkId, int dropParkId){
        this.user=getUser(userId);
        this.originPark=getPark(originParkId);
        this.dropPark=getPark(dropParkId);
        this.windInfo=getWindInfo(originParkId, dropParkId);
        Set<Bicycle> availableBicycleList = bicycleDB.getAllAvailableBicyclesWithGivenPower(user, originPark, dropPark,  windInfo.getKey(), windInfo.getValue());
        return availableBicycleList;
    }
    
    /**
     * Method that calls requisition add method to insert a new requisition in DB
     * 
     * @param userId
     * @param bike
     * @param pickUpParkId
     * @param dropParkId
     * @param inicialDate 
     * @return  
     */
    public boolean newRequisition(int userId, Bicycle bike, Park pickUpParkId, Park dropParkId, Date inicialDate){
        this.requisition = new Requisition(userId, bike, pickUpParkId,dropParkId, inicialDate);
        requisitionDB.addRequisition(requisition);
        return true;
    }
    
}
