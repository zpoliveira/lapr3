package lapr.project.controller;

import javafx.util.Pair;
import lapr.project.data.*;
import lapr.project.model.BicycleType;
import lapr.project.model.Park;
import lapr.project.model.User;
import lapr.project.utils.*;


public class CheckCaloriesProjectionController {

    private ParkDB parkDB;
    private UserDB userDB;
    private BicycleTypeDB bicycleTypeDB;
    private WindDB windDB;
    private BicycleType bType;
    private User user;
    private Park park1, park2;
    private Pair<Double, Double> windInfo;

    /**
     * CheckCaloriesProjectionController constructor
     *
     */
    public CheckCaloriesProjectionController(){
        this.parkDB=new ParkDB();
        this.userDB=new UserDB();
        this.bicycleTypeDB=new BicycleTypeDB();
        this.windDB = new WindDB();
    }

    /**
     *  Private ParkDB getter
     * @return
     */
    private ParkDB getParkDB(){
        return this.parkDB;
    }

    /**
     *  Private UserDB getter
     * @return
     */
    private UserDB getUserDB(){
        return this.userDB;
    }

    /**
     *  Private BicycleTypeDB getter
     * @return
     */
    private BicycleTypeDB getBicycleTypeDB() {
        return this.bicycleTypeDB;
    }

    /**
     * Private WindDB getter
     * @return
     */
    private WindDB getWindDB() {
        return this.windDB;
    }

    /**
     *  Private BicycleDB getter
     * @return
     */
    BicycleType getBicycleDB(String typeId){
        return getBicycleTypeDB().getBicycleType(typeId);
    }

    /**
     * Return Park object by Id
     * @param parkId
     * @return
     */
    public Park getPark(int parkId){
        return getParkDB().getPark(parkId);
    }

    /**
     * Return User object by Id
     * @param userId
     * @return
     */
    public User getUser(int userId){
        return getUserDB().getUser(userId);
    }

    /**
     *
     * @param pid1
     * @param pid2
     * @return Pair containing info on wind velocity and direction (Key: velocity; Value: Direction)
     *                  Positive direction value means tailwind;
     *                  Negative direction value means headwind.
     */
    Pair<Double, Double> getWindInfo(int pid1, int pid2){
        return getWindDB().getWindInfo(pid1, pid2);
    }


    /**
     * Calls methos thar calculates the projection of calories burnt between two Parks
     * @param usr User
     * @param p1 Origin Park
     * @param p2 Destiny Park
     * @param bicycleType Type of bicycle to be used
     * @return calories projection in Kcal
     */
    public double calculateCaloriesProjection(int usr, int p1, int p2, double bicWeight,
                                              String bicycleType) throws Exception {
        this.user=getUser(usr);
        this.park1=getPark(p1);
        this.park2=getPark(p2);
        this.bType=getBicycleDB(bicycleType);
        this.windInfo=getWindInfo(p1, p2);

        if (user==null || !user.isValid() ){
                throw new Exception("Invalid user!");
        }
        if ( park1==null || park2==null || !park1.isValid() || !park2.isValid() ){
            throw new Exception("Invalid park!");
        }
        if (Double.compare(bicWeight, 0)<=0){
            throw new Exception("Invalid bicycle weight!");
        }
        if (bType==null || !bType.isValid()){
            throw new Exception("Invalid bicycle type!");
        }

        double result = AuxMethods.calculateCaloriesProjection(user,park1,park2,bicWeight,
                bType.getWorkRatio(),windInfo.getKey(), windInfo.getValue());

        return result;
    }


}
