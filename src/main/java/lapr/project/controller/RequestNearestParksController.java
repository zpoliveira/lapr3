package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;



public class RequestNearestParksController {

    private ParkDB parkDB;

    /**
     * Package protected constructor for testing purposes
     */
    public RequestNearestParksController() {
        this.parkDB = new ParkDB();
    }

    /**
     * Method to list nearest parks
     * @param latitude user latitude
     * @param longitude user longitude
     * @param altitude user altitude
     * @return map with all maps and their distance to the user
     */
    public Map<Park,Double> listNearestParks(double latitude, double longitude, double altitude){

        Map<Park, Double> map = new HashMap<>();
        Map<Park, Double> newMap;
        Set<Park> allParks = this.parkDB.getAllParks();

            for (Park park : allParks) {
                double d = AuxMethods.getDistanceWithHeight(latitude, longitude, altitude, park);
                map.put(park, d);
            }
            newMap = AuxMethods.OrderMapByValue(map);

        return newMap;
    }

}
