package lapr.project.controller;

import lapr.project.data.UserDB;
import lapr.project.model.User;
import lapr.project.utils.ImportFiles;

public class CreateNewUserController {

    private UserDB userDB;
    private ImportFiles importFiles;

    public CreateNewUserController(){
        this.userDB = new UserDB();
        this.importFiles = new ImportFiles();
    }

    /**
     * returns UserDB
     * @return UserDB
     */
    public UserDB getUserDB(){
        return userDB;
    }

    /**
     * Controller's method to addUser to database.
     *
     *
     * @param name name variable for User
     * @param nif vat number variable for User
     * @param birthDate birth date variable for User
     * @param heigth height variable for User
     * @param weight weight variable for User
     * @param creditCardNumber credit card number variable for User
     * @param email email variable for User
     * 
     * @return
     */
    public User addUser(String name, int nif, String birthDate, double heigth, double weight, long creditCardNumber, String email, double averageSpeed){
         return getUserDB().addUser(name, nif, birthDate, heigth, weight, creditCardNumber, email, averageSpeed);
    }
    
    public int importUsers(String fileName){
        return importFiles.importUsers(fileName);
    }
    
}