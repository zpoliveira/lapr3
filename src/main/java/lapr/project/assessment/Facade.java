package lapr.project.assessment;

import lapr.project.controller.*;
import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.UserDB;
import lapr.project.model.*;
import lapr.project.utils.ExportFiles;
import lapr.project.utils.ImportFiles;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lapr.project.data.InvoiceDB;
import lapr.project.data.RequisitionDB;


public class Facade implements Serviceable {

    /**
     * Add Bicycles to the system.
     * <p>
     * Basic: Add one bicycle to one park.
     * Intermediate: Add several bicycles to one park.
     * Advanced: Add several bicycles to several parks.
     *
     * @param inputBicycleFile Path to file with bicycles to add, according
     *                         to input/bicycles.csv.
     * @return Number of added bicycles.
     */
    public int addBicycles(String inputBicycleFile) {
        CreateNewBicycleController cpb = new CreateNewBicycleController();
        return cpb.importBicycles(inputBicycleFile);
    }

    /**
     * Add Parks to the system.
     * <p>
     * Basic: Add one Park.
     * Intermediate: Add several Parks.
     *
     * @param inputParksFile Path to file that contains the parks, according
     *                       to file input/parks.csv.
     * @return The number of added parks.
     */
    public int addParks(String inputParksFile) {

        CreateNewParkController cnpk = new CreateNewParkController();
        return cnpk.importParks(inputParksFile);
    }

    /**
     * Add POIs to the system.
     * <p>
     * Basic: Add one POI.
     * Intermediate: Add several POIs.
     *
     * @param inputPOIsFile Path to file that contains the POIs, according to
     *                      file input/pois.csv.
     * @return The number of added POIs.
     */
    public int addPOIs(String inputPOIsFile) {
        CreateNewPoiController cnp = new CreateNewPoiController();
        return cnp.importPois(inputPOIsFile);
    }

    /**
     * Add Users to the system.
     * <p>
     * Basic: Add one User.
     * Intermediate: Add several Users.
     *
     * @param inputUsersFile Path to file that contains the Users, according
     *                       to file input/users.csv.
     * @return The number of added users.
     */
    public int addUsers(String inputUsersFile) {
        CreateNewUserController createNewUserController = new CreateNewUserController();
        return createNewUserController.importUsers(inputUsersFile);

    }

    /**
     * Add Paths to the system.
     *
     * @param inputPathsFile Path to file that contains the Paths, according
     *                       to file input/paths.csv.
     * @return The number of added Paths.
     */
    public int addPaths(String inputPathsFile){

        InsertWindInformationController iwiController = new InsertWindInformationController();

        return iwiController.importPaths(inputPathsFile);
    }

    /**
     * Get the list of bicycles parked at a given park.
     *
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @param outputFileName         Path to file where output should be written,
     *                               according to file output/bicycles.csv. Sort in
     *                               ascending order by bike description.
     * @return The number of bicycles at a given park.
     */
    public int getNumberOfBicyclesAtPark(double parkLatitudeInDegrees,
                                         double parkLongitudeInDegrees,
                                         String outputFileName) {

        Set<Bicycle> bicycleSet;
        CheckBicyclesAvailableAtParkController checkBicyclesAvailableAtParkController=
                new CheckBicyclesAvailableAtParkController();
        ExportFiles output = new ExportFiles();

        bicycleSet=checkBicyclesAvailableAtParkController
                .getAllAvailableBicyclesAtParkByCoordinates(parkLatitudeInDegrees, parkLongitudeInDegrees);
        return output.bicycles(outputFileName,bicycleSet);
    }

    /**
     * Get the number of free parking places at a given park for the loaned
     * bicycle.
     *
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @param username               The username that has an unlocked bicycle.
     * @return The number of free slots at a given park for the user's
     * bicycle type.
     */
    public int getFreeSlotsAtPArk(double parkLatitudeInDegrees,
                                  double parkLongitudeInDegrees, String username) {

        CheckParkingSlotsController cpsc = new CheckParkingSlotsController();
        return cpsc.checkFreeSlotsAtPark(parkLatitudeInDegrees, parkLongitudeInDegrees, username);
    }

    /**
     * Get a list of the nearest parks to the user.
     *
     * @param userLatitudeInDegrees  User latitude in Decimal Degrees.
     * @param userLongitudeInDegrees User longitude in Decimal Degrees.
     * @param outputFileName         Path to file where output should be written,
     *                               according to file output/pois.csv. Sort by
     *                               distance in ascending order.
     */
    public void getNearestParks(double userLatitudeInDegrees,
                                double userLongitudeInDegrees, String outputFileName) {


        RequestNearestParksController rnpController = new RequestNearestParksController();

        Map<Park,Double> nearestParksList = rnpController.listNearestParks(userLatitudeInDegrees,userLongitudeInDegrees,0d);

        ExportFiles ex = new ExportFiles();
        ex.mapParkDouble(outputFileName,nearestParksList);

    }

    /**
     * Get the distance from a location to another.
     *
     * @param destinyLatitudeInDegrees  Origin latitude in Decimal Degrees.
     * @param originLongitudeInDegrees  Origin longitude in Decimal Degrees.
     * @param destinyLatitudeInDegrees  Destiny latitude in Decimal Degrees.
     * @param destinyLongitudeInDegrees Destiny longitude in Decimal Degrees.
     * @return Returns the distance in meters from one location to another.
     */
    public int distanceTo(double originLatitudeInDegrees,
                          double originLongitudeInDegrees,
                          double destinyLatitudeInDegrees,
                          double destinyLongitudeInDegrees) {

        CheckDistandBetweenParksController cdbpController = new CheckDistandBetweenParksController();

        return cdbpController.getDistanceFromLocationToAnother(originLatitudeInDegrees,originLongitudeInDegrees,destinyLatitudeInDegrees,destinyLongitudeInDegrees);
    }

    /**
     * Unlocks a specific bicycle.
     *
     * @param bicycleDescription Bicycle description to unlock.
     * @return The time in miliseconds at which the bicycle was unlocked.
     */
    public long unlockBicycle(String bicycleDescription, String username) {
        RequestAnyBicycleByDescriptionController reqAnyBike = new RequestAnyBicycleByDescriptionController();
        return reqAnyBike.requestyAnyBicycleByDescription(bicycleDescription, username);
    }

    /**
     * Lock a specific bicycle at a park.
     * <p>
     * Basic: Lock a specific bicycle at a park.
     * Intermediate: Charge the user if 1h is exceeded.
     * Advanced: Add points to user.
     *
     * @param bicycleDescription     Bicycle to lock.
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @return The time in miliseconds at which the bicycle was locked.
     */
    public long lockBicycle(String bicycleDescription, double parkLatitudeInDegrees,
                            double parkLongitudeInDegrees) {
        ReturnBicycleController rbc = new ReturnBicycleController();
        return rbc.returnBicycle(bicycleDescription, parkLatitudeInDegrees, parkLongitudeInDegrees);
    }

    /**
     * Return the current debt for the user.
     *
     * @param username       The user to get the debt from.
     * @param outputFileName The path for the file to output the debt,
     *                       according to file output/balance.csv.
     *                       Sort the information by unlock time in ascending
     *                       order (oldest to newest).
     * @return The User's current debt in euros, rounded to two decimal places
     */
    public double getUserCurrentDebt(String username, String outputFileName) {
        return 0;
    }

    /**
     * Return the current points for the user.
     *
     * @param username       The user to get the points report from.
     * @param outputFileName The path for the file to output the points,
     *                       according to file output/points.csv.
     *                       Sort the information by unlock time in ascenind
     *                       order (oldest to newest).
     * @return The User's current points.
     */
    public double getUserCurrentPoints(String username, String outputFileName) {
        InvoiceIssuingController invIssController = new InvoiceIssuingController();
        UserDB uDB = new UserDB();
        RequisitionDB reqDB = new RequisitionDB();
        int userID = uDB.getUserByUsername(username).getUserId();
        double userPoints = (double) invIssController.getUserPoints(userID);
        Set<Requisition> usersRequist =  reqDB.getAllRequisitionsForUser(userID);
        ExportFiles eFiles = new ExportFiles();
        eFiles.points(outputFileName, usersRequist);
  
        return userPoints;
        
    }

    /**
     * Unlocks an available bicycle at one park.
     *
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @param outputFileName         Write the unlocked bicycle information to a file,
     *                               according to file output/bicycles.csv.
     * @return The time in milisendons at which the bicycle was unlocked.
     */
    public long unlockAnyBicycleAtPark(double parkLatitudeInDegrees,
                                       double parkLongitudeInDegrees,
                                       String username,
                                       String outputFileName) {

        CheckBicyclesAvailableAtParkController checkBicyclesAvailableAtParkController =
                new CheckBicyclesAvailableAtParkController();

        RequestNonElectricalBicycleController requestNonElectricalBicycleController =
                new RequestNonElectricalBicycleController();

        ExportFiles exportFiles = new ExportFiles();

        Set<Bicycle> setOfBicycles=checkBicyclesAvailableAtParkController
                .getAllAvailableBicyclesAtParkByCoordinates(parkLatitudeInDegrees, parkLongitudeInDegrees);


            Requisition requisition =
                    requestNonElectricalBicycleController.unlockAnyBicycleAtPark(setOfBicycles, username);

        if (requisition!=null){
            setOfBicycles.clear();
            setOfBicycles.add(requisition.getBicycle());
            exportFiles.bicycles(outputFileName, setOfBicycles);
            return requisition.getInitialDate().getTime();
        }

        return 0;
    }

    /**
     * Unlocks an eletrical bicycle at one park. It should unlock the one
     * with higher battery capacity.
     *
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @param outputFileName         Write the unlocked bicycle information to a file,
     *                               according to file output/bicycles.csv.
     * @return The time in milisendons at which the bicycle was unlocked.
     */
    public long unlockAnyElectricBicycleAtPark(double parkLatitudeInDegrees,
                                              double parkLongitudeInDegrees,
                                              String username,
                                              String outputFileName) {
        ParkDB parkDB = new ParkDB();
        UserDB userDB = new UserDB();
        RequestEletricalBicycleWithoutDestinationParkController reqEleBike =
                new RequestEletricalBicycleWithoutDestinationParkController();

        int parkID = parkDB.getParkByCoordinates(parkLatitudeInDegrees,parkLongitudeInDegrees);
        Park park = parkDB.getPark(parkID);
        User selectedUser = userDB.getUserByUsername(username);
        Bicycle b = reqEleBike.setData(selectedUser.getUserId(), park);
        Set<Bicycle> set = new HashSet<>();
        set.add(b);
        new ExportFiles().bicycles(outputFileName, set);
        return new Date().getTime();
    }

    /**
     * Calculate the amount of eletrical energy required to travel from one
     * park to another.
     *
     * @param originLatitudeInDegrees       Origin latitude in Decimal degrees.
     * @param originLongitudeInDegrees      Origin Longitude in Decimal degrees.
     * @param destinationLatitudeInDegrees  Destination Park latitude in
     *                                      Decimal degrees.
     * @param destinationLongitudeInDegrees Destination Park Longitude in
     *                                      Decimal degrees.
     * @param username                      Username.
     * @return The eletrical energy required in kWh, rounded to two decimal
     * places.
     */
    public double calculateElectricalEnergyToTravelFromOneLocationToAnother(
            double originLatitudeInDegrees,
            double originLongitudeInDegrees,
            double destinationLatitudeInDegrees,
            double destinationLongitudeInDegrees,
            String username) {

        DifferentRoutesBetweenTwoParksController df = new DifferentRoutesBetweenTwoParksController();

        return df.calculateEletricalEnergyToTravelFromOneLocationToAnother(originLatitudeInDegrees,originLongitudeInDegrees,destinationLatitudeInDegrees,destinationLongitudeInDegrees,username);
    }

    /**
     * Suggest an eletrical bicycle with enough energy + 10% to go from one
     * Park to another.
     *
     * @param originParkLatitudeInDegrees       Origin Park latitude in Decimal
     *                                          degrees.
     * @param originParkLongitudeInDegrees      Origina Park Longitude in Decimal
     *                                          degrees.
     * @param destinationParkLatitudeInDegrees  Destination Park latitude in
     *                                          Decimal degrees.
     * @param destinationParkLongitudeInDegrees Destination Park Longitude in
     *                                          Decimal degrees.
     * @param username                          Username.
     * @param outputFileName                    Write the bicycles information to a file,
     *                                          according to file output/bicycles.csv.
     * @return The number of suggested bicycles.
     */
    public int suggestElectricalBicyclesToGoFromOneParkToAnother(
            double originParkLatitudeInDegrees,
            double originParkLongitudeInDegrees,
            double destinationParkLatitudeInDegrees,
            double destinationParkLongitudeInDegrees,
            String username,
            String outputFileName) {

        CheckBicyclesAvailableAtParkController cbapController = new CheckBicyclesAvailableAtParkController();

        ExportFiles exportFiles = new ExportFiles();

        Set<Bicycle> bikes = cbapController.suggestEletricalBicyclesToGoFromOneParkToAnother(originParkLatitudeInDegrees,originParkLongitudeInDegrees,destinationParkLatitudeInDegrees,destinationParkLongitudeInDegrees,username,outputFileName);

         exportFiles.bicycles(outputFileName,bikes);

        return bikes.size();
    }

    /**
     * Get for how long has a bicycle been unlocked.
     *
     * @param bicycleDescription Bicycle description.
     * @return The time in seconds since the bicycle was unlocked.
     */
    public long forHowLongWasTheBicycleUnlocked(String bicycleDescription) {
        CheckBicyclesAvailableAtParkController checkBicyclesAvailableAtParkController =
                new CheckBicyclesAvailableAtParkController();
        return checkBicyclesAvailableAtParkController.getUnlockedBicTime(bicycleDescription);
    }


    /**
     * Calculate the shortest Route from one park to another.
     * <p>
     * Basic: Only one shortest Route between two Parks is available.
     * Intermediate: Consider that connections between locations are not
     * bidirectional.
     * Advanced: More than one Route between two parks are available with
     * different number of points inbetween and different evelations difference.
     *
     * @param originLatitudeInDegrees       Origin latitude in Decimal degrees.
     * @param originLongitudeInDegrees      Origin Longitude in Decimal degrees.
     * @param destinationLatitudeInDegrees  Destination Park latitude in
     *                                      Decimal degrees.
     * @param destinationLongitudeInDegrees Destination Park Longitude in
     *                                      Decimal degrees.
     * @param outputFileName                Write to the file the Route between two parks
     *                                      according to file output/paths.csv. More than one
     *                                      path may exist. If so, sort routes by the ascending
     *                                      number of points between the parks and by ascending
     *                                      order of elevation difference.
     * @return The distance in meters for the shortest path.
     */
    public long shortestRouteBetweenTwoParks(
            double originLatitudeInDegrees,
            double originLongitudeInDegrees,
            double destinationLatitudeInDegrees,
            double destinationLongitudeInDegrees,
            String outputFileName) {

        DifferentRoutesBetweenTwoParksController dfbtpController = new DifferentRoutesBetweenTwoParksController();
        ParkDB parkDB = new ParkDB();

        dfbtpController.addSortingCriteria(1);
        dfbtpController.numberOfRoutes(1);

        int originPark = parkDB.getParkByCoordinates(originLatitudeInDegrees,originLongitudeInDegrees);
        int destinyPark = parkDB.getParkByCoordinates(destinationLatitudeInDegrees,destinationLongitudeInDegrees);

        Map<Location[],Double> result = dfbtpController.mostEnergeticallyEfficientRoute(null,originPark,destinyPark,0d,0d,1);

        ExportFiles exportFiles = new ExportFiles();

        exportFiles.exportPath(outputFileName,result,1);

        return Math.round(result.values().iterator().next());
    }

    /**
     * Calculate the most energetically efficient route from one park to
     * another using any bicycle.
     * <p>
     * Basic: Does not consider wind.
     * Intermediate: Considers wind.
     * Advanced: Considers the different mechanical and aerodynamic
     * coefficients.
     *
     * @param originLatitudeInDegrees       Origin latitude in Decimal degrees.
     * @param originLongitudeInDegrees      Origin Longitude in Decimal degrees.
     * @param destinationLatitudeInDegrees  Destination Park latitude in
     *                                      Decimal degrees.
     * @param destinationLongitudeInDegrees Destination Park Longitude in
     *                                      Decimal degrees.
     * @param typeOfBicyle                  The type of bicycle required e.g. "eletric", "mtb"
     *                                      or "road".
     * @param username                      The username.
     * @param outputFileName                Write to the file the Route between two parks
     *                                      according to file output/paths.csv. More than one
     *                                      path may exist. If so, sort routes by the ascending
     *                                      number of points between the parks and by ascending
     *                                      order of elevation difference.
     * @return The distance in meters for the shortest path.
     */
    public long mostEnergyEfficientRouteBetweenTwoParks(
            double originLatitudeInDegrees,
            double originLongitudeInDegrees,
            double destinationLatitudeInDegrees,
            double destinationLongitudeInDegrees,
            String typeOfBicyle,
            String username,
            String outputFileName) {

        DifferentRoutesBetweenTwoParksController dfbtpController = new DifferentRoutesBetweenTwoParksController();

        ParkDB parkDB = new ParkDB();
        UserDB userDB = new UserDB();
        BicycleDB bikeDB = new BicycleDB();

        dfbtpController.addSortingCriteria(1);
        dfbtpController.numberOfRoutes(1);

        int originPark = parkDB.getParkByCoordinates(originLatitudeInDegrees,originLongitudeInDegrees);
        int destinyPark = parkDB.getParkByCoordinates(destinationLatitudeInDegrees,destinationLongitudeInDegrees);

        User user = userDB.getUserByUsername(username);
        Bicycle b = bikeDB.returnOneBycicleOfType(typeOfBicyle);

        Map<Location[],Double> result = dfbtpController.mostEnergeticallyEfficientRoute(user,originPark,destinyPark,b.getWeight(),bikeDB.getBycicleRatioByType(b.getTypeID()),2);

        ExportFiles exportFiles = new ExportFiles();

        exportFiles.exportPath(outputFileName,result,2);

        return Math.round(result.values().iterator().next());
    }

    /**
     * Calculate the shortest Route from one park to another.
     * <p>
     * Basic: Only one shortest Route between two Parks is available.
     * Intermediate: Consider that connections between locations are not
     * bidirectional.
     * Advanced: More than one Route between two parks are available with
     * different number of points inbetween and different evelations difference.
     *
     * @param originLatitudeInDegrees       Origin latitude in Decimal degrees.
     * @param originLongitudeInDegrees      Origin Longitude in Decimal degrees.
     * @param destinationLatitudeInDegrees  Destination Park latitude in
     *                                      Decimal degrees.
     * @param destinationLongitudeInDegrees Destination Park Longitude in
     *                                      Decimal degrees.
     * @param inputPOIs                     Path to file that contains the POIs that the route
     *                                      must go through, according to file input/pois.csv.
     * @param outputFileName                Write to the file the Route between two parks
     *                                      according to file output/paths.csv. More than one
     *                                      path may exist. If so, sort routes by the ascending
     *                                      number of points between the parks and by ascending
     *                                      order of elevation difference.
     * @return The distance in meters for the shortest path.
     */
    public long shortestRouteBetweenTwoParksForGivenPOIs(
            double originLatitudeInDegrees,
            double originLongitudeInDegrees,
            double destinationLatitudeInDegrees,
            double destinationLongitudeInDegrees,
            String inputPOIs,
            String outputFileName) {

        DifferentRoutesBetweenTwoParksController dfbtpController = new DifferentRoutesBetweenTwoParksController();
        ParkDB parkDB = new ParkDB();
        ImportFiles im = new ImportFiles();

        dfbtpController.setIntermediateLocationArray(im.importIntermediatePois(inputPOIs));

        dfbtpController.addSortingCriteria(1);
        dfbtpController.numberOfRoutes(1);

        int originPark = parkDB.getParkByCoordinates(originLatitudeInDegrees,originLongitudeInDegrees);
        int destinyPark = parkDB.getParkByCoordinates(destinationLatitudeInDegrees,destinationLongitudeInDegrees);

        Map<Location[],Double> result = dfbtpController.mostEnergeticallyEfficientRoute(null,originPark,destinyPark,0d,0d,1);

        ExportFiles exportFiles = new ExportFiles();
        exportFiles.exportPath(outputFileName,result,1);

        return Math.round(result.values().iterator().next()*1000);
    }

    /**
     * Get a report for the bicycle charging status at a given park.
     *
     * @param parkLatitudeInDegrees  Park latitude in Decimal degrees.
     * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
     * @param outputFileName         Path to file where bicycle information should be
     *                               written, according to file output/bicycles.csv.
     *                               Sort items by descending order of time to finish
     *                               charge in seconds and secondly by ascending bicycle
     *                               description order.
     * @return The number of bicycles charging at the moment that are not
     * 100% fully charged.
     */
    public long getParkChargingReportForPark(
            double parkLatitudeInDegrees,
            double parkLongitudeInDegrees,
            String outputFileName) {

        RequestChargeReportController rcrController = new RequestChargeReportController();
        ParkDB parkDB = new ParkDB();
        int parkID = parkDB.getParkByCoordinates(parkLatitudeInDegrees,parkLongitudeInDegrees);
        Park park = parkDB.getPark(parkID);
        Map<Bicycle, String> parkChargingReport = rcrController.requestChargeReport(park);
        parkChargingReport = rcrController.requestBicycleThatAreChargingReport(parkChargingReport);
        new ExportFiles().exportChargeReport(outputFileName, parkChargingReport);

        return parkChargingReport.size();
    }

    /**
     * Calculate the most energetically efficient route from one park to
     * another with sorting options.
     *
     * @param originLatitudeInDegrees       Origin latitude in Decimal degrees.
     * @param originLongitudeInDegrees      Origin Longitude in Decimal degrees.
     * @param destinationLatitudeInDegrees  Destination Park latitude in
     *                                      Decimal degrees.
     * @param destinationLongitudeInDegrees Destination Park Longitude in
     *                                      Decimal degrees.
     * @param typeOfBicycle                  The type of bicycle required e.g. "eletric", "mtb"
     *                                      or "road".
     * @param username                      The user that asked for the routes.
     * @param maxNumberOfSuggestions        The maximum number of suggestions to
     *                                      provide.
     * @param ascendingOrder                If routes should be ordered by ascending or
     *                                      descending order
     * @param sortingCriteria               The criteria to user for ordering "energy",
     *                                      "shortest_distance", "number_of_points"
     * @param outputFileName                Write to the file the Route between two parks
     *                                      according to file output/paths.csv. More than one
     *                                      path may exist.
     * @return The number of suggestions
     */
    public int suggestRoutesBetweenTwoLocations(double originLatitudeInDegrees,
                                                double originLongitudeInDegrees,
                                                double destinationLatitudeInDegrees,
                                                double destinationLongitudeInDegrees,
                                                String typeOfBicycle,
                                                String username,
                                                int maxNumberOfSuggestions,
                                                boolean ascendingOrder,
                                                String sortingCriteria,
                                                String inputPOIs,
                                                String outputFileName) {
        return 0;
    }


    /**
     * Get the invoice for the current month.
     * This should include all bicycle loans that charged the user, the
     * number of points the user had before the actual month, the number of
     * points earned during the month, the number of points converted to euros.
     *
     * @param month      The month of the invoice e.g. 1 for January.
     * @param username   The user for which the invoice should be created.
     * @param outputPath Path to file where the invoice should be written,
     *                   according to file output/invoice.csv.
     * @return User debt in euros rounded to two decimal places.
     */
    public double getInvoiceForMonth(int month, String username, String outputPath) {
        InvoiceIssuingController iiController = new InvoiceIssuingController();
        UserDB userDB = new UserDB();
        InvoiceDB invoiceDB = new InvoiceDB();
        int selectedUser = userDB.getUserByUsername(username).getUserId();
        Invoice selectedInvoice = invoiceDB.getInvoiceForUser(selectedUser, month);
        int invoiceId=selectedInvoice.getInvoiceID();
        double userDebt = iiController.getSumMonthlyPayments(month, selectedUser);
        int userPoints =  iiController.getUserPoints(selectedUser);
        RequisitionDB reqDB = new RequisitionDB();
        Set<Requisition> userInvoices =  reqDB.getAllRequisitionsForUser(selectedUser);
        ExportFiles eFiles = new ExportFiles();
        
        eFiles.exportInvoice(outputPath, userInvoices,username, userPoints,selectedInvoice);
        
        return userDebt;
    
    }



}