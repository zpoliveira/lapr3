/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import lapr.project.model.Payment;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;


public class PaymentDB extends DataHandler {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    static final String USER = "LAPR3_G1";
    static final String PASS = "gebos";

    Connection conn = null;
    CallableStatement callStmt = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;

    /**
     * Method that invokes addPayment() method and estimatePayment() method
     * 
     * @param requisitionId
     * @param payment
     * @return the new payment
     */
    public Payment addPayment(int requisitionId, Payment payment) {
        double amount = estimatePayment(requisitionId, payment.getServiceTypeId());
        return addPayment(payment.getUserId(), payment.getServiceTypeId(), payment.getInvoiceId(), payment.getDate(), amount);
    }

    /**
     * Method that adds a new payment to the database 
     * 
     * @param userId
     * @param serviceType
     * @param invoiceId
     * @param date
     * @param amount
     * @return 
     */
    private Payment addPayment(int userId, int serviceType, int invoiceId, Date date, double amount) {
        Payment newPayment = new Payment();

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout("Connected database successfully...");

            pstmt = conn.prepareStatement("INSERT INTO Payment (userId, serviceType, invoiceId, paymentdate, amount) VALUES ( ?, ?, ?, ?, ?)");
            pstmt.setInt(1, userId);
            pstmt.setInt(2, serviceType);
            pstmt.setInt(3, invoiceId);
            pstmt.setDate(4, AuxMethods.convertDateToSQL(date));
            pstmt.setDouble(5, amount);

            pstmt.execute();
            AuxMethods.sout("Inserted records into the table...");

            // STEP 4: Clean-up environment
            pstmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return newPayment;
    }

    /**
     * Method that invokes FUNC_PAYMENTAMOUT function from database to estimate
     * the amout to be paid for a requisition. 
     * 
     * @param requisitionId
     * @param serviceType
     * @return the amout to be payed for a requisition
     */
    private double estimatePayment(int requisitionId, int serviceType) {
        Double amount = null;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_PAYMENTAMOUT(?,?) }");
            callStmt.registerOutParameter(1, Types.DOUBLE);
            callStmt.setDouble(2, serviceType);
            callStmt.setDouble(3, requisitionId);

            callStmt.execute();
            amount = callStmt.getDouble(1);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try
        return amount;
    }

}
