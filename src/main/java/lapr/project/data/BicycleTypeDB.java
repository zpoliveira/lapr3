/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data;

import java.sql.*;

import lapr.project.model.BicycleType;
import lapr.project.utils.AuxMethods;


public class BicycleTypeDB extends DataHandler {
    // JDBC driver name and database URL
    final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final String USER = "LAPR3_G1";
    final String PASS = "gebos";

    Connection conn = null;
    PreparedStatement pstmt = null;
    Statement stmt = null;

    /**
     * method to get a bicycle from the database
     *
     * @param type
     * @return
     */
    public BicycleType getBicycleType(String type) {
        
        int typeId=0;
        String description=null;
        double workRatio=0;

        
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout("Connected database successfully...");
            String sql = "SELECT * FROM BicycleType where description=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, type);
            ResultSet rs = pstmt.executeQuery();


            // STEP 4: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                typeId = rs.getInt("typeId");
                description = rs.getNString("description");
                workRatio = rs.getDouble("workratio");
              
            }
            // STEP 5: Clean-up environment
            rs.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
                closeAll();
        } // end try

        BicycleType bType = new BicycleType();
        bType.setTypeId(typeId);
        bType.setDescription(description);
        bType.setWorkRatio(workRatio);
    
        return bType;

    }

}
