package lapr.project.data;

import lapr.project.model.User;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;
import oracle.jdbc.internal.OracleTypes;

import java.sql.*;

public class UserDB extends DataHandler {

    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection connec = null;
    PreparedStatement pstmt = null;
    CallableStatement callStmt = null;

    public User getUser(int userId) {
        int id = 0;
        String name = "";
        int nif = 0;
        int userpoints = 0;
        String birthDate = "";
        double heigth = 0;
        double weight = 0;
        long creditCardNumber = 0;
        String email = "";
        int state = 0;
        double averageSpeed = 0;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout("Connecting to a selected database...");
            connec = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout("Connected database successfully...");
            String sql = "SELECT * FROM Users where userID =?";

            pstmt = connec.prepareStatement(sql);

            pstmt.setInt(1, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                id = rs.getInt("userid");
                name = rs.getString("name");
                nif = rs.getInt("nif");
                birthDate = rs.getDate("birthDate").toString();
                userpoints = rs.getInt("userpoints");
                heigth = rs.getDouble("height");
                weight = rs.getDouble("weigth");
                creditCardNumber = rs.getLong("creditCardNumber");
                email = rs.getString("email");
                state = rs.getInt("state");
                averageSpeed = rs.getDouble("average_speed");
                
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (connec != null) {
                    connec.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            } finally {
                closeAll();
            }
        }

        User u = new User(name, nif, birthDate, heigth, weight, creditCardNumber, email,averageSpeed);
        u.setState(state);
        u.setUserPoints(userpoints);
        u.setUserId(id);


        return u;

    }

public User getUserByUsername(String username){

        return getUser(getUserIDByName(username));

    }


    private int getUserIDByName(String username) {

        int userID = 0;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            connec = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = connec.prepareCall("{ ? = call FUNC_GET_USERID_BY_NAME(?) }");
            callStmt.registerOutParameter(1, OracleTypes.INTEGER);
            callStmt.setString(2, username);

            callStmt.execute();
            userID = callStmt.getInt(1);

            connec.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return userID;

    }

    public User addUser(User user){
        return addUser(user.getName(), user.getNif(), user.getBirthDate(), user.getHeigth(), user.getWeight(), 
                user.getCreditCardNumber(), user.getEmail(), user.getUserAverageSpeed());
    }

    /**
     *
     * public method to insert a new user.
     *
     * @param name name variable for User
     * @param nif vat number variable for User
     * @param birthDate birth date variable for User
     * @param heigth height variable for User
     * @param weight weight variable for User
     * @param creditCardNumber credit card number variable for User
     * @param email email variable for User
     * @param averageSpeed the average speed of the user
     * @return true if correctly inserted, false if don't
     */
    public User addUser(String name, int nif, String birthDate, double heigth, double weight, long creditCardNumber,
            String email, Double averageSpeed) {

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout("Connecting to a selected database...");
            connec = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout("Connected database successfully...");

            callStmt = connec.prepareCall("{ call PROC_ADDUSER(?, ?, ?, ?, ?, ?, ?, ?) }");
            callStmt.setString(1, name);
            callStmt.setInt(2, nif);
            callStmt.setDate(3,  java.sql.Date.valueOf(birthDate));
            callStmt.setDouble(4, heigth);
            callStmt.setDouble(5, weight);
            callStmt.setLong(6, creditCardNumber);
            callStmt.setString(7, email);
            callStmt.setDouble(8, averageSpeed);
            callStmt.execute();

            callStmt = connec.prepareCall("{ ? = call FUNC_MAX_USER()}");
            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int userID = callStmt.getInt(1);
            User user = new User(name,nif,birthDate,heigth,weight,creditCardNumber,email,averageSpeed);
            user.setUserId(userID);

            connec.close();
            return user;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                callStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally { 
                closeAll();

            }
        }
        return null;
    }

    /**
     * Calls PROC_REMOVEUSER stored procedure in order to remove a given user ID
     * from Users table.
     *
     * @param userId user unique id
     */
    public boolean removeUser(int userId) {

        try {

            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            connec = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = connec.prepareCall("{ call PROC_REMOVEUSER(?) }");
            callStmt.setInt(1, userId);
            callStmt.execute();
            closeAll();
            return true;

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return false;
    }
    
    public int getUserPoints(int userId) {
        int quantityOfUsuerPointsAvaliable = 0;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            connec = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = connec.prepareCall("{ ? = call FUNC_GETUSERPOINTS(?) }");
            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.setInt(2, userId);
            callStmt.execute();
            quantityOfUsuerPointsAvaliable = callStmt.getInt(1);

            closeAll();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return quantityOfUsuerPointsAvaliable;
    }
    
    public boolean updateUserPoints(int userId, int userPoints) {

        try {

            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            connec = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = connec.prepareCall("{ call PROC_UPDATEUSERPOINTS(?,?) }");
            callStmt.setInt(1, userId);
            callStmt.setInt(2, userPoints);
            callStmt.execute();
            closeAll();
            return true;

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return false;
    }

}
