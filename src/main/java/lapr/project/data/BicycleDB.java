/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import lapr.project.controller.DifferentRoutesBetweenTwoParksController;
import lapr.project.model.Bicycle;
import lapr.project.model.Location;
import lapr.project.utils.AuxMethods;
import oracle.jdbc.OracleTypes;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import lapr.project.model.Park;
import lapr.project.model.User;
import lapr.project.utils.Configs;




public class BicycleDB extends DataHandler  {

    //cal to kW
    private static final double CONVERTION_FACTOR = 860420.65;
    //the bicycle energy factor (10% extra) 
    final static double FACTOR = 1.1;
    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection conn = null;
    PreparedStatement pstmt = null;
    CallableStatement callStmt = null;
    Statement stmt = null;

    /**
     * Method to get a bicycle from the database
     *
     * @param id the bicycle id
     * @return a bicycle
     */
    public Bicycle getBicycleById(int id) {

        int bicycleID = 0;
        int typeID = 0;
        int parkID = 0;
        double batteryLevel = 0;
        double fullBatteryCapacity = 0;
        double weigth = 0;
        int available = 0;
        String description = "";
        double aero_dynam = 0;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql = "SELECT * FROM Bicycle where bicycleID =?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            ResultSet rs = pstmt.executeQuery();

            // STEP 4: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                bicycleID = rs.getInt("bicycleID");
                typeID = rs.getInt("typeID");
                parkID = rs.getInt("parkID");
                batteryLevel = rs.getDouble("batteryLevel");
                fullBatteryCapacity = rs.getDouble("fullBatteryCapacity");
                weigth = rs.getDouble("weight");
                available = rs.getInt("available");
                description = rs.getString("description");
                aero_dynam = rs.getDouble("aerodynamic_coefficient");


            }
            // STEP 5: Clean-up environment
            rs.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        Bicycle b = new Bicycle();
        b.setBicycleID(bicycleID);
        b.setTypeID(typeID);
        b.setParkID(parkID);
        b.setBatteryLevel(batteryLevel);
        b.setFullBatteryCapacity(fullBatteryCapacity);
        b.setWeight(weigth);
        b.setAvaliability(available != 0);
        b.setBicycleDescription(description);
        b.setAerodynamicCoefficient(aero_dynam);
        return b;

    }

    /**
     * Method to get all available bicycles from the database
     *
     * @return set allavailablebicycles
     */
    public Set<Bicycle> getAllAvailableBicycles() {

        return getAllAvailableBicyclesFromDB();

    }

    /**
     * Method to get all available bicycles from the database
     *
     * @return set allavailablebicycles
     */
    private Set<Bicycle> getAllAvailableBicyclesFromDB() {

        Set<Bicycle> allAvailableBicycles = new HashSet<>();

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout("Connected database successfully...");

            callStmt = conn.prepareCall("{ ? = call FUNC_GETAVAILABLEBICYCLES() }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                Bicycle auxBicycle = new Bicycle(rSet.getInt(2), rSet.getInt(3), rSet.getInt(4), rSet.getDouble(5), rSet.getDouble(6));
                auxBicycle.setBicycleID(rSet.getInt(1));
                boolean check = rSet.getInt(7)==0?Boolean.FALSE:Boolean.TRUE;
                auxBicycle.setAvaliability(check);
                auxBicycle.setBicycleDescription(rSet.getString(8));
                auxBicycle.setAerodynamicCoefficient(rSet.getDouble(9));
                auxBicycle.setTypeDescription(rSet.getString(10));
                allAvailableBicycles.add(auxBicycle);
            }
            callStmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return allAvailableBicycles;
    }


    /**
     * Method to get all available bicycles from the database
     *
     * @return set allavailablebicycles
     */
    public Set<Bicycle> getAllBicycles() {

        return getAllBicyclesFromDB();

    }

    /**
     * Method to get all bicycles from the database
     *
     * @return set allavailablebicycles
     */
    private Set<Bicycle> getAllBicyclesFromDB() {

        Set<Bicycle> allBicycles = new HashSet<>();

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout("Connected database successfully...");

            callStmt = conn.prepareCall("{ ? = call FUNC_GET_ALL_BICYCLES() }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                Bicycle auxBicycle = new Bicycle(rSet.getInt(2), rSet.getInt(3), rSet.getDouble(4), rSet.getDouble(5), rSet.getDouble(6));
                boolean check = rSet.getInt(7)==0?Boolean.FALSE:Boolean.TRUE;
                auxBicycle.setAvaliability(check);
                auxBicycle.setBicycleDescription(rSet.getString(8));
                auxBicycle.setAerodynamicCoefficient(rSet.getDouble(9));
                auxBicycle.setTypeDescription(rSet.getString(10));
                auxBicycle.setBicycleID(rSet.getInt(1));
                allBicycles.add(auxBicycle);
            }
            callStmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return allBicycles;
    }

    /**
     * Method that call the addBicycle method below
     *
     * @param bicycle
     * @return a bicycle
     */
    public Bicycle addBicycle(Bicycle bicycle, String typeName, double latitude, double longitude) {
        return addBicycle(bicycle.getBicycleDescription() ,bicycle.getBatteryLevel(), bicycle.getFullBatteryCapacity(),
                bicycle.getWeight(), bicycle.getAerodynamicCoefficient() ,typeName, latitude, longitude);
    }

    /**
     * Method that adds a bicycle to the database
     *
     * @param description, the park's description
     * @param batteryLevel, actual bicycle battery levef
     * @param fullBatteryCapacity, total battery capacy of bicycle 
     * @param weigth, the bicycle wwight
     */
    private Bicycle addBicycle(String description, double batteryLevel, double fullBatteryCapacity, double weigth,
                               double aerodynamic_coef, String typeName, double latitude, double longitude) {
        Bicycle newBicycle = new Bicycle();

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_ADDBICYCLE(?, ?, ?, ?, ?, ?, ?, ?) }");
            callStmt.setString(1, description);
            callStmt.setDouble(2, weigth);
            callStmt.setString(3, typeName);
            callStmt.setDouble(4, latitude);
            callStmt.setDouble(5, longitude);
            callStmt.setDouble(6, fullBatteryCapacity);
            callStmt.setDouble(7, batteryLevel);
            callStmt.setDouble(8, aerodynamic_coef);

            callStmt.execute();
            callStmt.close();

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_BICYCLEID() }");

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int bicycleId = callStmt.getInt(1);
            newBicycle = getBicycleById(bicycleId);

        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return newBicycle;
    }

    /**
     * Method for updating a bicycle
     *
     * @param bike, the bicycle to be updated
     * @return the success of the operation
     */
    public boolean updateBicycle(Bicycle bike) {

        return updateBibycle(bike.getBicycleID(), bike.getTypeID(), bike.getParkID(), bike.getBatteryLevel(), bike.getFullBatteryCapacity(), bike.getWeight());
    }

    /**
     * Method for updating bicycle in the DB by calling PROC_UPDATEBICYCLE
     * procedure
     *
     * @param idBicycle, the bicycle id
     * @param bicycleType, bicycle type id
     * @param parkId, the park id
     * @param batteryLevel, its actual betery level
     * @param fullCapacity, its full battery capacity
     * @param weight, its weight
     * @return the success of the operation
     */
    private boolean updateBibycle(int idBicycle, int bicycleType, int parkId, double batteryLevel, double fullCapacity, double weight) {

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_UPDATEBICYCLE(?, ?, ?, ?, ?, ?) }");
            callStmt.setInt(1, idBicycle);
            callStmt.setInt(2, bicycleType);
            callStmt.setInt(3, parkId);
            callStmt.setDouble(4, batteryLevel);
            callStmt.setDouble(5, fullCapacity);
            callStmt.setDouble(6, weight);
            callStmt.execute();
            callStmt.close();

            conn.close();
            return true;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeAll();
            try {
                callStmt.close();
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Calls PROC_REMOVEBICYCLE stored procedure in order to remove a given
     * Bicycle ID from Bicycle table.
     *
     * @param bicycleId Bicycle unique id
     * @return
     */
    public boolean removeBicycle(int bicycleId) {

        try {

            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_REMOVEBICYCLE(?) }");
            callStmt.setInt(1, bicycleId);
            callStmt.execute();
            closeAll();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeAll();
            try {
                callStmt.close();
            } catch (SQLException | NullPointerException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    /**
     * Method that allows returning a bicycle to a given park 
     * 
     * @param bicycleId, the bicycle to be returned
     * @param parkId, the park where it will be returned
     * @return userId
     */

    public int returnBicycle(int bicycleId, int parkId) {
        int userId = -1;
        try {
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_RETURN_BICYCLE(?, ?) }");
            AuxMethods.sout(Configs.CONNECTED);

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.setLong(2, bicycleId);
            callStmt.setLong(3, parkId);
            callStmt.execute();
            userId = callStmt.getInt(1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return userId;
    }

    /**
     * Method that returns the available bicycles with enough (plus 10%) power
     * to travel from a given park to other (specified by user). This method uses 
     * the heaviest bicycle to estimate the necessary energy for the travel. 
     * 
     * @param usr, the user that will do the travel
     * @param originPark, the pick up park
     * @param dropPark, de destination park
     * @param windVel, the wind velocity
     * @param windFactor, the wind factor
     * @return the set of bicycles thet allows the user to travel between two parks
     */
    public Set<Bicycle> getAllAvailableBicyclesWithGivenPower(User usr, Park originPark, Park dropPark, double windVel, double windFactor) {
        Set<Bicycle> allAvailableBicycleList = getAllAvailableBicyclesOrderdByHigherBatteryLevelDb(originPark.getParkId());
        double heaviestBicycle = getHeaviestBicycle(allAvailableBicycleList);
        //uses the heaviest bicycle to estimate the energy

        DifferentRoutesBetweenTwoParksController df = new DifferentRoutesBetweenTwoParksController();
        Map<Location[],Double> map = df.mostEnergeticallyEfficientRoute(usr,originPark.getParkId(),dropPark.getParkId(),heaviestBicycle,0.99,2);
        double necessaryEnergy = map.values().iterator().next() / CONVERTION_FACTOR;

        Set<Bicycle> allAvailableBicyclesWithGivenPower = getBicyclesWithGivenPower(necessaryEnergy, allAvailableBicycleList);
        return allAvailableBicyclesWithGivenPower;
    } 

    /**
     * Method that returns the heaviest bicycle of a set of bicycles
     * 
     * @param allAvailableBicycleList, the set of bicycles
     * @return the weight of the heaviest bicycle
     */
    private double getHeaviestBicycle(Set<Bicycle> allAvailableBicycleList) {
       Iterator<Bicycle> it = allAvailableBicycleList.iterator();
       double weight =  it.next().getWeight();
       while(it.hasNext()){
           double aux =  it.next().getWeight();
           if( aux > weight){
               weight = aux;
           }
       }
       return weight;   
    }
    
    /**
     * Method that returns all the bicycles from a set of bicycles that have a 
     * certain  amount of energy (os more) 
     * 
     * @param necessaryEnergy, the energy target
     * @param allAvailableBicycleList, the set of bicycles
     * @return the group of bicycles that have at least a certain amount of energy
     */
    private Set<Bicycle> getBicyclesWithGivenPower(double necessaryEnergy, Set<Bicycle> allAvailableBicycleList){
       Iterator<Bicycle> it = allAvailableBicycleList.iterator();
       Set<Bicycle> allAvailableBicyclesWithGivenPower = new HashSet<>();
       while(it.hasNext()){
           Bicycle b =  it.next();
           double power = b.getBatteryLevel();
           if( power >= necessaryEnergy){
               allAvailableBicyclesWithGivenPower.add(b);
           }
       }
        return allAvailableBicyclesWithGivenPower;
    }
    
    /**
     * Method that returns all the electrical available bicycles at given park 
     * order by theire battery level
     * @param parkId, the park 
     * @return the grup of electrical available bicycles at a given park order 
     * by battery level
     */
    public Set<Bicycle> getAllAvailableBicyclesOrderdByHigherBatteryLevel(int parkId) {
        return getAllAvailableBicyclesOrderdByHigherBatteryLevelDb(parkId);
    }
    
    /**
     * Method that calls FUNC_GETAVAILABLEBICYCLES_BYBATTERYLEVEL function from DB
     * @param parkId, desired park id
     * @return a group of bicycles 
     */
    private Set<Bicycle> getAllAvailableBicyclesOrderdByHigherBatteryLevelDb(int parkId) {

        Set<Bicycle> higherBatteryBicycles = new HashSet<>();

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GETAVAILABLEBICYCLES_BYBATTERYLEVEL(?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setLong(2, parkId);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {
                Bicycle auxBicycle = new Bicycle(rSet.getInt(2), rSet.getInt(3), rSet.getDouble(4), rSet.getDouble(5), rSet.getDouble(6));
                boolean check = rSet.getInt(7)==0?Boolean.FALSE:Boolean.TRUE;
                auxBicycle.setAvaliability(check);
                auxBicycle.setBicycleDescription(rSet.getString(8));
                auxBicycle.setAerodynamicCoefficient(rSet.getDouble(9));
                auxBicycle.setBicycleID(rSet.getInt(1));
                higherBatteryBicycles.add(auxBicycle);

            }
            callStmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return higherBatteryBicycles;

    }

    /**
     * Method to get how many time a bicycle has been unlocked
     *
     * @param bicycleId,
     * @return time in days
     */

    public double getBicycleTimeUnlocked(int bicycleId) {
        double time=-1;
        try {
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_TIME_BIC_UNLOCK(?) }");
            AuxMethods.sout(Configs.CONNECTED);

            callStmt.registerOutParameter(1, Types.DOUBLE);
            callStmt.setInt(2, bicycleId);
            callStmt.execute();
            time = callStmt.getDouble(1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return time;
    }

    public double getBycicleRatioByType(int bycicleType){

        double ratio = 0d;

            try {
                AuxMethods.sout(Configs.CONNECTING);
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                callStmt = conn.prepareCall("{ ? = call FUNC_RETURN_BICYCLE_RATIO(?) }");
                AuxMethods.sout(Configs.CONNECTED);

                callStmt.registerOutParameter(1, Types.INTEGER);
                callStmt.setLong(2, bycicleType);

                callStmt.execute();
                ratio = callStmt.getDouble(1);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeAll();
            }
            return ratio;
        }

    public Bicycle returnOneBycicleOfType (String bycicleType){

        int type = 0;

        if("eletric".equalsIgnoreCase(bycicleType)){
            type = 1;
        }else if("mtb".equalsIgnoreCase(bycicleType) || "road".equalsIgnoreCase(bycicleType)){
            type = 2;
        }


        Bicycle b = new Bicycle();

        Set<Bicycle>allBikes = getAllAvailableBicycles();

        for(Bicycle bi : allBikes){
            if(bi.getTypeID() == type){
                b = bi;
                break;
            }
        }

        return b;

    }

    /**
     * Method to get how many time a bicycle has been unlocked
     *
     * @param bicycleDesc
     * @return time in days
     */

    public Bicycle getBicycleByDescription(String bicycleDesc) {
        int bikeId = 0;
        try {
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_BIKEID_BY_DESC(?) }");
            AuxMethods.sout(Configs.CONNECTED);

            callStmt.registerOutParameter(1, Types.DOUBLE);
            callStmt.setString(2, bicycleDesc);
            callStmt.execute();
            bikeId = callStmt.getInt(1);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return getBicycleById(bikeId);
    }



}
