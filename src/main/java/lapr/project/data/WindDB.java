package lapr.project.data;

import javafx.util.Pair;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;
import oracle.jdbc.internal.OracleTypes;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import lapr.project.model.Wind;

public class WindDB extends DataHandler{

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    static final String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    static final String USER = "LAPR3_G1";
    static final String PASS = "gebos";

    Connection conn = null;
    Statement stmt = null;
    PreparedStatement statement = null;
    CallableStatement callStmt=null;

    public Pair<Double, Double> getWindInfo(int pid1, int pid2) {
        Pair <Double, Double> windInfo = new Pair<>(0d, 0d);

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_GET_WINDINFO(?, ?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2,pid1);
            callStmt.setInt(3,pid2);
            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            // STEP 4: Extract data from result set
            while (rSet.next()) {
                windInfo = new Pair<>(rSet.getDouble(1), rSet.getDouble(2));

            }

            // STEP 5: Clean-up environment
            rSet.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return windInfo;

    }
    
    public Wind addWind(Wind wind) {
        return addWind(wind.getInitiallocationId(),wind.getFinallocationId(),wind.getWindspeed(),wind.getWindfactor(),
                wind.getAerodynamicCoefficient(), wind.getPathDirection());
    }
    
    private Wind addWind(int initiallocationId, int finallocationId, double windspeed, double windfactor,
                         double aerodynamicCoefficient, String pathDirection) {
        Wind newWind = new Wind();
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            statement = conn.prepareCall("{ call PROC_INS_UPD_WIND(?,?,?,?,?,?) }");
            statement.setLong(1, initiallocationId);
            statement.setLong(2, finallocationId);
            statement.setDouble(3, windspeed);
            statement.setDouble(4, windfactor);
            statement.setDouble(5, aerodynamicCoefficient);
            statement.setString(6, pathDirection);

            statement.execute();
            statement.close();
            conn.close();



        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_GET_WIND(?,?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, initiallocationId);
            callStmt.setInt(3, finallocationId);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                newWind = new Wind(rSet.getInt(1), rSet.getInt(2),
                        rSet.getDouble(3), rSet.getDouble(4),
                        rSet.getDouble(5), rSet.getString(6));
            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return newWind;
    }


    public int getLocationIdByCoordinates(double latitude, double longitude) {

        int locationId=0;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GET_LOCATIONID_BY_COORDINATES(?,?) }");
            callStmt.registerOutParameter(1, OracleTypes.INTEGER);
            callStmt.setDouble(2, latitude);
            callStmt.setDouble(3, longitude);

            callStmt.execute();

            locationId = callStmt.getInt(1);

            callStmt.close();
            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return locationId;
    }

    public Set<Wind> getAllWind() {

        return getAllWindFromDB();

    }

    private Set<Wind> getAllWindFromDB() {

        Set<Wind> allWind = new HashSet<>();
        Wind auxWind=null;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GETALLWIND() }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxWind = new Wind();
                auxWind.setInitiallocationId(rSet.getInt(1));
                auxWind.setFinallocationId(rSet.getInt(2));
                auxWind.setWindspeed(rSet.getDouble(3));
                auxWind.setWindfactor(rSet.getDouble(4));
                auxWind.setAerodynamicCoefficient(rSet.getDouble(5));
                auxWind.setPathDirection(rSet.getString(6));

                allWind.add(auxWind);
            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return allWind;

    }



    }