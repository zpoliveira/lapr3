/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data;


import lapr.project.model.Poi;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;
import oracle.jdbc.internal.OracleTypes;

import java.sql.*;

import java.util.HashSet;
import java.util.Set;

public class PoiDB extends DataHandler {
    
    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection conn = null;
    Statement stmt = null;
    PreparedStatement statement = null;
    CallableStatement callStmt=null;


    public Poi getPoi(int poiId) {

        int id = 0;
        String name = "";
        double latitude = 0;
        double longitude = 0;
        double altitude = 0;
        ResultSet rs=null;
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            String sql = "SELECT p.poiid, p.name, l.latitude, l.longitude, l.altitude\n" +
                         "FROM poi p, location l\n" +
                         "WHERE p.locationid = l.locationid and l.typeid = 1 and p.poiid = ?";

            statement = conn.prepareStatement(sql);

            statement.setInt(1, poiId);

            rs = statement.executeQuery();


            // STEP 4: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                id = rs.getInt("poiId");
                name = rs.getString("name");
                latitude = rs.getDouble("latitude");
                longitude = rs.getDouble("longitude");
                altitude = rs.getDouble("altitude");

            }
            // STEP 5: Clean-up environment
            rs.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        Poi p = new Poi();
        p.setPoiId(id);
        p.setName(name);
        p.setLatitude(latitude);
        p.setLongitude(longitude);
        p.setAltitude(altitude);

        return p;

    }

    public Poi addPoi(Poi poi) {
        return addPoi(poi.getName(), poi.getLatitude(), poi.getLongitude(), poi.getAltitude());
    }

    private Poi addPoi(String name, double latitude, double longitude, double altitude) {
        Poi newPoi = new Poi();
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            int poiType = 2;
            statement = conn.prepareCall("{ call PROC_ADDPOI(?,?,?,?) }");
            statement.setString(1, name);
            statement.setDouble(2, latitude);
            statement.setDouble(3, longitude);
            statement.setDouble(4, altitude);
            statement.execute();
            statement.close();
            conn.close();


            AuxMethods.sout(Configs.INSERTED);


        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }


        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_POIID() }");

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int poiID = callStmt.getInt(1);

            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_LOCATIONID() }");

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int locId = callStmt.getInt(1);

            newPoi = new Poi(name, latitude, longitude, altitude);
            newPoi.setPoiId(poiID);
            newPoi.setLocationId(locId);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return newPoi;
    }

    /**
     * Method for updating a poi
     *
     * @param poi Poi object to update
     */
    public boolean updatePoi(Poi poi) {
        return updatePoi(poi.getPoiId(), poi.getName(), poi.getLatitude(), poi.getLongitude(), poi.getAltitude());
    }

    /**
     * @param poiId    - parameter poiId
     * @param name      - parameter name
     * @param latitude  - parameter latitude
     * @param longitude - parameter longitude
     * @param altitude  - parameter altitude
     */
    private boolean updatePoi(int poiId, String name, double latitude, double longitude, double altitude) {

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_UPDATEPOI(?, ?, ?, ?, ?) }");
            callStmt.setLong(1, poiId);
            callStmt.setString(2, name);
            callStmt.setDouble(3, latitude);
            callStmt.setDouble(4, longitude);
            callStmt.setDouble(5, altitude);
            callStmt.execute();
            callStmt.close();

            conn.close();
            return true;

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return false;
    }

    public Set<Poi> getAllPoi(){

        return getAllPoiFromDB();

    }

    private Set<Poi> getAllPoiFromDB(){

        Set<Poi>allPoi = new HashSet<>();
        
        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GETALLPOI() }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                Poi auxPoi = new Poi(rSet.getString(2),rSet.getDouble(3),rSet.getDouble(4),rSet.getDouble(5));
                int index = rSet.getInt(1);
                auxPoi.setPoiId(index);
                auxPoi.setLocationId(rSet.getInt(6));

                allPoi.add(auxPoi);

            }

            conn.close();


        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return allPoi;

    }

    /**
     * Calls PROC_REMOVEPOI stored procedure in order to remove a given poi ID from Poi table.
     *
     * @param poiId Poi unique id
     */
    public boolean removePoi(int poiId) {

        try {

            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_REMOVEPOI(?) }");
            callStmt.setInt(1, poiId);
            callStmt.execute();
            closeAll();
            return true;

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return false;
    }

    public void initDbMock(){

    }
    
}
