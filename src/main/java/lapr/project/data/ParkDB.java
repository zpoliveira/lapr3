package lapr.project.data;

import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;
import oracle.jdbc.internal.OracleTypes;

import java.sql.*;

import java.util.HashSet;
import java.util.Set;

public class ParkDB extends DataHandler {

    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection conn = null;
    Statement stmt = null;
    PreparedStatement statement = null;
    CallableStatement callStmt = null;

    public Park getPark(int parkId) {

        Park auxPark = null;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_GETPARKBYID(?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, parkId);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxPark = new Park();
                auxPark.setParkId(rSet.getInt(1));
                auxPark.setName(rSet.getString(2));
                auxPark.setLatitude(rSet.getDouble(3));
                auxPark.setLongitude(rSet.getDouble(4));
                auxPark.setAltitude(rSet.getDouble(5));
                auxPark.setInputVoltage(rSet.getDouble(6));
                auxPark.setInputCurrent(rSet.getDouble(7));
                auxPark.setMaxElectricalLotation(rSet.getInt(8));
                auxPark.setMaxNonElectricalLotation(rSet.getInt(9));
                auxPark.setLocationId(rSet.getInt(10));
                auxPark.setElectricalLotation(rSet.getInt(11));
                auxPark.setNonElectricalLotation(rSet.getInt(12));

            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return auxPark;

    }

    public Park addPark(Park park) {
        return addPark(park.getName(), park.getLatitude(), park.getLongitude(), park.getAltitude(),
        park.getMaxElectricalLotation(),park.getMaxNonElectricalLotation(), park.getInputVoltage(),
                park.getInputCurrent());
    }

    private Park addPark(String name, double latitude, double longitude, double altitude,
    int maxElectricalLotation, int maxNonElectricalLotation, double inputVoltage, double inputCurrent) {

        Park newPark = new Park();
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            statement = conn.prepareCall("{ call PROC_ADDPARK(?,?,?,?,?,?,?,?) }");
            statement.setString(1, name);
            statement.setDouble(2, latitude);
            statement.setDouble(3, longitude);
            statement.setDouble(4, altitude);
            statement.setInt(5, maxElectricalLotation);
            statement.setInt(6, maxNonElectricalLotation);
            statement.setDouble(7, inputVoltage);
            statement.setDouble(8, inputCurrent);

            statement.execute();
            statement.close();
            conn.close();

            AuxMethods.sout(Configs.INSERTED);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_PARKID() }");
            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int parkID = callStmt.getInt(1);

            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_LOCATIONID() }");
            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int locID = callStmt.getInt(1);

            newPark = new Park(name, latitude, longitude, altitude, maxElectricalLotation,
                    maxNonElectricalLotation,inputVoltage, inputCurrent);

            newPark.setParkId(parkID);
            newPark.setLocationId(locID);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return newPark;
    }

    /**
     * Method for updating a park
     *
     * @param park Park object to update
     */
    public boolean updatePark(Park park) {
        return updatePark(park.getParkId(), park.getName(), park.getLatitude(), park.getLongitude(), park.getAltitude(),
                park.getElectricalLotation(), park.getNonElectricalLotation(), park.getMaxElectricalLotation(),
                park.getMaxNonElectricalLotation(), park.getInputVoltage(), park.getInputCurrent());
    }

    /**
     * @param parkId - parameter parkId
     * @param name - parameter name
     * @param latitude - parameter latitude
     * @param longitude - parameter longitude
     * @param altitude - parameter altitude
     */
    private boolean updatePark(int parkId, String name, double latitude, double longitude, double altitude,
                               int electricalLotation, int nonElectricalLotation, int maxElectricalLotation,
                               int maxNonElectricalLotation, double inputVoltage, double inputCurrent) {

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_UPDATEPARK(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
            callStmt.setLong(1, parkId);
            callStmt.setString(2, name);
            callStmt.setDouble(3, latitude);
            callStmt.setDouble(4, longitude);
            callStmt.setDouble(5, altitude);
            callStmt.setDouble(6, electricalLotation);
            callStmt.setDouble(7, nonElectricalLotation);
            callStmt.setDouble(8, maxElectricalLotation);
            callStmt.setDouble(9, maxNonElectricalLotation);
            callStmt.setDouble(10, inputVoltage);
            callStmt.setDouble(11, inputCurrent);

            callStmt.execute();
            callStmt.close();

            conn.close();
            return true;

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return false;
    }

    public Set<Park> getAllParks() {

        return getAllParksFromDB();

    }

    private Set<Park> getAllParksFromDB() {

        Set<Park> allParks = new HashSet<>();
        Park auxPark=null;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GETALLPARKS() }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxPark = new Park();
                auxPark.setParkId(rSet.getInt(1));
                auxPark.setName(rSet.getString(2));
                auxPark.setLatitude(rSet.getDouble(3));
                auxPark.setLongitude(rSet.getDouble(4));
                auxPark.setAltitude(rSet.getDouble(5));
                auxPark.setInputVoltage(rSet.getDouble(6));
                auxPark.setInputCurrent(rSet.getDouble(7));
                auxPark.setMaxElectricalLotation(rSet.getInt(8));
                auxPark.setMaxNonElectricalLotation(rSet.getInt(9));
                auxPark.setLocationId(rSet.getInt(10));
                auxPark.setElectricalLotation(rSet.getInt(11));
                auxPark.setNonElectricalLotation(rSet.getInt(12));

                allParks.add(auxPark);
            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return allParks;

    }

    /**
     * Calls PROC_REMOVEPARK stored procedure in order to remove a given park ID
     * from Parks table.
     *
     * @param parkId Park unique id
     */
    public boolean removePark(int parkId) {

        try {

            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ call PROC_REMOVEPARK(?) }");
            callStmt.setInt(1, parkId);
            callStmt.execute();
            closeAll();
            return true;

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return false;
    }

    public int avaliableSlotsInPark(String username) {
        int bicType = -1;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_AVALIABLE_SLOTS(?) }");
            callStmt.registerOutParameter(1, OracleTypes.INTEGER);
            callStmt.setString(2, username);
            callStmt.execute();
            bicType = callStmt.getInt(1);

            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return bicType;
    }

    public int getParkByCoordinates(double latitude, double longitude) {

        int parkId = 0;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GET_PARK_OR_POI_BY_COORDINATES(?,?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setDouble(2, latitude);
            callStmt.setDouble(3, longitude);
            callStmt.execute();

            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {
                parkId = rSet.getInt(1);
            }


            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return parkId;
    }
}
