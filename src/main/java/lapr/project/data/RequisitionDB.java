package lapr.project.data;

import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;

import java.sql.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import oracle.jdbc.OracleTypes;

public class RequisitionDB extends DataHandler {

    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection conn = null;
    Statement stmt = null;
    PreparedStatement statement = null;
    CallableStatement callStmt = null;

    public Requisition addRequisition(Requisition requisition) {

        return addRequisition(requisition.getUserID(), requisition.getBicycle(), requisition.getPickUpPark(), requisition.getDropOffPark());

    }

    private Requisition addRequisition(int userID, Bicycle bicycle, Park pickUpPark, Park dropOffPark) {

        Requisition req = null;
        Date requisitionDate = null;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            statement = conn.prepareStatement("INSERT INTO requisition (userID,BicycleID,pickUpPark,dropPark,initialDate) VALUES ( ?, ?, ?, ?, ?)");
            statement.setInt(1, userID);
            statement.setInt(2, bicycle.getBicycleID());
            statement.setInt(3, pickUpPark.getParkId());
            statement.setInt(4, dropOffPark.getParkId());
            requisitionDate = new Date();
            statement.setDate(5, AuxMethods.convertDateToSQL(requisitionDate));
            statement.execute();
            AuxMethods.sout(Configs.INSERTED);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_REQID() }");

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int reqID = callStmt.getInt(1);
            req = new Requisition(userID, bicycle, pickUpPark, dropOffPark, requisitionDate);
            req.setRequisitionID(reqID);

        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            closeAll();
        }

        return req;
    }

    public void initDbMock() {

    }

    public Set<Requisition> getAllRequisitionsForUser(int userId) {
        Set<Requisition> userRequisitions = new HashSet<>();
        Requisition auxRequ = null;

        try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_USERQUERITIONS(?)}");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setDouble(2, userId);

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxRequ = new Requisition();
                auxRequ.setRequisitionID(rSet.getInt(1));
                Bicycle b = new Bicycle();
                b.setBicycleDescription(rSet.getString(2));
                auxRequ.setBicycle(b);
                auxRequ.setInitialDate(rSet.getDate(3));
                auxRequ.setFinalDate(rSet.getDate(4));
                Park pickUpPark = new Park();
                pickUpPark.setLatitude(rSet.getDouble(5));
                pickUpPark.setLongitude(rSet.getDouble(6));
                pickUpPark.setAltitude(rSet.getDouble(7));
                auxRequ.setPickUpPark(pickUpPark);
                Park dropPark = new Park();
                dropPark.setLatitude(rSet.getDouble(8));
                dropPark.setLongitude(rSet.getDouble(9));
                dropPark.setAltitude(rSet.getDouble(10));
                auxRequ.setDropOffPark(dropPark);
                userRequisitions.add(auxRequ);
            }

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return userRequisitions;
    }

}
