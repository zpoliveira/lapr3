
package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import lapr.project.model.Invoice;
import lapr.project.utils.AuxMethods;
import lapr.project.utils.Configs;
import oracle.jdbc.OracleTypes;

public class InvoiceDB extends DataHandler {
    
    // JDBC driver name and database URL
    final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    final static String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

    //  Database credentials
    final static String USER = "LAPR3_G1";
    final static String PASS = "gebos";

    Connection conn = null;
    Statement stmt = null;
    PreparedStatement statement = null;
    CallableStatement callStmt = null;
    
        public Invoice getInvoice(int invoiceID) {

  
        Invoice auxInvoice = null;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_GETINVOICE(?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, invoiceID);
            

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxInvoice = new Invoice();
                auxInvoice.setInvoiceID(rSet.getInt(1));
                auxInvoice.setTotal(rSet.getDouble(2));
                auxInvoice.setDiscount(rSet.getDouble(3));
            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return auxInvoice;

    }
        

    public Invoice addInvoice(double total, double discount, int userId) {
        Invoice newInvoice = new Invoice();
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            statement = conn.prepareCall("{ call PROC_ADDINVOICE(?,?,?) }");
            statement.setDouble(1, total);
            statement.setDouble(2, discount);
            statement.setInt(3, userId);
            statement.execute();
            statement.close();
            conn.close();

            AuxMethods.sout(Configs.INSERTED);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call FUNC_MAX_INVOICEID() }");

            callStmt.registerOutParameter(1, Types.INTEGER);
            callStmt.execute();
            int invoiceId = callStmt.getInt(1);
            newInvoice = new Invoice(total,discount,userId);
            newInvoice.setInvoiceID(invoiceId);

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            callStmt = conn.prepareCall("{ ? = call PROC_UPDATEDISCOUNT() }");
            callStmt.registerOutParameter(1, Types.INTEGER);
            int invoiceId = callStmt.getInt(1);
            callStmt.setInt(1, invoiceId);
            callStmt.execute();
       
        

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }

        return newInvoice;
    } 
    
    
    public double getMonthlyPayments(int month, int userId){
    
       double amount=0;
        
             try {
            Class.forName(JDBC_DRIVER);
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            AuxMethods.sout(Configs.CONNECTED);

            callStmt = conn.prepareCall("{ ? = call FUNC_GETSUM_MONHTLYPAYMENTS(?,?) }");
            callStmt.registerOutParameter(1, OracleTypes.INTEGER);
            callStmt.setInt(2, month);
            callStmt.setInt(3, userId);
            callStmt.execute();
            amount = callStmt.getInt(1);

            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        }
        
        return amount;
    }
    
     public Invoice getInvoiceForUser(int userId, int month) {

         
        Invoice auxInvoice = null;

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            AuxMethods.sout(Configs.CONNECTING);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            AuxMethods.sout(Configs.CONNECTED);
            callStmt = conn.prepareCall("{ ? = call FUNC_GETINVOICESFORUSERANDMONTH(?,?) }");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);  
            callStmt.setInt(2, userId);
            callStmt.setInt(3, month);
            

            callStmt.execute();
            ResultSet rSet = (ResultSet) callStmt.getObject(1);

            while (rSet.next()) {

                auxInvoice = new Invoice();
                auxInvoice.setInvoiceID(rSet.getInt(1));
                auxInvoice.setTotal(rSet.getDouble(2));
                auxInvoice.setDiscount(rSet.getDouble(3));
                auxInvoice.setUserID(rSet.getInt(4));
            }

            conn.close();

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            closeAll();
        } // end try

        return auxInvoice;

    }
    
}
