/*
* A collection of graph algorithms.
*/
package lapr.project.graphbase;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author DEI-ESINF
 */

public final class GraphAlgorithms {

    /**
     * Private constructor to hide the implicit public one.
     */
    private GraphAlgorithms(){
    }

   /**
   * Computes shortest-path distance from a source vertex to all reachable 
   * vertices of a graph g with nonnegative edge weights
   * This implementation uses Dijkstra's algorithm
   * @param g Graph instance
   * @param locationOrig Vertex that will be the source of the path
   * @param visited set of discovered vertices
   * @param pathKeys minimum path vertices keys
   * @param dist minimum distances
   */
    protected static<Location, String> void shortestPathLength(Graph<Location, String> g, Location locationOrig, Location[] vertices,
                                                               boolean[] visited, int[] pathKeys, double[] dist){

        int indexOrigem = g.getKey(locationOrig);
        dist[indexOrigem] = 0;
        Double peso;

        while(indexOrigem != -1){
            visited[indexOrigem] = true;

            for(Edge<Location, String> ed : g.outgoingEdges(vertices[indexOrigem])){
                int indexDestino = g.getKey(ed.getVDest());

                    peso = ed.getWeight();

                if(!visited[indexDestino] && dist[indexDestino] > dist[indexOrigem] + peso){
                    dist[indexDestino] = (dist[indexOrigem] + peso);
                    pathKeys[indexDestino] = indexOrigem;
                }
            }

            indexOrigem = getVertMinDist(dist, visited);

        }
    }

    /**
     * Método para devolver index de estação seguinte
     * @param dist Array de distâncias
     * @param visited Array de visitados
     * @return index de estação seguinte
     */
    private static int getVertMinDist(double[] dist, boolean[] visited){

        double minDist = Double.MAX_VALUE;
        int index = -1;

        for(int i = 0; i < dist.length; i++){
            if (minDist > dist[i] && !visited[i]){
                minDist = dist[i];
                index = i;
            }
        }
        return index;
    }


    
    /**
    * Extracts from pathKeys the minimum path between voInf and vdInf
    * The path is constructed from the end to the beginning
    * @param g Graph instance
    * @param vOrig information of the Vertex origin
    * @param vDest information of the Vertex destination
    * @param pathKeys minimum path vertices keys
    * @param path stack with the minimum path (correct order)
    */
    protected static<V,E> void getPath(Graph<V,E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path){
    
       if(!vOrig.equals(vDest)){
           path.push(vDest);

           int indexDestino = g.getKey(vDest);
           int indexOrigem = pathKeys[indexDestino];

           vDest = verts[indexOrigem];

           getPath(g,vOrig,vDest,verts,pathKeys,path);
       }else{
           path.push(vOrig);
        }
    }

    //shortest-path between voInf and vdInf
    public static<V,E> double shortestPath(Graph<V,E> g, V vOrig, V vDest, List<V> shortPath){

        if(vOrig == null || vDest == null || !g.validVertex(vDest)){
            return 0;
        }

        int numVertices = g.numVertices();


        boolean[] visited = new boolean[numVertices];
        int[] pathKeys = new int[numVertices];
        double[] dist = new double[numVertices];
        V[] vertices = g.allkeyVerts();

        for(int i = 0; i < numVertices; i++){
            pathKeys[i] = -1;
            dist[i] = Double.MAX_VALUE;

        }


        shortestPathLength(g,vOrig,vertices,visited,pathKeys,dist);


        double lengthPath = dist[g.getKey(vDest)];

        if(Double.compare(lengthPath, Double.MAX_VALUE)!=0){
            getPath(g,vOrig,vDest,vertices,pathKeys,(LinkedList<V>) shortPath);
            return lengthPath;
        }


        return 0;

    }

}