/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Objects;

/**
 *
 * @author Bruno Patrão <1151052@isep.ipp.pt>
 */
public class Poi extends Location{
    
    private int poiId;
    private String name;


    /**
     * Public default constructor
     */
    public Poi(){

    }

    /**
     * Constructor with parameters
     * @param name Point of Interest name
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Poi(String name, double latitude, double longitude, double altitude) {
        super (latitude, longitude, altitude);
        this.name = name;
    }

    public int getPoiId() {
        return poiId;
    }

    public String getName() {
        return name;
    }

    public void setPoiId(int poiId) {
        this.poiId = poiId;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Poi poi = (Poi) o;
        return poiId == poi.poiId || super.getLocationId() == ((Location) o).getLocationId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), poiId);
    }
}
