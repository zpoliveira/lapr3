package lapr.project.model;

import java.util.Objects;

public class Park extends Location{

    private int parkId;
    private String name;
    private int electricalLotation;
    private int nonElectricalLotation;
    private int maxElectricalLotation;
    private int maxNonElectricalLotation;
    private double inputVoltage;
    private double inputCurrent;

    /**
     * Public default constructor
     */
    public Park(){
        super();
    }

    /**
     * Public constructur with parameters
     * @param name
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Park(String name, double latitude, double longitude, double altitude) {
        super(latitude, longitude, altitude);
        this.name = name;
    }

    /**
     * Public constructur with parameters
     * @param name
     * @param latitude
     * @param longitude
     * @param altitude
     * @param maxElectricalLotation
     * @param maxNonElectricalLotation
     * @param inputVoltage
     * @param inputCurrent
     */
    public Park(String name, double latitude, double longitude, double altitude,
                int maxElectricalLotation, int maxNonElectricalLotation, double inputVoltage, double inputCurrent) {
        super(latitude, longitude, altitude);
        this.name = name;
        this.maxElectricalLotation=maxElectricalLotation;
        this.maxNonElectricalLotation=maxNonElectricalLotation;
        this.inputVoltage=inputVoltage;
        this.inputCurrent=inputCurrent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Park park = (Park) o;
        return parkId == park.parkId || super.getLocationId() == ((Location) o).getLocationId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), parkId);
    }

    public int getParkId() {
        return parkId;
    }

    public String getName() {
        return name;
    }

    public int getMaxLotation()
    {
        return maxElectricalLotation + maxNonElectricalLotation;
    }

    public int getMaxElectricalLotation() {
        return maxElectricalLotation;
    }

    public int getMaxNonElectricalLotation() {
        return maxNonElectricalLotation;
    }

    public int getElectricalLotation() {
        return electricalLotation;
    }

    public int getNonElectricalLotation() {
        return nonElectricalLotation;
    }

    public double getInputVoltage() {
        return inputVoltage;
    }

    public double getInputCurrent() {
        return inputCurrent;
    }

    public int getAvailableElectricalSlots() {
        return maxElectricalLotation-electricalLotation;
    }

    public int getAvailableNonElectricalSlots() {
        return maxNonElectricalLotation-nonElectricalLotation;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocationId(int locationId){
        super.setLocationId(locationId);
    }

    public void setMaxElectricalLotation(int maxElectricalLotation) {
        this.maxElectricalLotation = maxElectricalLotation;
    }

    public void setMaxNonElectricalLotation(int maxNonElectricalLotation) {
        this.maxNonElectricalLotation = maxNonElectricalLotation;
    }

    public void setElectricalLotation(int electricalLotation) {
        this.electricalLotation = electricalLotation;
    }

    public void setNonElectricalLotation(int nonElectricalLotation) {
        this.nonElectricalLotation = nonElectricalLotation;
    }

    public void setInputVoltage(double inputVoltage) {
        this.inputVoltage = inputVoltage;
    }

    public void setInputCurrent(double inputCurrent) {
        this.inputCurrent = inputCurrent;
    }

    public boolean isValid(){
        return super.isValid() && this.parkId>0 && this.name!="";
    }
}

