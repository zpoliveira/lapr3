/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Comparator;
import java.util.Objects;
import lapr.project.data.BicycleDB;

/**
 *
 * @author Ferreira
 */
public class Bicycle implements Comparable<Bicycle> {

    /**
     * bicycle identifier
     */
    private int bicycleID;

    /**
     * bicycle identifier
     */
    private String bicycleDescription;
    /**
     * type of bicycle identifier
     */
    private int typeID;
    /**
     * type description
     */
    private String typeDescription;
    /**
     * park where the bicycle is located
     */
    private int parkID;
    /**
     * battery level of the electrical bicycle
     */
    private double batteryLevel;
    /**
     * full battery capacity of the electrical bicycle
     */
    private double fullBatteryCapacity;
    /**
     * weight of the bicycle
     */
    private double weight;
    /**
     * avaliability of the bicycle avaliable = true not-avaliable= false
     */
    private boolean avaliability = false;
    /**
     * aerodynamicCoefficient of the bicycle
     */
    private double aerodynamicCoefficient;

    /**
     * Public Bicycle constructor
     */
    public Bicycle() {

    }

    /**
     * Complete constructor, builds an instance of bicycle
     *
     * @param typeID
     * @param parkID
     * @param batteryLevel
     * @param fullBatteryCapacity
     * @param weight
     */
    public Bicycle( int typeID, int parkID, double batteryLevel, double fullBatteryCapacity, double weight) {
        this.typeID = typeID;
        this.parkID = parkID;
        this.batteryLevel = batteryLevel;
        this.fullBatteryCapacity = fullBatteryCapacity;
        this.weight = weight;
    }

    /**
     * Complete constructor, builds an instance of bicycle
     *
     * @param description
     * @param batteryLevel
     * @param fullBatteryCapacity
     * @param weigth
     * @param aerodynamic_coef
     */
    public Bicycle(String description, double batteryLevel, double fullBatteryCapacity, double weigth,
                   double aerodynamic_coef) {
        this.bicycleDescription = description;
        this.batteryLevel = batteryLevel;
        this.fullBatteryCapacity = fullBatteryCapacity;
        this.weight = weigth;
        this.aerodynamicCoefficient = aerodynamic_coef;
    }

    /**
     * @return the bicycleID
     */
    public int getBicycleID() {
        return bicycleID;
    }

    /**
     * @return the typeID
     */
    public int getTypeID() {
        return typeID;
    }

    /**
     * @return the parkID
     */
    public int getParkID() {
        return parkID;
    }

    /**
     * @return the batteryLevel
     */
    public double getBatteryLevel() {
        return batteryLevel;
    }

    /**
     * @return the fullBatteryCapacity
     */
    public double getFullBatteryCapacity() {
        return fullBatteryCapacity;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @return the avaliability
     */
    public boolean isAvaliability() {
        return avaliability;
    }

    /**
     * @return bicycle's description
     */
    public String getBicycleDescription() {
        return bicycleDescription;
    }

    /**
     *
     * @return bicycle's Aerodynamic Coefficient
     */
    public double getAerodynamicCoefficient() {
        return aerodynamicCoefficient;
    }

    /**
     *
     * @return bicycle's type Description
     */
    public String getTypeDescription() {
        return typeDescription;
    }

    /**
     *
     * @param bicycleDescription the bicycle description to set
     */
    public void setBicycleDescription(String bicycleDescription) {
        this.bicycleDescription = bicycleDescription;
    }

    /**
     *
     * @param aerodynamicCoefficient the bicycle aerodynamic coefficient to set
     */
    public void setAerodynamicCoefficient(double aerodynamicCoefficient) {
        this.aerodynamicCoefficient = aerodynamicCoefficient;
    }

    /**
     * @param bicycleID the bicycleID to set
     */
    public void setBicycleID(int bicycleID) {
        this.bicycleID = bicycleID;
    }

    /**
     * @param typeID the typeID to set
     */
    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    /**
     * @param parkID the parkID to set
     */
    public void setParkID(int parkID) {
        this.parkID = parkID;
    }

    /**
     * @param batteryLevel the batteryLevel to set
     */
    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    /**
     * @param fullBatteryCapacity the fullBatteryCapacity to set
     */
    public void setFullBatteryCapacity(double fullBatteryCapacity) {
        this.fullBatteryCapacity = fullBatteryCapacity;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @param avaliability the avaliability to set
     */
    public void setAvaliability(boolean avaliability) {
        this.avaliability = avaliability;
    }

    /**
     * @param typeDescription Bicycle type description
     */
    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bicycle b = (Bicycle) o;
        return bicycleID == b.bicycleID;
    }

    @Override
    public int hashCode() {

        return Objects.hash(bicycleID);
    }


    @Override
    public int compareTo(Bicycle o) {
        return this.getBicycleDescription().compareTo(o.getBicycleDescription());
    }
}
