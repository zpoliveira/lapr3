/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Objects;


public abstract class Location {
    
    private int locationId;
    private double latitude;
    private double longitude;
    private double altitude;

    private static final int MIN_LAT = -90;
    private static final int MAX_LAT = 90;
    private static final int MIN_LON = -180;
    private static final int MAX_LON = 180;

    /**
     * Private default constructor
     */
    protected Location() {
    }

    /**
     * Public constructor with parameters
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Location(double latitude, double longitude, double altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public int getLocationId() {
        return this.locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public double getLatitude(){
        return this.latitude;
    };

    public double getLongitude(){
        return this.longitude;
    };

    public double getAltitude(){
        return this.altitude;
    }

    public void setLatitude(double latitude){
        this.latitude=latitude;
    };

    public void setLongitude(double longitude){
        this.longitude=longitude;
    };

    public void setAltitude(double altitude){
        this.altitude=altitude;
    };


    public abstract boolean equals(Object o);

    /**
     * Checks of a Poi is valid for calculations
     * @return true if Poi has valid attributes
     */
    public boolean isValid(){
        if (Double.compare(this.latitude, MIN_LAT) < 0){
            return false;
        } else if(Double.compare(this.latitude, MAX_LAT) > 0){
            return false;
        } else if (Double.compare(this.longitude, MIN_LON) < 0){
            return false;
        } else if (Double.compare(this.longitude, MAX_LON) > 0){
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(locationId);
    }


}
