/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author anacerqueira
 */
public class Payment {
    
    private int paymentId;
    
    private int userId;
    
    private int serviceTypeId;
    
    private int invoiceId;
           
    private Date date;
    
    private double amount;
    
    
    public Payment ( int userId, int serviceType, int invoiceId, Date date){
        
        this.userId = userId;
        this.serviceTypeId = serviceType;
        this.invoiceId = invoiceId;
        this.date = date;
    }

    public Payment() {
        
    }

    public int getPaymentId() {
        return paymentId;
    }

    public int getUserId() {
        return userId;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.paymentId;
        return hash;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (this.paymentId != other.paymentId) {
            return false;
        }
      
        return true;
    }
    
    
    
}
