package lapr.project.model;

import java.util.Objects;

public class User {

    /**
     *
     * Name variable for User
     *
     */
    private int userId;

    /**
     *
     * Name variable for User
     *
     */
    private String name;

    /**
     *
     * Vat number variable for User
     *
     */
    private int nif;

    /**
     *
     * Birthday date variable for User
     *
     */
    private String birthDate;

    /**
     *
     * Userpoints variable for User
     *
     */
    private int userPoints;

    /**
     *
     * Height variable for User
     *
     */
    private double heigth;

    /**
     *
     * Weight variable for User
     *
     */
    private double weight;

    /**
     *
     * Credit Card Numver variable for User
     *
     */
    private long creditCardNumber;

    /**
     *
     * Email variable for User
     *
     */
    private String email;

    /**
     *
     * State variable for User
     *
     */
    private int state;

    /**
     * The average speed of the user
     */
    private double averageSpeed;

    /**
     * Constructor of User class
     *
     * @param name name variable for User
     * @param nif vat number variable for User
     * @param birthDate birth date variable for User
     * @param heigth height variable for User
     * @param weight weight variable for User
     * @param creditCardNumber credit card number variable for User
     * @param email email variable for User
     */
    public User(String name, int nif, String birthDate, double heigth, double weight, long creditCardNumber,
            String email) {
        this.name = name;
        this.nif = nif;
        this.birthDate = birthDate;
        this.userPoints = 0;
        this.heigth = heigth;
        this.weight = weight;
        this.email = email;
        this.creditCardNumber = creditCardNumber;
        this.state = 0;
    }

    /**
     * Constructor of User classs
     *
     * @param username
     * @param nif
     * @param birthDate
     * @param heigth
     * @param weight
     * @param creditCardNumber
     * @param email
     * @param averageSpeed
     */
    public User(String username, int nif, String birthDate, double heigth, double weight, long creditCardNumber,
            String email, double averageSpeed) {
        this.name=username;
        this.nif = nif;
        this.birthDate = birthDate;
        this.userPoints = 0;
        this.heigth = heigth;
        this.weight = weight;
        this.email = email;
        this.creditCardNumber = creditCardNumber;
        this.state = 0;
        this.averageSpeed = averageSpeed;
    }

    /**
     * Public default constructor
     */
    public User() {

    }

    /**
     * Get method for user id
     *
     * @return userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Get method for user's points
     *
     * @return userPoints
     */
    public int getUserPoints() {
        return userPoints;
    }

    /**
     * Get method for user's average speed
     *
     * @return average speed
     */
    public double getUserAverageSpeed() {
        return averageSpeed;
    }

    /**
     * Set method for user id
     *
     * @return userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Set method for state
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * Get method for user name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get method for user nif
     *
     * @return nif
     */
    public int getNif() {
        return nif;
    }

    /**
     * Get method for user birthdate
     *
     * @return birthdate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Get method for user height
     *
     * @return height
     */
    public double getHeigth() {
        return heigth;
    }

    /**
     * Get method for user weight
     *
     * @return weigth
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Get method for user creditcardnumber
     *
     * @return creditcardnumber
     */
    public long getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * Get method for user's Points
     *
     * @return userPoints
     */
    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }

    /**
     * Get method for user email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Get method for user state
     *
     * @return state
     */
    public int getState() {
        return state;
    }

    /**
     * Validate User information
     *
     * @return
     */
    public boolean isValid() {
        return getState() == 1
                && getHeigth() > 0 && getWeight() > 0 && !getBirthDate().equals("");
    }

    /**
     * Equals method
     *
     * @return true in case of equality, false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return userId == user.userId
                && nif == user.nif;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, nif);
    }
}
