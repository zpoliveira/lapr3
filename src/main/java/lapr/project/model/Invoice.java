package lapr.project.model;


public class Invoice {
    
   /**
    * invoice identifier
    */
    private int invoiceID;
    /**
     * user identifier
     */
    private int userID;
    /**
     * total in euros
     */
    private double total;
    
    /**
     * discount - points converted in euros
     */
    private double discount;
    /**
     * Complete constructor, builds an instance of Invoice
     * @param total 
     */
    
    public Invoice (double total, double discount, int userID){
        this.total=total;
        this.discount=discount;
        this.userID=userID;
    }

    /**
     * Empty constructor
     */
    public Invoice() {    }

    /**
     * @return the invoiceID
     */
    public int getInvoiceID() {
        return invoiceID;
    }

    /**
     * @param invoiceID the invoiceID to set
     */
    public void setInvoiceID(int invoiceID) {
        this.invoiceID = invoiceID;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the discount
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    /**
     * @return the userID
     */
    public int getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    
}
