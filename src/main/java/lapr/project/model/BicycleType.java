/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

/**
 *
 * @author anacerqueira
 */
public class BicycleType {
    
    int typeId;
    String description;
    double workRatio;
    
    public BicycleType (int typeId, String description, double workRatio){
        this.typeId = typeId;
        this.description = description;
        this.workRatio =workRatio;
    }
    
     public BicycleType (){
     
     }

    public int getTypeId() {
        return typeId;
    }

    public String getDescription() {
        return description;
    }

    public double getWorkRatio() {
        return workRatio;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setWorkRatio(double workRatio) {
        this.workRatio = workRatio;
    }

    public boolean isValid(){
        return this.description!="" && getWorkRatio()>0;
    }

}
