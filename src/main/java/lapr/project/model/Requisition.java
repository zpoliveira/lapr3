package lapr.project.model;


import java.util.Date;
import java.util.Objects;

public class Requisition {

    /**
     * Variable requisition id number
     */
    private int requisitionID;

    /**
     * Variable user id number
     */
    private int userID;

    /**
     * Variable bicycle
     */
    private Bicycle bicycle;

    /**
     * Vaiable pickupPark
     */
    private Park pickUpPark;

    /**
     * variable dropOffPark
     */
    private Park dropOffPark;

    /**
     * Variable pickup Date
     */
    private Date initialDate;

    /**
     * Variable dropOff date
     */
    private Date finalDate;

    /**
     * Empty constructor
     */
    public Requisition() {

    }

    /**
     * Contructor
     * @param userID userID
     * @param bicycle bicycleID
     * @param pickUpPark pickUpPark
     * @param dropOffPark dropOffPark
     * @param initialDate pick Up date
     * @param finalDate drop off date
     */
    public Requisition(int userID, Bicycle bicycle, Park pickUpPark, Park dropOffPark, Date initialDate, Date finalDate) {

        this.userID = userID;
        this.bicycle = bicycle;
        this.pickUpPark = pickUpPark;
        this.dropOffPark = dropOffPark;
        this.initialDate = initialDate;
        this.finalDate = finalDate;
    }
///**
// * Constructor
// * @param userID
// * @param bicycle
// * @param pickUpPark
// * @param initialDate
// * @param finalDate 
// */
//       public Requisition(int userID, Bicycle bicycle, Park pickUpPark, Date initialDate, Date finalDate) {
//
//        this.userID = userID;
//        this.bicycle = bicycle;
//        this.pickUpPark = pickUpPark;
//        this.initialDate = initialDate;
//        this.finalDate = finalDate;
//    }
    
    /**
     * Contructor
     * @param userID userID
     * @param bicycle bicycle
     * @param pickUpPark pickUpPark
     * @param initialDate pick Up date
     */
    public Requisition(int userID, Bicycle bicycle, Park pickUpPark, Date initialDate) {
        this.userID = userID;
        this.bicycle = bicycle;
        this.pickUpPark = pickUpPark;
        this.initialDate = initialDate;
    }

    /**
     * Contructor
     * @param userID userID
     * @param bicycle bicycle
     * @param pickUpPark pickUpPark
     * @param dropOffPark dropOffPark
     * @param initialDate pick Up date
     */
    public Requisition(int userID, Bicycle bicycle, Park pickUpPark, Park dropOffPark, Date initialDate) {
        this.userID = userID;
        this.bicycle = bicycle;
        this.pickUpPark = pickUpPark;
        this.dropOffPark = dropOffPark;
        this.initialDate = initialDate;
    }

    /**
     * Get method for requisitionID
     * @return requisitionID
     */
    public int getRequisitionID() {
        return requisitionID;
    }

    /**
     * Get method for userID
     * @return userID
     */
    public int getUserID() {
        return userID;
    }

    /**
     * Get method for bicycleID
     * @return bicycleID
     */
    public Bicycle getBicycle() {
        return bicycle;
    }

    /**
     * Get method for pickUp park
     * @return pickUp Park
     */
    public Park getPickUpPark() {
        return pickUpPark;
    }

    /**
     * Get method for dropOff park
     * @return dropOff park
     */
    public Park getDropOffPark() {
        return dropOffPark;
    }

    /**
     * Get method for pickUpdate
     * @return pickUp date
     */
    public Date getInitialDate() {
        return initialDate;
    }

    /**
     * Get method for dropOff date
     * @return dropOff date
     */
    public Date getFinalDate() {
        return finalDate;
    }

    /**
     * Set method for requisitionID
     * @param requisitionID requisitionID
     */
    public void setRequisitionID(int requisitionID) {
        this.requisitionID = requisitionID;
    }

    /**
     * Set method for userID
     * @param userID userID
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * Set method for bicycle
     * @param bicycle bicycle
     */
    public void setBicycle(Bicycle bicycle) {
        this.bicycle = bicycle;
    }

    /**
     * Set method for pickUp park
     * @param pickUpPark pickUp park
     */
    public void setPickUpPark(Park pickUpPark) {
        this.pickUpPark = pickUpPark;
    }

    /**
     * Set method for dropOff park
     * @param dropOffPark dropOff park
     */
    public void setDropOffPark(Park dropOffPark) {
        this.dropOffPark = dropOffPark;
    }

    /**
     * Set method for pickUp date
     * @param initialDate pickUp date
     */
    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    /**
     * Set method for dropOff date
     * @param finalDate dropOff date
     */
    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    /**
     * Equals method
     * @param o object
     * @return true if equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Requisition that = (Requisition) o;
        return requisitionID == that.requisitionID;
    }

    /**
     * Hashhcode method
     * @return hashcode
     */
    @Override
    public int hashCode() {

        return Objects.hash(requisitionID);
    }

}
