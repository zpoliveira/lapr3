package lapr.project.model;

import java.util.Objects;

public class Wind {

    private int initiallocationId;
    private int finallocationId;
    private double windspeed;
    private double windfactor;
    private double aerodynamicCoefficient;
    private String pathDirection;

    /**
     * Default constructor
     */
    public Wind(){

    }

     /**
     * Constructor with parameters
     * @param initiallocationId initial location id
     * @param finallocationId final location id
     * @param windspeed wind speed 
     * @param windfactor wind factor
     */
    public Wind(int initiallocationId, int finallocationId, double windspeed, double windfactor,
                double aerodynamicCoefficient, String pathDirection) {

        this.initiallocationId = initiallocationId;
        this.finallocationId = finallocationId;
        this.windspeed = windspeed;
        this.windfactor = windfactor;
        this.aerodynamicCoefficient=aerodynamicCoefficient;
        this.pathDirection=pathDirection;
    }

    public int getInitiallocationId() {
        return initiallocationId;
    }

    public int getFinallocationId() {
        return finallocationId;
    }

    public double getWindspeed() {
        return windspeed;
    }

    public double getWindfactor() {
        return windfactor;
    }

    public double getAerodynamicCoefficient() {
        return aerodynamicCoefficient;
    }

    public String getPathDirection() {
        return pathDirection;
    }

    public void setInitiallocationId(int initiallocationId) {
        this.initiallocationId = initiallocationId;
    }

    public void setFinallocationId(int finallocationId) {
        this.finallocationId = finallocationId;
    }

    public void setWindspeed(double windspeed) {
        this.windspeed = windspeed;
    }

    public void setWindfactor(double windfactor) {
        this.windfactor = windfactor;
    }

    public void setAerodynamicCoefficient(double aerodynamicCoefficient) {
        this.aerodynamicCoefficient = aerodynamicCoefficient;
    }

    public void setPathDirection(String pathDirection) {
        this.pathDirection = pathDirection;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        final Wind other = (Wind) obj;
        if (this.initiallocationId != other.initiallocationId) {
            return false;
        }
        if (this.finallocationId != other.finallocationId) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(initiallocationId, finallocationId);
    }
   
}
