package lapr.project.utils;

public final class Configs {



    /**
     * Variable for use of MockDB
     */
    public static final boolean DEBUG_MODE = true;

    /**
     * Variable for printing connecting to a DB.
     */
    public static final String CONNECTING = "Connecting to a selected database...";

    /**
     * Variable success of printing connecting to a DB.
     */
    public static final String CONNECTED = "Connected database successfully...";

    /**
     * Variable success of inserting records on a DB.
     */
    public static final String INSERTED = "Inserted records into the table...";

    /**
     * Variable email configured.
     */
    public static final String EMAIL = "grupo1lapr3@gmail.com";

    /**
     * Variable email's configuration.
     */
    public static final String EMAIL_CONF = "Gebos2018";

    /**
     * Variable email's smtp configured.
     */
    public static final String EMAIL_SMTP_CONFIG = "mail.smtp.host";

    /**
     * Variable email's smtp configured.
     */
    public static final String EMAIL_SMTP = "smtp.gmail.com";

    /**
     * Variable email's smtp port configured.
     */
    public static final String EMAIL_SMTP_CONFIG_PORT = "mail.smtp.port";

    /**
     * Variable email's smtp TLS port configured.
     */
    public static final String EMAIL_SMTP_PORT = "587";

    /**
     * Variable email's smtp auth configured.
     */
    public static final String EMAIL_SMTP_CONFIG_AUTH = "mail.smtp.auth";

    /**
     * Variable email's smtp auth configured.
     */
    public static final String EMAIL_SMTP_AUTH = "true";

    /**
     * Variable email's smtp startTLS configured.
     */
    public static final String EMAIL_SMTP_CONFIG_STARTTLS = "mail.smtp.starttls.enable";

    /**
     * Variable email's smtp startTLS configured.
     */
    public static final String EMAIL_SMTP_STARTTLS = "true";

    /**
     * Variable email's body.
     */
    public static final String EMAIL_BODY = ", you have successfully locked your bicycle!";

    /**
     * Variable email's subject.
     */
    public static final String EMAIL_SUBJECT = "Bicycle locking confirmation";

    /**
     * Variable every park's current value.
     */
    public static final int PARK_CURRENT = 16;

    /**
     * Variable every park's current tension.
     */
    public static final int PARK_TENSION = 220;

    /**
     * Variable every park slot's potency.
     */
    public static final int PARK_SLOT_POTENCY = 3;

    /**
     * Variable every bicycle's battery capacity.
     */
    public static final int BICYCLE_BATTERY_CAPACITY = 1;

    /**
     * Private constructor to hide the implicit public one
     */
    private Configs(){

    }

}
