package lapr.project.utils;

import lapr.project.model.Bicycle;
import lapr.project.model.Location;
import lapr.project.model.Park;


import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import lapr.project.model.Invoice;
import lapr.project.model.Requisition;
import lapr.project.model.User;


public final class ExportFiles {

    private int lineCount;
    File file;

    private static final String DELIMITER =";";
    private static final String SEPARATOR ="line.separator";
    private static final Boolean FILE_APPEND = Boolean.TRUE;
    private static final String PATH = "exportFolder/";

    public ExportFiles(){
        this.lineCount=0;
    }

    public int bicycles(String outputFile, Set<Bicycle> setBicycles) {

        Iterator <Bicycle> iterator = setBicycles.iterator();
        while (iterator.hasNext()){
            StringBuilder stringBuilder = new StringBuilder();
            Bicycle bic = iterator.next();
            stringBuilder.append(bic.getBicycleDescription());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(bic.getTypeDescription());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(bic.getBatteryLevel());
            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
        return this.lineCount;
    }

    private void writeFile(String outputFile, StringBuilder stringBuilder) {
        file = null;
        FileWriter fileOutputStream = null;
        try {
            file=new File(PATH + outputFile);
            if(this.lineCount==0) {
                if (file.delete()){
                    AuxMethods.sout(outputFile+" file deleted!");
                };
            }
            fileOutputStream = new FileWriter(file,FILE_APPEND);
            fileOutputStream.write(stringBuilder.toString()+"\n");
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException | NullPointerException e ) {
                e.printStackTrace();
            }
        }


    }

    public int mapParkDouble(String outputFile, Map<Park,Double> map) {

        map = AuxMethods.OrderMapByValue(map);


        Iterator <Map.Entry<Park,Double>> iterator = map.entrySet().iterator();



        while (iterator.hasNext()){

            Map.Entry<Park,Double> en = iterator.next();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(en.getKey().getLatitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(en.getKey().getLongitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(en.getValue());
            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
        return this.lineCount;
    }

    public int exportPath(String outputFile, Map<Location[],Double> result, int choice) {


        Iterator <Map.Entry<Location[],Double>> iterator = result.entrySet().iterator();
        double energy = 0d;
        double distance = 0d;

        while (iterator.hasNext()){

            Map.Entry<Location[],Double> en = iterator.next();
            Location[] aux = en.getKey();
        if (aux.length==0){
            return 0;
        }
            if(choice == 1){
                distance = Math.round(en.getValue()*1000);
            }else if(choice == 2){
                energy = en.getValue();
            }

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Path ");
            stringBuilder.append(lineCount + 1);
            stringBuilder.append(System.getProperty(SEPARATOR));
            stringBuilder.append("total_distance : ");
            stringBuilder.append(distance);
            stringBuilder.append(System.getProperty(SEPARATOR));
            stringBuilder.append("total_energy : ");
            stringBuilder.append(energy);
            stringBuilder.append(System.getProperty(SEPARATOR));
            stringBuilder.append("elevation : ");
            stringBuilder.append(aux[0].getAltitude()-aux[aux.length-1].getAltitude());
            stringBuilder.append(System.getProperty(SEPARATOR));

            for(int i = 0; i < aux.length ; i++){

                stringBuilder.append(aux[i].getLatitude());
                stringBuilder.append(DELIMITER);
                stringBuilder.append(aux[i].getLongitude());
                stringBuilder.append(System.getProperty(SEPARATOR));
            }

            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
        return this.lineCount;
    }

    public int exportChargeReport(String outputFile, Map<Bicycle, String> setBikes){
        Iterator <Map.Entry<Bicycle,String>> iterator = setBikes.entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<Bicycle, String> temporarySet = iterator.next();
            Bicycle bike = temporarySet.getKey();
            String time = temporarySet.getValue();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(bike.getBicycleDescription());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(bike.getBatteryLevel());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(time);
            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
        return this.lineCount;
    }
public int points(String outputFile, Set<Requisition> setRequisition) {
        lineCount=0;
        Iterator<Requisition> iterator = setRequisition.iterator();
        while (iterator.hasNext()) {
            StringBuilder stringBuilder = new StringBuilder();
            Requisition req = iterator.next();
            Bicycle b = req.getBicycle();
            stringBuilder.append(b.getBicycleDescription());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(req.getInitialDate());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(req.getFinalDate());
            stringBuilder.append(DELIMITER);
            Park oP = req.getPickUpPark();
            stringBuilder.append(oP.getLatitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(oP.getLongitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(oP.getAltitude());
            Park dP = req.getDropOffPark();
            stringBuilder.append(dP.getLatitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(dP.getLongitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(dP.getAltitude());
            double elevation = Math.abs(dP.getAltitude() - oP.getAltitude());
            stringBuilder.append(DELIMITER);
            stringBuilder.append(elevation);
            double pointsEarned = 0;
            if (Double.compare(dP.getAltitude(), oP.getAltitude()) > 0) {
                pointsEarned = AuxMethods.earnedPoints(elevation);
            }
            stringBuilder.append(DELIMITER);
            stringBuilder.append(pointsEarned);

            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
        return this.lineCount;
    }

    public int exportInvoice(String outputFile, Set<Requisition> setRequisition, String userName, int userPoits, Invoice invoice) {
         lineCount=0; 
         Iterator<Requisition> iterator = setRequisition.iterator();
        while (iterator.hasNext()) {
            
            
            StringBuilder stringBuilder = new StringBuilder();
            Requisition req = iterator.next();
            Bicycle b = req.getBicycle();
            Park oP = req.getPickUpPark();
            Park dP = req.getDropOffPark();
//            Invoice invoice = new Invoice();
//            invoice.setInvoiceID(invoiceId);
//            UserDB user = new UserDB();
//            user.setUserId(userId);

            double elevation = Math.abs(dP.getAltitude() - oP.getAltitude());
            double pointsEarned = 0;
            if (Double.compare(dP.getAltitude(), oP.getAltitude()) > 0) {
                pointsEarned = AuxMethods.earnedPoints(elevation);
            }
            
            double previousPoints =0;
            previousPoints = userPoits-pointsEarned;

            double discountPoints = 0;
            discountPoints = invoice.getDiscount()*10;
                       
//            double actualPoints =0;
//            actualPoints = user.getUserPoints();
      
            double chargedValue = 0;
            chargedValue = invoice.getTotal()-invoice.getDiscount();
            
//            String userName =user.getName();

            
            stringBuilder.append(userName);
            stringBuilder.append(System.getProperty(SEPARATOR));

            stringBuilder.append(previousPoints);
            stringBuilder.append(System.getProperty(SEPARATOR));
            
            stringBuilder.append(pointsEarned);
            stringBuilder.append(System.getProperty(SEPARATOR));            

            stringBuilder.append(discountPoints);
            stringBuilder.append(System.getProperty(SEPARATOR)); 
     
            stringBuilder.append(userPoits);
            stringBuilder.append(System.getProperty(SEPARATOR)); 
          
            stringBuilder.append(Math.round(chargedValue));
            stringBuilder.append(System.getProperty(SEPARATOR)); 

            
            stringBuilder.append(b.getBicycleDescription());
            stringBuilder.append(DELIMITER);
           
            stringBuilder.append(req.getInitialDate());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(req.getFinalDate());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(oP.getLatitude());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(oP.getLongitude());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(dP.getLatitude());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(dP.getLongitude());
            stringBuilder.append(DELIMITER);
            
            stringBuilder.append(chargedValue);
   

           
            writeFile(outputFile, stringBuilder);
            this.lineCount++;
        }
    
        return this.lineCount;

    }

}
