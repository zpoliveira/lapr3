package lapr.project.utils;

import lapr.project.data.*;
import lapr.project.model.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ImportFiles {

    private ParkDB parkDB;
    private PoiDB poiDB;
    private WindDB windDB;
    private BicycleDB bicycleDB;
    private UserDB userDB;

    private static final String CHAR_IGNORE = "#";
    private static final String DELIMITER = ";";

    public ImportFiles() {
        this.parkDB = new ParkDB();
        this.poiDB = new PoiDB();
        this.bicycleDB = new BicycleDB();
        this.windDB = new WindDB();
        this.userDB = new UserDB();

    }

    public int importParks(String fileName) {
        int cont = 0;
        Park p;
        Scanner flIn = null;
        try {
            flIn = new Scanner(new File(fileName));
            while (flIn.hasNext()) {
                String line = flIn.nextLine();
                if (!line.substring(0, 1).equals(CHAR_IGNORE)) {
                    String[] temp = line.split(DELIMITER);
                    double latitude = Double.parseDouble(temp[0]);
                    double longitude = Double.parseDouble(temp[1]);
                    double altitude = Double.parseDouble(temp[2]);
                    String name = temp[3];
                    int maxElectricalLotation = Integer.parseInt(temp[4]);
                    int maxNonElectricalLotation = Integer.parseInt(temp[5]);
                    double inputVoltage = Integer.parseInt(temp[6]);
                    double inputCurrent = Integer.parseInt(temp[7]);
                    p = new Park(name, latitude, longitude, altitude, maxElectricalLotation, maxNonElectricalLotation, inputVoltage, inputCurrent);
                    parkDB.addPark(p);
                    cont++;
                }
            }
            flIn.close();
        } catch (FileNotFoundException fe) {
            fe.toString();
        } finally {
            if (flIn != null) {
                flIn.close();
            }
        }
        return cont;
    }

    public int importBicycles(String fileName) {
        int cont = 0;
        Scanner fileInput = null;
        try {
            fileInput = new Scanner(new File(fileName));
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                String x = line.substring(0, 1);
                if (!x.equals(CHAR_IGNORE)) {
                    String[] temp = line.split(DELIMITER);
                    String desc = temp[0];
                    Double weigth = Double.parseDouble(temp[1]);
                    String typeName = temp[2];
                    Double latitude = Double.parseDouble(temp[3]);
                    Double longitude = Double.parseDouble(temp[4]);
                    Double fullBtCapacity = Double.parseDouble(temp[5]);
                    Double btLevel = Double.parseDouble(temp[6]);
                    Double aero = Double.parseDouble(temp[7]);
                    Bicycle bic = new Bicycle(desc, btLevel, fullBtCapacity, weigth, aero);
                    bicycleDB.addBicycle(bic, desc, latitude, longitude);
                    cont++;
                }
            }

        } catch (FileNotFoundException f) {
            f.toString();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
        return cont;
    }

    public int importPois(String fileName) {
        int cont = 0;
        Scanner fileInput = null;
        try {
            fileInput = new Scanner(new File(fileName));
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                String x = line.substring(0, 1);
                if (!x.equals(CHAR_IGNORE)) {
                    String[] temp = line.split(DELIMITER);
                    double latitude = Double.parseDouble(temp[0]);
                    double longitude = Double.parseDouble(temp[1]);
                    double altitude = Double.parseDouble(temp[2]);
                    String name = temp[3];
                    Poi p = new Poi(name, latitude, longitude, altitude);
                    poiDB.addPoi(p);
                    cont++;
                }
            }
        } catch (FileNotFoundException f) {
            f.toString();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
        return cont;
    }

    public int importPaths(String fileName) {

        int locationOrigin;
        int locationDest;
        String pathType;
        String[] arrayAux;
        String linhaAux;
        double latitudeA;
        double longitudeA;
        double latitudeB;
        double longitudeB;
        double aeroCoef;
        double windDir;
        double windSpeed;
        int cont = 0;
        Scanner flIn = null;
        try {
            flIn = new Scanner(new File(fileName));
            while (flIn.hasNext()) {

                linhaAux = flIn.nextLine();
                String x = linhaAux.substring(0, 1);
                if (!x.equals(CHAR_IGNORE)) {
                    arrayAux = linhaAux.split(DELIMITER);

                    latitudeA = Double.parseDouble(arrayAux[0]);
                    longitudeA = Double.parseDouble(arrayAux[1]);
                    latitudeB = Double.parseDouble(arrayAux[2]);
                    longitudeB = Double.parseDouble(arrayAux[3]);
                    pathType = arrayAux[4];
                    aeroCoef = Double.parseDouble(arrayAux[5]);
                    windDir = Double.parseDouble(arrayAux[6]);
                    windSpeed = Double.parseDouble(arrayAux[7]);

                    locationOrigin = windDB.getLocationIdByCoordinates(latitudeA, longitudeA);
                    locationDest = windDB.getLocationIdByCoordinates(latitudeB, longitudeB);
                    Wind newWind = new Wind(locationOrigin, locationDest, windSpeed, windDir, aeroCoef, pathType);
                    windDB.addWind(newWind);
                    cont++;
                }
            }

        } catch (FileNotFoundException fe) {
            fe.toString();
        } finally {
            if (flIn != null) {

                flIn.close();
            }
        }

        return cont;
    }

    public List<Location> importIntermediatePois(String fileName) {

        List<Location> intermediatePoi = new ArrayList<>();
        Scanner fileInput = null;
        try {
            fileInput = new Scanner(new File(fileName));
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                String x = line.substring(0, 1);
                if (!x.equals(CHAR_IGNORE)) {
                    String[] temp = line.split(DELIMITER);
                    double latitude = Double.parseDouble(temp[0]);
                    double longitude = Double.parseDouble(temp[1]);
                    double altitude = Double.parseDouble(temp[2]);
                    String name = temp[3];
                    Poi p = new Poi(name, latitude, longitude, altitude);
                    Set<Poi> setPois = poiDB.getAllPoi();
                    for (Poi poi : setPois){
                        if (Double.compare(poi.getLatitude(),p.getLatitude())==0 
                                && Double.compare(poi.getLongitude(), p.getLongitude())==0){
                            intermediatePoi.add(poi);
                        }
                    }

                }
            }
        } catch (FileNotFoundException f) {
            f.toString();
        } finally {
            if (fileInput != null) {

                fileInput.close();
            }
        }
        return intermediatePoi;
    }

    public int importUsers(String fileName) {
        int cont = 0;
        Scanner fileInput = null;
        try {
            fileInput = new Scanner(new File(fileName));
            while (fileInput.hasNext()) {
                String line = fileInput.nextLine();
                String x = line.substring(0, 1);
                if (!x.equals(CHAR_IGNORE)) {
                    String[] temp = line.split(DELIMITER);
                    String username = temp[0];
                    String email = temp[1];
                    Double height = Double.parseDouble(temp[2]);
                    Double weigth = Double.parseDouble(temp[3]);
                    Double averageSpeed = Double.parseDouble(temp[4]);
                    long creditCardNumber = Long.parseLong(temp[5]);
                    int nif = 999999999;
                    String birthDate = "1900-01-01";
                    User user = userDB.addUser(username, nif, birthDate, height, weigth, creditCardNumber, email, averageSpeed);
                    if (user.getUserId()!=0){
                        cont++;
                    }
                }
            }
            fileInput.close();
        } catch (FileNotFoundException f) {
            f.toString();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
        return cont;

    }

}
