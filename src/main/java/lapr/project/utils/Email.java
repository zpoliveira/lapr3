package lapr.project.utils;

import lapr.project.model.User;
import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {

    Properties props;

    /**
     * Email's constructor that iniciates the SMTP configuration.
     *
     */
    public Email(){
        props = new Properties();
        props.put(Configs.EMAIL_SMTP_CONFIG, Configs.EMAIL_SMTP);
        props.put(Configs.EMAIL_SMTP_CONFIG_PORT, Configs.EMAIL_SMTP_PORT);
        props.put(Configs.EMAIL_SMTP_CONFIG_AUTH, Configs.EMAIL_SMTP_AUTH);
        props.put(Configs.EMAIL_SMTP_CONFIG_STARTTLS, Configs.EMAIL_SMTP_STARTTLS);
    }

    /**
     * Public method to sent an email to a User
     *
     * @param user
     * @return success of operation
     */
    public boolean sendEmail(User user){
        return sendEmail(user.getName(), user.getEmail());
    }

    /**
     * Private method to send a email to a User
     *
     * @param name - name of User
     * @param email - email of User
     * @return success of operation
     */
    private boolean sendEmail(String name, String email){
        final String fromEmail = Configs.EMAIL;
        final String password = Configs.EMAIL_CONF;

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
        Session session = Session.getInstance(props, auth);
        return sendEmailService(session, email, Configs.EMAIL_SUBJECT ,name + Configs.EMAIL_BODY);
    }

    /**
     * private method to send a email to a User.
     *
     * @param session - session with the smtp parameters
     * @param toEmail - destination email
     * @param body - text of email
     * @param subject - subject of email
     */
    private static boolean sendEmailService(Session session, String toEmail, String subject ,String body){
        try{
            MimeMessage msg = new MimeMessage(session);
            msg.setText(body, "UTF-8");
            if(!validateEmail(toEmail)){
               throw new Exception();
            }
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            if(!Configs.DEBUG_MODE) {
                Transport.send(msg);
            }
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @param email
     * @return
     * @throws AddressException
     */
    private static boolean validateEmail(String email) throws AddressException {
        return !email.equals("");
    }
}