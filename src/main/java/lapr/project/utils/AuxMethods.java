package lapr.project.utils;

import lapr.project.graphbase.Graph;
import lapr.project.graphbase.GraphAlgorithms;
import lapr.project.model.Bicycle;
import lapr.project.model.Location;
import lapr.project.model.Park;
import lapr.project.model.User;
import java.util.*;

public final class AuxMethods {

    //Earth Radius
    private static final double EARTH_RADIUS = 6373;
    //Average ciclyng velocity in m/s
    // private static final double AVERAGE_VELOCITY = 6.25; // vem da base de dados
    //Gravitational accelaration force
    private static final double G_FORCE = 9.8;

    //Frictional losses constant
    private static final double K1 = 0.0053;//vem da base de dados
    //Aerodynamic drag constant
    private static final double K2 = 0.185; //vem da base de dados 

    //Constant to convert to Kwh
    private static final double CONV_CONSTANT = 1000;
    //Percentage constant
    private static final double PERC_CONSTANT = 100;
    //total seconds in an hour
    private static final int HOUR_SEC_CONSTANT = 3600;
    //total seconds in a minute
    private static final int SEC_CONSTANT = 60;
    //
    private static final int ONE_SLOT = 1;
    //Constant to convert cal to Kwh
    private static final double CONVERTION_FACTOR = 860420.65;
    private static final int ASCENDING_SORTING = 1;
    private static final int DESCENDING_SORTING = 2;
    private static final int SECONDS_PER_DAY = 86400;

    /**
     * Private constructor to hide the implicit public one.
     */
    private AuxMethods() {

    }

    /**
     * *********************************************
     ******************* PHYSICS *******************
     * *********************************************
     */
    /**
     * Get distance between a given coordinate and a Park
     *
     * @param latitude1 point latitude
     * @param longitude1 point longitude
     * @param altitude1 point altitude
     * @param park Park object
     * @return distance between a given geographical point and a park in Km
     * considering altitudes
     */
    public static double getDistanceWithHeight(double latitude1, double longitude1, double altitude1, Location park) {

        double altitude = Math.abs(altitude1 - park.getAltitude()) / 1000;
        double distance = getDistance(latitude1, longitude1, park.getLatitude(), park.getLongitude());
        return Math.sqrt(Math.pow(distance, 2) + Math.sqrt(Math.pow(altitude, 2)));

    }

    /**
     * Algorithm that calculates distance between two points
     *
     * @param latitude1 origin latitude
     * @param longitude1 origin longitude
     * @param latitude2 destiny latitude
     * @param longitude2 destiny
     * @return distance between points in km
     */
    public static double getDistance(double latitude1, double longitude1, double latitude2, double longitude2) {

        double dlon = Math.toRadians(longitude2 - longitude1);
        double dlat = Math.toRadians(latitude2 - latitude1);
        double a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2)) * Math.pow((Math.sin(dlon / 2)), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }

    /**
     * Calculates an angle given a distance and two heights.
     *
     * @param distance
     * @param altitude1
     * @param altitude2
     * @return
     */
    private static double calculateAngle(double distance, double altitude1, double altitude2) {
        double height = altitude2 - altitude1;
        double alfa = height / distance;
        return Math.asin(alfa);
    }

    /**
     * Calculates a projection of calories burnt between two parks.
     *
     * @param usr User object
     * @param p1 Origin park object
     * @param p2 Destination park object
     * @param bicWeight Bicycle Weight in Kg
     * @param bicycleWorkRatio Bicycle work ratio between 0 tand 1
     * @param windVel wind velocity in Km/h
     * @param windFactor the wind angle
     * @return amount of calories burnt in cal
     */
    public static double calculateCaloriesProjection(User usr, Location p1, Location p2, double bicWeight,
            double bicycleWorkRatio, double windVel, double windFactor) {

        return calculateEnergyToTravelFromAtoB(usr, p1, p2, bicWeight,
                windVel, windFactor) * bicycleWorkRatio;

    }

    /**
     * Calculates the amount energy necessary to travel between two parks.
     *
     * @param usr User object
     * @param p1 Origin park object
     * @param p2 Destination park object
     * @param bicWeight Bicycle Weight in Kg
     * @param windVel wind velocity in Km/h
     * @param windFactor the wind angle
     * @return amount of energy cal
     */
    private static double calculateEnergyToTravelFromAtoB(User usr, Location p1, Location p2, double bicWeight,
            double windVel, double windFactor) {

        windVel = kilometerByHourToMeterBySecond(windVel);
        double usrWeight = usr.getWeight();
        double distance = getDistanceWithHeight(p1.getLatitude(), p1.getLongitude(), p1.getAltitude(), p2) * 1000;
        double time = distance / usr.getUserAverageSpeed();
        double angle = calculateAngle(distance, p1.getAltitude(), p2.getAltitude());
        double grade = Math.tan(angle);
        //bearingAngle 
        double bearingAngle = bearingAngleCalculation(p1, p2);
        windFactor = degreesToRad(windFactor);
        //contabilização da vento e do deslocamento
        double windVelocity = calculateWindVelocity(usr, windVel, windFactor, bearingAngle);
        double power = G_FORCE * (usrWeight + bicWeight) * usr.getUserAverageSpeed() * (K1 + grade) + K2 * windVelocity * usr.getUserAverageSpeed();
        double result = power * time;
        return Double.compare(result, 0) > 0 ? result : 0;
    }

    public static double calculateEnergyToTravelFromAtoBinKwh(User usr, Location p1, Location p2, double bicWeight,
            double windVel, double windFactor) {
        return calculateEnergyToTravelFromAtoB(usr, p1, p2, bicWeight,
                windVel, windFactor) / CONVERTION_FACTOR;
    }

    /**
     * Method to calculate the resultant wind velocity considering relative wind
     * direction to the cycling motion.
     *
     * @param windVel wind velocity in km/h
     * @param windFactor wind angle
     * @param bearingAngle the angle between trajentory and North
     * @return resultant wind velocity in m/s
     */
    public static double calculateWindVelocity(User usr, double windVel, double windFactor, double bearingAngle) {
        double va = 0;
        if ((Double.compare(windFactor, Math.PI) < 0 && Double.compare(bearingAngle, Math.PI) < 0)
                || (Double.compare(windFactor, Math.PI) > 0 && Double.compare(bearingAngle, Math.PI) > 0)) {
            double factor = Math.abs(bearingAngle - windFactor);
            va = usr.getUserAverageSpeed() - Math.cos(factor) * kilometerByHourToMeterBySecond(windVel);
        }
        if ((Double.compare(windFactor, Math.PI) < 0 && Double.compare(bearingAngle, Math.PI) > 0)
                || (Double.compare(windFactor, Math.PI) > 0 && Double.compare(bearingAngle, Math.PI) < 0)) {
            double factor = Math.abs(2 * Math.PI - (bearingAngle + windFactor));
            va = usr.getUserAverageSpeed() + Math.cos(factor) * kilometerByHourToMeterBySecond(windVel);
        }
        return Double.compare(windVel, 0) == 0 ? Math.pow(usr.getUserAverageSpeed(), 2) : Math.pow(va, 2);
    }

    /**
     * Method that converts velocity measure unit from Km/h to m/s
     *
     * @param vel the velocity in km/h
     * @return velocity in meter / second
     */
    public static double kilometerByHourToMeterBySecond(double vel) {
        double meterByHour = vel * 1000;
        double meterBySec = meterByHour / 3600;
        return meterBySec;
    }

    /**
     * Calculates which potency to use. If there is only one slot, then te max
     * potency to calculate should be the slot's max potency. Else should be the
     * total Park's potency dividing by the number of slots occupied.
     *
     * @param slotsOccupied to convert to Kw/h
     * @return the calculated value
     */
    private static double potencyUsage(int slotsOccupied, double InputCurrent, double InputVoltage) {
        return slotsOccupied > ONE_SLOT ? (InputCurrent * InputVoltage) / CONV_CONSTANT : Configs.PARK_SLOT_POTENCY;
    }

    /**
     * This method converts timeInHours from the calculation to a string that is
     * going to be stored in the report.
     *
     * @param timeInHours - time in hours
     * @return a string with HH:MM:SS
     */
    private static String displayTime(double timeInHours) {
        double totalSecs = timeInHours * HOUR_SEC_CONSTANT;
        int hours = (int) totalSecs / HOUR_SEC_CONSTANT;
        int minutes = (int) (totalSecs % HOUR_SEC_CONSTANT) / SEC_CONSTANT;
        int seconds = (int) totalSecs % SEC_CONSTANT;
        return String.valueOf(hours) + ":" + String.valueOf(minutes) + ":" + String.valueOf(seconds);
    }

    /**
     * Method to calculate the charging time remaining of each bicycle in a
     * given park.
     *
     * @param reportSet - set of available bicycles in a given park.
     * @return HashMap<Bicycle   ,       String> where the string is the time in
     * HH:MM:SS that is the time remaining
     */
    public static Map<Bicycle, String> calculateChargingTime(Set<Bicycle> reportSet, double inputCurrent, double inputVoltage) {
        HashMap<Bicycle, String> finalReport = new HashMap<>();
        int slotsOccupied = reportSet.size();
        double totalPotency = potencyUsage(slotsOccupied, inputCurrent, inputVoltage);
        double potencySlot = totalPotency / slotsOccupied;
        for (Bicycle b : reportSet) {
            double btLevel = b.getBatteryLevel();
            double btCapacity = b.getFullBatteryCapacity();
            double remainingTime = (btCapacity - (btCapacity * (btLevel / PERC_CONSTANT))) / potencySlot;
            String time = displayTime(remainingTime);
            finalReport.put(b, time);
        }
        return finalReport;
    }

    /**
     * ********************************************
     * ******* BEARING ANGLE DETERMINATION ********
     * ********************************************
     */
    /**
     * Method that estimates the bearing angle between two locations, p1 and p2
     *
     * @param p1 origin location
     * @param p2 detination location
     * @return bearing angle
     */
    public static double bearingAngleCalculation(Location p1, Location p2) {
        double p1Latitude = p1.getLatitude();
        double p2Latitude = p2.getLatitude();
        double p1Longitude = p1.getLongitude();
        double p2Longitude = p2.getLongitude();

        if (Double.compare(p1Longitude, p2Longitude) == 0 && Double.compare(p1Latitude, p2Latitude) < 0) {
            return 0f;
        }
        if (Double.compare(p1Latitude, p2Latitude) == 0 && Double.compare(p1Longitude, p2Longitude) < 0) {
            return Math.PI / 2;
        }
        if (Double.compare(p1Longitude, p2Longitude) == 0 && Double.compare(p1Latitude, p2Latitude) > 0) {
            return Math.PI;
        }
        if (Double.compare(p1Latitude, p2Latitude) == 0 && Double.compare(p1Longitude, p2Longitude) > 0) {
            return Math.PI * 3 / 2;
        }
        if (Double.compare(p1Longitude, p2Longitude) < 0 && Double.compare(p1Latitude, p2Latitude) < 0) {
            return bearingAngleScenario1(p1, p2);
        }
        if (Double.compare(p1Longitude, p2Longitude) < 0 && Double.compare(p1Latitude, p2Latitude) > 0) {
            return bearingAngleScenario2(p1, p2);
        }
        if (Double.compare(p1Longitude, p2Longitude) > 0 && Double.compare(p1Latitude, p2Latitude) > 0) {
            return bearingAngleScenario3(p1, p2);
        }
        if (Double.compare(p1Longitude, p2Longitude) > 0 && Double.compare(p1Latitude, p2Latitude) < 0) {
            return bearingAngleScenario4(p1, p2);
        }
        return 0f;
    }

    /**
     * Method that estimates the bearing angle for two locations - p1 and p2. In
     * this scenario location p1 has lower latitude and lower longitude.
     *
     * @param p1
     * @param p2
     * @return
     */
    private static double bearingAngleScenario1(Location p1, Location p2) {
        double adjacentSide = getDistanceWithHeight(p2.getLatitude(), p1.getLongitude(), p1.getAltitude(), p1);
        double hypotenuse = getDistanceWithHeight(p1.getLatitude(), p1.getLongitude(), p1.getAltitude(), p2);
        return Math.acos(adjacentSide / hypotenuse);
        
    }

    /**
     * Method that estimates the bearing angle for two locations - p1 and p2. In
     * this scenario location p1 has higher latitude and lower longitude.
     *
     * @param p1
     * @param p2
     * @return
     */
    private static double bearingAngleScenario2(Location p1, Location p2) {
        double adjacentSide = getDistanceWithHeight(p2.getLatitude(), p1.getLongitude(), p1.getAltitude(), p1);
        double hypotenuse = getDistanceWithHeight(p1.getLatitude(), p1.getLongitude(), p1.getAltitude(), p2);
        return Math.PI - Math.acos(adjacentSide / hypotenuse);

    }

    /**
     * Method that estimates the bearing angle for two locations - p1 and p2. In
     * this scenario location p1 has higher latitude and higher longitude.
     *
     * @param p1
     * @param p2
     * @return
     */
    private static double bearingAngleScenario3(Location p1, Location p2) {
        double adjacentSide = getDistanceWithHeight(p1.getLatitude(), p2.getLongitude(), p1.getAltitude(), p1);
        double hypotenuse = getDistanceWithHeight(p1.getLatitude(), p1.getLongitude(), p1.getAltitude(), p2);
        return Math.PI + Math.acos(adjacentSide / hypotenuse);
        
    }

    /**
     * Method that estimates the bearing angle for two locations - p1 and p2. In
     * this scenario location p1 has lower latitude and higher longitude.
     *
     * @param p1
     * @param p2
     * @return
     */
    private static double bearingAngleScenario4(Location p1, Location p2) {
        double adjacentSide = getDistanceWithHeight(p1.getLatitude(), p2.getLongitude(), p1.getAltitude(), p1);
        double hypotenuse = getDistanceWithHeight(p1.getLatitude(), p1.getLongitude(), p1.getAltitude(), p2);
        return (2 * Math.PI) - Math.acos(adjacentSide / hypotenuse);
        
    }

    /**
     * Method that converts an angle from degrees to radians
     *
     * @param angle in degrees
     * @return angle in rad
     */
    public static double degreesToRad(double angle) {
        return angle * Math.PI / 180;
    }

    /**
     * *********************************************
     ***************** OTHER UTILS *****************
     * *********************************************
     */
    public static void sout(String str) {
        System.out.println(str);
    }

    public static java.sql.Date convertDateToSQL(java.util.Date date) {

        if (date == null) {
            return new java.sql.Date(0);
        }
        return new java.sql.Date(date.getTime());
    }

    /**
     * Method to sort map by value
     *
     * @param map Map
     * @return sorted map
     */
    public static Map<Park, Double> OrderMapByValue(Map<Park, Double> map) {
        Map<Park, Double> newMap = new LinkedHashMap<>();
        List<Map.Entry<Park, Double>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Park, Double>>() {
            @Override
            public int compare(Map.Entry<Park, Double> m1, Map.Entry<Park, Double> m2) {
                return (m1.getValue().compareTo(m2.getValue()));
            }
        });
        for (Map.Entry<Park, Double> m : list) {
            newMap.put(m.getKey(), m.getValue());
        }
        return newMap;
    }

    public static double routeSelection(User user, Location pickUp, Location dropOff, double bikeWeight, double bikeRatio, double windVel, double windFact, int routeChoice) {

        if (routeChoice == 1) {
            return AuxMethods.getDistanceWithHeight(pickUp.getLatitude(), pickUp.getLongitude(), pickUp.getAltitude(), dropOff);
        }

        return AuxMethods.calculateCaloriesProjection(user, pickUp, dropOff, bikeWeight, bikeRatio, windVel, windFact);
    }

    /**
     * Return a Set containing all possible permutations of Locations within an
     * array
     *
     * @param locationArray array containing Locations
     * @return Set of Location []
     */
    private static Set<Location[]> locationsPermutations(Location[] locationArray) {
        Set<Location[]> result = new HashSet<>();
        return locationsPermutations(locationArray, locationArray.length, result);
    }

    private static Set<Location[]> locationsPermutations(Location[] locationArray, int size, Set<Location[]> result) {

        if (Integer.compare(size, 1) == 0) {
            result.add(locationArray.clone());
        }

        for (int i = 0; i < size; i++) {
            locationsPermutations(locationArray, size - 1, result);
            if (size % 2 == 1) {
                Location temp = locationArray[0];
                locationArray[0] = locationArray[size - 1];
                locationArray[size - 1] = temp;
            } else {
                Location temp = locationArray[i];
                locationArray[i] = locationArray[size - 1];
                locationArray[size - 1] = temp;
            }
        }
        return result;
    }

    /**
     * Calculates the effortless route considering all intermediate Locations
     * within a given Array of locations
     *
     * @param graph graph representing the paths network
     * @param locationArray Array containing the intermidiate Locations
     * @param pickUpPark pick up Location (should be a Park)
     * @param dropOffPark drop off Location (should be a Park)
     * @return An array of ordered Locations tha represent the best route
     */
    public static Map<Location[], Double> bestRouteWithIntermediatePOI(Graph<Location, String> graph, Location[] locationArray, Location pickUpPark, Location dropOffPark) {

        Set<Location[]> perm = locationsPermutations(locationArray);
        Map<Location[], Double> permutationsWithWeight = new HashMap<>();

        for (Location[] locArr : perm) {

            Location[] completeArray = completeArray(locArr, pickUpPark, dropOffPark);

            double routeSize = routeSize(graph, completeArray);

            permutationsWithWeight.put(completeArray, routeSize);

        }

        return permutationsWithWeight;

    }

    private static Location[] completeArray(Location[] loc, Location pickUpPark, Location dropOffPark) {

        ArrayList<Location> arr = new ArrayList<>();

        for (Location local : loc) {
            arr.add(local);
        }

        arr.add(dropOffPark);
        arr.add(0, pickUpPark);

        Location[] newArr = arr.toArray(new Location[arr.size()]);

        return newArr;

    }

    private static double routeSize(Graph<Location, String> graph, Location[] completeArray) {

        LinkedList<Location> shortPath = new LinkedList<>();
        double total = 0.0;

        for (int i = 0; i < completeArray.length - 1; i++) {

            total += GraphAlgorithms.shortestPath(graph, completeArray[i], completeArray[i + 1], shortPath);

        }

        return total;

    }

    /**
     * Receives List of Locations and converts to an Array of Locations
     *
     * @param loc
     * @return Array of Location
     */
    public static Location[] listToArray(List<Location> loc) {

        return loc.toArray(new Location[loc.size()]);
    }

    public static Map<Location[], Double> routeSorting(Map<Location[], Double> map, int ascendingDescending, int numberOfAlternativeRoutes) {

        int cont = 0;
        Map<Location[], Double> permutationsWithWeight = new LinkedHashMap<>();
        List<Map.Entry<Location[], Double>> list = new LinkedList<>(map.entrySet());
        Iterator<Map.Entry<Location[], Double>> it = list.iterator();

        if (ascendingDescending == ASCENDING_SORTING) {

            Collections.sort(list, new Comparator<Map.Entry<Location[], Double>>() {
                public int compare(Map.Entry<Location[], Double> m1, Map.Entry<Location[], Double> m2) {
                    return (m1.getValue().compareTo(m2.getValue()));
                }
            });

            for (Map.Entry<Location[], Double> m : list) {

                if (cont < numberOfAlternativeRoutes) {
                    permutationsWithWeight.put(m.getKey(), m.getValue());
                }
                cont++;
            }

        } else if (ascendingDescending == DESCENDING_SORTING) {
            Collections.sort(list, new Comparator<Map.Entry<Location[], Double>>() {
                public int compare(Map.Entry<Location[], Double> m1, Map.Entry<Location[], Double> m2) {
                    return (m2.getValue().compareTo(m1.getValue()));
                }
            });

            for (Map.Entry<Location[], Double> m : list) {
                if (cont < numberOfAlternativeRoutes) {
                    permutationsWithWeight.put(m.getKey(), m.getValue());
                }
                cont++;
            }
        }

        return permutationsWithWeight;
    }

    public static double convertDaysToSeconds(double days) {
        return days * SECONDS_PER_DAY;
    }

    public static double earnedPoints(double altitudDifference) {
        double points = 0;

        if (Double.compare(altitudDifference, 50) > 0) {
            return 15;
        }
        if (Double.compare(altitudDifference, 25) > 0) {
            return 5;
        }
        return points;
    }

//    TESTAR!!! 
//    public static Set<Requisition> insertBicycleAndPakrs(Set<Requisition> reqs, Set<Bicycle> bics, Set<Park> parks) {
//        Iterator<Requisition> it = reqs.iterator();
//
//        while (it.hasNext()) {
//            Requisition r = it.next();
//            Bicycle b = r.getBicycle() ;
//            b = getBicycle(bics , b.getBicycleID());
//            if(b!=null){
//                r.setBicycle(b);
//            }
//            Park pickupPark = r.getPickUpPark();
//            pickupPark = getPark(parks , pickupPark.getParkId());
//            if(pickupPark!=null){
//                r.setPickUpPark(pickupPark);
//            }
//            Park dropPark = r.getDropOffPark();
//            dropPark = getPark(parks , dropPark.getParkId());
//            if(dropPark!=null){
//                r.setPickUpPark(dropPark);
//            }
//        }
//        return reqs;
//
//    }
//
//    private static Bicycle getBicycle(Set<Bicycle> bics, int bicId) {
//        Iterator<Bicycle> it = bics.iterator();
//        while (it.hasNext()) {
//            Bicycle b = it.next();
//            if (b.getBicycleID() == bicId) {
//                return b;
//            }
//        }
//        return null;
//    }
//
//    private static Park getPark(Set<Park> parks, int parkId) {
//        Iterator<Park> it = parks.iterator();
//        while (it.hasNext()) {
//            Park p = it.next();
//            if (p.getParkId() == parkId) {
//                return p;
//            }
//        }
//        return null;
//    }
}
