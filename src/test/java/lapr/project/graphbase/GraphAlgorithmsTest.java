
package lapr.project.graphbase;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 *
 * @author DEI-ISEP
 */
public class GraphAlgorithmsTest {
    
    static Graph<String,String> completeMap = new Graph<>(false);
    
    public GraphAlgorithmsTest() {
    }

    @BeforeAll
    public static void setUp() throws Exception {
        
        completeMap.insertVertex("Porto");
        completeMap.insertVertex("Braga");
        completeMap.insertVertex("Vila Real");
        completeMap.insertVertex("Aveiro");
        completeMap.insertVertex("Coimbra");
        completeMap.insertVertex("Leiria");

        completeMap.insertVertex("Viseu");
        completeMap.insertVertex("Guarda");
        completeMap.insertVertex("Castelo Branco");
        completeMap.insertVertex("Lisboa");
        completeMap.insertVertex("Faro");
                
        completeMap.insertEdge("Porto","Aveiro","A1",75);
        completeMap.insertEdge("Porto","Braga","A3",60);
        completeMap.insertEdge("Porto","Vila Real","A4",100);
        completeMap.insertEdge("Viseu","Guarda","A25",75);
        completeMap.insertEdge("Guarda","Castelo Branco","A23",100);
        completeMap.insertEdge("Aveiro","Coimbra","A1",60);
        completeMap.insertEdge("Coimbra","Lisboa","A1",200);
        completeMap.insertEdge("Coimbra","Leiria","A34",80);
        completeMap.insertEdge("Aveiro","Leiria","A17",120);
        completeMap.insertEdge("Leiria","Lisboa","A8",150);
      
//        incompleteMap = completeMap.clone();
        
//        completeMap.insertEdge("Aveiro","Viseu","A25",85);
//        completeMap.insertEdge("Leiria","Castelo Branco","A23",170);
//        completeMap.insertEdge("Lisboa","Faro","A2",280);
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }


    /**
    * Test of shortestPath method, of class GraphAlgorithms.
    */
    @Test
    public void testShortestPath() {
        System.out.println("Test of shortest path");
		
	LinkedList<String> shortPath = new LinkedList<String>();
	double lenpath=0;
        lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","LX",shortPath);
        assertEquals(0, shortPath.size());
	
        lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","Faro",shortPath);
        assertEquals(0, shortPath.size());
		
        lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","Porto",shortPath);
        assertEquals(1, shortPath.size());
		
	lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","Lisboa",shortPath);
        assertEquals(335, lenpath, 0.5);
		
        Iterator<String> it = shortPath.iterator();

        assertEquals(0, it.next().compareTo("Porto"));
        assertEquals(0, it.next().compareTo("Aveiro"));
        assertEquals(0, it.next().compareTo("Coimbra"));
        assertEquals(0, it.next().compareTo("Lisboa"));

	lenpath=GraphAlgorithms.shortestPath(completeMap,"Braga","Leiria",shortPath);
        assertEquals(255, lenpath, 0.5);
		
        it = shortPath.iterator();

        assertEquals(0, it.next().compareTo("Braga"));
        assertEquals(0, it.next().compareTo("Porto"));
        assertEquals(0, it.next().compareTo("Aveiro"));
        assertEquals(0, it.next().compareTo("Leiria"));
	
        completeMap.insertEdge("Aveiro","Viseu","A25",85);
        
        shortPath.clear();
        lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","Castelo Branco",shortPath);
        assertEquals(335, lenpath, 0.5);
        assertEquals(5, shortPath.size());

        it = shortPath.iterator();

        assertEquals(0, it.next().compareTo("Porto"));
        assertEquals(0, it.next().compareTo("Aveiro"));
        assertEquals(0, it.next().compareTo("Viseu"));
        assertEquals(0, it.next().compareTo("Guarda"));
        assertEquals(0, it.next().compareTo("Castelo Branco"));

        //Changing Edge: Aveiro-Viseu with Edge: Leiria-C.Branco 
        //should change shortest path between Porto and Castelo Branco

        completeMap.removeEdge("Aveiro", "Viseu");
        completeMap.insertEdge("Leiria","Castelo Branco","A23",170);
	shortPath.clear();
        lenpath=GraphAlgorithms.shortestPath(completeMap,"Porto","Castelo Branco",shortPath);
        assertEquals(365, lenpath, 0.5);
        assertEquals(4, shortPath.size());

        it = shortPath.iterator();

        assertEquals(0, it.next().compareTo("Porto"));
        assertEquals(0, it.next().compareTo("Aveiro"));
        assertEquals(0, it.next().compareTo("Leiria"));
        assertEquals(0, it.next().compareTo("Castelo Branco"));
		
    }




}
