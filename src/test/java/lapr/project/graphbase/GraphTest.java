package lapr.project.graphbase;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author DEI-ISEP
 */
public class GraphTest {
    
    Graph<String, String> instance = new Graph<>(true) ;
    
    public GraphTest() {
    }

    @BeforeAll
    public static void setUp() {

    }

    /**
     * Test of numVertices method, of class Graph.
     */
    @Test
    public void testNumVertices() {
        System.out.println("Test numVertices");
                      
        assertEquals(0, (instance.numVertices()));
        
        instance.insertVertex("A");
        assertEquals(1, (instance.numVertices()));
        
        instance.insertVertex("B");
        assertEquals(2, (instance.numVertices()));
        
        instance.removeVertex("A");
        assertEquals(1, (instance.numVertices()));
        
        instance.removeVertex("B");
        assertEquals(0, (instance.numVertices()));
    }
    
    /**
     * Test of vertices method, of class Graph.
     */
    @Test
    public void testVertices() {
        System.out.println("Test vertices");

        Iterator<String> itVerts = instance.vertices().iterator();
        
        assertEquals(false, itVerts.hasNext());
        
        instance.insertVertex("A");
        instance.insertVertex("B");
        	
        itVerts = instance.vertices().iterator();
                
        assertEquals(0, (itVerts.next().compareTo("A")));
        assertEquals(0,(itVerts.next().compareTo("B")));

        instance.removeVertex("A");
		
        itVerts = instance.vertices().iterator();
        assertEquals(0,(itVerts.next().compareTo("B")));

	instance.removeVertex("B");
		
        itVerts = instance.vertices().iterator();
	assertEquals(false,itVerts.hasNext());
    }

    /**
     * Test of numEdges method, of class Graph.
     */
    @Test
    public void testNumEdges() {
        System.out.println("Test numEdges");
        
        assertEquals(0, (instance.numEdges()));

        instance.insertEdge("A","B","Edge1",6);
        assertEquals(1, (instance.numEdges()));
        
        instance.insertEdge("A","C","Edge2",1);
        assertEquals(2, (instance.numEdges()));
        
        instance.removeEdge("A","B");
        assertEquals(1, (instance.numEdges()));

        instance.removeEdge("A","C");
        assertEquals(0, (instance.numEdges()));
    }

    /**
     * Test of edges method, of class Graph.
     */
    @Test
    public void testEdges() {
        System.out.println("Test Edges");

        Iterator<Edge<String,String>> itEdge = instance.edges().iterator();

        assertEquals(false, (itEdge.hasNext()));

        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);

        itEdge = instance.edges().iterator();
        
        itEdge.next(); itEdge.next();
        assertEquals(true, itEdge.next().getElement().equals("Edge3"));
        
        itEdge.next(); itEdge.next();
        assertEquals(true, itEdge.next().getElement().equals("Edge6"));
        
        instance.removeEdge("A","B");

        itEdge = instance.edges().iterator();
        assertEquals(true, itEdge.next().getElement().equals("Edge2"));

        instance.removeEdge("A","C"); instance.removeEdge("B","D");
        instance.removeEdge("C","D"); instance.removeEdge("C","E");
        instance.removeEdge("D","A"); instance.removeEdge("E","D");
        instance.removeEdge("E","E");
        itEdge = instance.edges().iterator();
        assertEquals(false, (itEdge.hasNext()));
    }

    /**
     * Test of getEdge method, of class Graph.
     */
    @Test
    public void testGetEdge() {
        System.out.println("Test getEdge");
		        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
		
        assertEquals(null, instance.getEdge("A","E"));
		
        assertEquals(true, instance.getEdge("B","D").getElement().equals("Edge3"));
        assertEquals(null, instance.getEdge("D","B"));

	instance.removeEdge("D","A");	
        assertEquals(null, instance.getEdge("D","A"));
        
        assertEquals(true, instance.getEdge("E","E").getElement().equals("Edge8"));
    }

    /**
     * Test of endVertices method, of class Graph.
     */
    @Test
    public void testEndVertices() {
        System.out.println("Test endVertices");
        			 
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
        
        Edge<String,String> edge0 = new Edge<>();
        
        String[] vertices = new String[2];
        
        //assertTrue("endVertices should be null", instance.endVertices(edge0)==null);

        Edge<String,String> edge1 = instance.getEdge("A","B");
        //vertices = instance.endVertices(edge1);
        assertTrue(instance.endVertices(edge1)[0].equals("A"));
        assertTrue(instance.endVertices(edge1)[1].equals("B"));
    }

    /**
     * Test of opposite method, of class Graph.
     */
    @Test
    public void testOpposite() {
        System.out.println("Test opposite");
        		
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);

        Edge<String,String> edge1 = instance.getEdge("A","B");
        String vert = instance.opposite("A", edge1);
        assertTrue(vert.equals("B"));
        
        Edge<String,String> edge8 = instance.getEdge("E","E");
        vert = instance.opposite("E", edge8);
        assertTrue(vert.equals("E"));
    }

    /**
     * Test of outDegree method, of class Graph.
     */
    @Test
    public void testOutDegree() {
        System.out.println("Test outDegree");
        		
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
		    
        int outdeg = instance.outDegree("G");    
        assertTrue(outdeg==-1);
        
        outdeg = instance.outDegree("A");
        assertTrue(outdeg==2);
        
        outdeg = instance.outDegree("B");
        assertTrue(outdeg==1);
         
        outdeg = instance.outDegree("E");
        assertTrue(outdeg==2);
    }

    /**
     * Test of inDegree method, of class Graph.
     */
    @Test
    public void testInDegree() {
        System.out.println("Test inDegree");
        
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
		       
        int indeg = instance.inDegree("G");    
        assertTrue(indeg==-1);
        
        indeg = instance.inDegree("A");
        assertTrue(indeg==1);
        
        indeg = instance.inDegree("D");
        assertTrue(indeg==3);
         
        indeg = instance.inDegree("E");
        assertTrue(indeg==2);
    }

    /**
     * Test of outgoingEdges method, of class Graph.
     */
    @Test
    public void testOutgoingEdges() {
        System.out.println(" Test outgoingEdges");
        		
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
		                        
        Iterator<Edge<String,String>> itEdge = instance.outgoingEdges("C").iterator();
        Edge<String,String> first = itEdge.next();
        Edge<String,String> second = itEdge.next();
        assertTrue( (first.getElement().equals("Edge4") && second.getElement().equals("Edge5")) ||
                    (first.getElement().equals("Edge5") && second.getElement().equals("Edge4")) );
        
        instance.removeEdge("E","E");
        
        itEdge = instance.outgoingEdges("E").iterator();
        assertTrue(itEdge.next().getElement().equals("Edge7"));
        
        instance.removeEdge("E","D");

        itEdge = instance.outgoingEdges("E").iterator();
        assertFalse(itEdge.hasNext());
    }

    /**
     * Test of incomingEdges method, of class Graph.
     */
    @Test
    public void testIncomingEdges() {
        		
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
        
        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);
		      
        Iterator<Edge<String,String>> itEdge = instance.incomingEdges("D").iterator();
        
        assertTrue(itEdge.next().getElement().equals("Edge3"));
        assertTrue((itEdge.next().getElement().equals("Edge4")));
        assertTrue((itEdge.next().getElement().equals("Edge7")));
        
        itEdge = instance.incomingEdges("E").iterator();
        
        assertTrue((itEdge.next().getElement().equals("Edge5")));
        assertTrue((itEdge.next().getElement().equals("Edge8")));
        
        instance.removeEdge("E","E");
        
        itEdge = instance.incomingEdges("E").iterator();
        
        assertTrue((itEdge.next().getElement().equals("Edge5")));
        
        instance.removeEdge("C","E");

        itEdge = instance.incomingEdges("E").iterator();
        assertFalse((itEdge.hasNext()));
    }

    /**
     * Test of insertVertex method, of class Graph.
     */
    @Test
    public void testInsertVertex() {
        System.out.println("Test insertVertex");
        
        instance.insertVertex("A");   
        instance.insertVertex("B");    
        instance.insertVertex("C");    
        instance.insertVertex("D");      
        instance.insertVertex("E");
             
        Iterator <String> itVert = instance.vertices().iterator();
		
        assertTrue((itVert.next().equals("A")));
        assertTrue((itVert.next().equals("B")));
        assertTrue((itVert.next().equals("C")));
        assertTrue((itVert.next().equals("D")));
        assertTrue((itVert.next().equals("E")));
    }
    
    /**
     * Test of insertEdge method, of class Graph.
     */
    @Test
    public void testInsertEdge() {
        System.out.println("Test insertEdge");

        assertEquals(0, instance.numEdges());

        instance.insertEdge("A","B","Edge1",6);
        assertEquals(1, instance.numEdges());
        
        instance.insertEdge("A","C","Edge2",1);
        assertEquals(2, instance.numEdges());
        
        instance.insertEdge("B","D","Edge3",3);
        assertEquals(3, instance.numEdges());
        
        instance.insertEdge("C","D","Edge4",4);
        assertEquals(4, instance.numEdges());
        
        instance.insertEdge("C","E","Edge5",1);
        assertEquals(5, instance.numEdges());
        
        instance.insertEdge("D","A","Edge6",2);
        assertEquals(6, instance.numEdges());
        
        instance.insertEdge("E","D","Edge7",1);
        assertEquals(7, instance.numEdges());
        
        instance.insertEdge("E","E","Edge8",1);
        assertEquals(8, instance.numEdges());
        
        Iterator <Edge<String,String>> itEd = instance.edges().iterator();
		
        itEd.next(); itEd.next();
        assertEquals("Edge3", itEd.next().getElement());
        itEd.next(); itEd.next();
        assertEquals("Edge6", itEd.next().getElement());
    }

    /**
     * Test of removeVertex method, of class Graph.
     */
    @Test
    public void testRemoveVertex() {       
        System.out.println("Test removeVertex");
        
        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");
 
        instance.removeVertex("C");
        assertEquals(4, instance.numVertices());
      
        Iterator<String> itVert = instance.vertices().iterator();
        assertEquals("A", itVert.next());
        assertEquals("B", itVert.next());
        assertEquals("D", itVert.next());
        assertEquals("E", itVert.next());
        
        instance.removeVertex("A");
        assertEquals(3, instance.numVertices());
   
        itVert = instance.vertices().iterator();
        assertEquals("B", itVert.next());
        assertEquals("D", itVert.next());
        assertEquals("E", itVert.next());

        instance.removeVertex("E");
        assertEquals(2, instance.numVertices());

        itVert = instance.vertices().iterator();

        assertEquals("B", itVert.next());
        assertEquals("D", itVert.next());
        
        instance.removeVertex("B"); instance.removeVertex("D");
        assertTrue((instance.numVertices()==0));
    }
    
    /**
     * Test of removeEdge method, of class Graph.
     */
    @Test
    public void testRemoveEdge() {     
        System.out.println("Test removeEdge");

        assertEquals(0, instance.numEdges());

        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);

        assertEquals(8, instance.numEdges());
        
        instance.removeEdge("E","E");
        assertEquals(7, instance.numEdges());
        
        Iterator <Edge<String,String>> itEd = instance.edges().iterator();
		
        itEd.next(); itEd.next();
        assertEquals("Edge3", itEd.next().getElement());
        itEd.next(); itEd.next();
        assertEquals("Edge6", itEd.next().getElement());
        
        instance.removeEdge("C","D");
        assertEquals(6, instance.numEdges());
        
        itEd = instance.edges().iterator();	
        itEd.next(); itEd.next();
        assertEquals("Edge3", itEd.next().getElement());
        assertEquals("Edge5", itEd.next().getElement());
        assertEquals("Edge6", itEd.next().getElement());
        assertEquals("Edge7", itEd.next().getElement());
    }

    /**
     * Test of toString method, of class Graph.
     */
    @Test
    public void testClone() {
        System.out.println("Test Clone");

        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);

        Graph<String,String> instClone = instance.clone();

        assertEquals(instance.numVertices(), instClone.numVertices());
        assertEquals(instance.numEdges(), instClone.numEdges());

        //vertices should be equal
        Iterator<String> itvertClone = instClone.vertices().iterator();
        Iterator<String> itvertSource = instance.vertices().iterator();
        while (itvertSource.hasNext())
            assertEquals(itvertSource.next(), itvertClone.next());
    }

    @Test
    public void testEquals() {
        System.out.println("Test Equals");

        instance.insertEdge("A","B","Edge1",6);
        instance.insertEdge("A","C","Edge2",1);
        instance.insertEdge("B","D","Edge3",3);
        instance.insertEdge("C","D","Edge4",4);
        instance.insertEdge("C","E","Edge5",1);
        instance.insertEdge("D","A","Edge6",2);
        instance.insertEdge("E","D","Edge7",1);
        instance.insertEdge("E","E","Edge8",1);

        assertFalse(instance.equals(null));

        assertEquals(instance, instance);

        assertEquals(instance, instance.clone());

        Graph<String,String> other = instance.clone();

        other.removeEdge("E","E");
        assertNotEquals(instance, other);

        other.insertEdge("E","E","Edge8",1);
        assertEquals(instance, other);

        other.removeVertex("D");
        assertNotEquals(instance, other);

    }

    
    
    /**
     * Test of toString method, of class Graph.
     */
    @Test
    public void testToString() {
        
        System.out.println(instance);
    }


}

