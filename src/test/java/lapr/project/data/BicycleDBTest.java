/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data;

import lapr.project.controller.CreateNewBicycleController;
import lapr.project.controller.CreateNewParkController;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;
import lapr.project.utils.Configs;
import lapr.project.utils.ImportFiles;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class BicycleDBTest {
    BicycleDB db = new BicycleDB();
    int parkid=1;

    @Test
    void getBicycleById() {
        if(!Configs.DEBUG_MODE) {
            Bicycle expected = new Bicycle(2, 3, 20, 100, 15);
            expected.setBicycleID(1);
            Bicycle result = db.getBicycleById(1);
            assertEquals(expected, result);
        }
    }

    @Test
    void getAllAvailableBicycles() {
        if(!Configs.DEBUG_MODE) {
            Set<Bicycle> stABikes = db.getAllAvailableBicycles();
            assertEquals(true, stABikes.size() > 0);
        }
    }

    @Test
    void addBicycle() {
        if(!Configs.DEBUG_MODE) {
            BicycleDB bdb = new BicycleDB();
            CreateNewBicycleController cb = new CreateNewBicycleController();
            Bicycle temp = new Bicycle("PT050", 75, 1, 11, 1.2);
            Bicycle bike = null;
            bike = bdb.addBicycle(temp, "eletric", 41.16942, -8.672861);
            assertEquals(true, bike != null);
        }

    }
    @Test
    void addPoi() {
        if(!Configs.DEBUG_MODE) {
            ImportFiles im = new ImportFiles();
            String file = "importFolder/pois_test.csv";
            int pois = im.importPois(file);
            assertEquals(1, pois);
        }
    }

    @Test
    void getAllAvailableBicyclesOrderdByHigherBatteryLevel() {
        if(!Configs.DEBUG_MODE) {
            Set<Bicycle> stABikes = db.getAllAvailableBicyclesOrderdByHigherBatteryLevel(parkid);
            assertEquals(true, stABikes.size() > 0);
        }
    }

    @Test
    void testScript() throws SQLException {
        if(!Configs.DEBUG_MODE) {
            // JDBC driver name and database URL
            final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
            final String DB_URL = "jdbc:oracle:thin:@vsrvbd1.dei.isep.ipp.pt:1521/pdborcl";

            //  Database credentials
            final String USER = "LAPR3_G1";
            final String PASS = "gebos";

            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            ScriptRunner scp = new ScriptRunner(conn, false, false);

            String fileName = "SQLFiles/DB_SCRIPT_LAPR3G001.sql";
            try {
                Reader re = new BufferedReader(new FileReader(fileName));
                scp.runScript(re);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Test
    void testAddUser() {
        if(!Configs.DEBUG_MODE) {

            BicycleDB bdb = new BicycleDB();
            Bicycle bike = null;
            Bicycle temp = new Bicycle("PT050", 75, 1, 11, 1.2);
            bike = bdb.addBicycle(temp, "eletric", 41.16942, -8.672861);

            UserDB userDB = new UserDB();

            User x = userDB.getUser(1);

            ParkDB parkDB = new ParkDB();
            Park one = parkDB.getPark(1);
            Park two = parkDB.getPark(2);

            Date d1 = new Date(10000);
            Date d2 = new Date(20000);

            Requisition a = new Requisition(x.getUserId(), bike, one, two, d1, d2);
            RequisitionDB req = new RequisitionDB();
            req.addRequisition(a);
        }
    }
    
}
