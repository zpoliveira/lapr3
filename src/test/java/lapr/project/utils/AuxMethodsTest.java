package lapr.project.utils;

import lapr.project.controller.DifferentRoutesBetweenTwoParksController;
import lapr.project.graphbase.Graph;
import lapr.project.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class AuxMethodsTest {

    private static Park p1, p2, p3, p4, p5, p6,p7,p8;
    private static Poi poi1, poi2,poi3;
    private static Location[] perm;
    private static Graph<Location, String> graph = new Graph<>(false);
    private static User usr;

    @BeforeAll
    static void setUp() {
        p1 = new Park("Parque", 41.169420, -8.672861, 0);
        p2 = new Park("Boavista", 41.157883, -8.629217, 122);
        p3 = new Park("Boavista", 41.157883, -8.629217, 0);
        p4 = new Park("Praia anemona", 41.169420, -8.689464, 0);
        p5 = new Park("Presas dos Currais", 41.176397, -8.577025, 0);
        p6 = new Park("Passeio Alegre", 41.148224, -8.671316, 0);
        p7 = new Park("Moreira da Maia", 41.339947, -8.672861, 0);
        p8 = new Park();
        p1.setParkId(1);
        p2.setParkId(2);
        usr = new User("userTest1", 123456789, "2000-07-31", 1.78, 75, 4430178955563528L, "mail@mail.com",6.25);
        usr.setState(1);
        usr.setUserId(1);



        /**
         * Setup for bestRouteWithIntermediatePOItest
         */

        poi1 = new Poi("Serralves",41.159580,-8.655704,25);
        poi2 = new Poi("Estadio do Bessa",41.161930,-8.642831,15);
        poi3 = new Poi("Casa da Música",41.158839,-8.630985,5);
        poi1.setPoiId(1);
        poi2.setPoiId(2);
        poi3.setPoiId(3);
        perm= new Location[3];

        perm[0]=poi1;
        perm[1]=poi2;
        perm[2]=poi3;

        graph.insertVertex(p1);
        graph.insertVertex(p2);
        graph.insertVertex(poi1);
        graph.insertVertex(poi2);
        graph.insertVertex(poi3);
        graph.insertEdge(p1,p2,"wheight",10);
        graph.insertEdge(p1,poi1,"wheight",3);
        graph.insertEdge(p1,poi2,"wheight",6);
        graph.insertEdge(p1,poi3,"wheight",9);
        graph.insertEdge(p2,poi1,"wheight",7);
        graph.insertEdge(p2,poi2,"wheight",4);
        graph.insertEdge(p2,poi3,"wheight",1);
        graph.insertEdge(poi1,poi2,"wheight",3);
        graph.insertEdge(poi1,poi3,"wheight",6);
        graph.insertEdge(poi2,poi3,"wheight",3);

    }

    @Test
    void getDistanceWithHeightTest() {
        double latitude1 = 10.0;
        double longitude1 = 20.0;
        double altitude = 0.0;
        Park newPark = new Park("psrk", 20.0, 30.0, 10);
        double expected = 1545;
        double distance = AuxMethods.getDistanceWithHeight(latitude1, longitude1, altitude, newPark);
        assertEquals(expected, distance, 0.5);
    }

    @Test
    void orderMapByValue() {

        Map<Park, Double> map = new LinkedHashMap<>();
        Park p1 = new Park();
        Park p2 = new Park();
        p1.setParkId(1);
        p2.setParkId(2);
        map.put(p1, 10.0);
        map.put(p2, 1.0);
        Map<Park, Double> newMap = new LinkedHashMap<>();
        newMap = AuxMethods.OrderMapByValue(map);
        assertEquals(p2, newMap.entrySet().iterator().next().getKey());

    }

    @Test
    void convertDateToSQL() {

        Date d1 = new Date(1000);
        java.sql.Date d2 = new java.sql.Date(1000);
        java.sql.Date d5 = new java.sql.Date(0);
        Date d3 = AuxMethods.convertDateToSQL(d1);
        Date d4 = null;
        Date d6 = AuxMethods.convertDateToSQL(d4);
        assertEquals(d2, d3);
        assertEquals(d5, d6);
    }

    
    @Test
    void calculateWindVelocityTailWind() {
        double expected = 12.723;
        double result = AuxMethods.calculateWindVelocity(usr, 10d, Math.PI/4, Math.PI/3);
        assertEquals(expected, result, 0.5);
    }
    @Test
    void calculateWindVelocityTailWind2() {
        double expected = 12.723;
        double result = AuxMethods.calculateWindVelocity(usr,10d, Math.PI/4, Math.PI/3);
        assertEquals(expected, result, 0.5);
    }

    @Test
    void calculateWindVelocityHeadWind() {
        double expected = 39.0625;
        double result = AuxMethods.calculateWindVelocity(usr,10d, 5*Math.PI/4,Math.PI/4);
        assertEquals(expected, result, 0.55);
    }
    @Test
    void calculateWindVelocityHeadWind2() {
        double expected = 39.0625;
        double result = AuxMethods.calculateWindVelocity(usr, 10d, Math.PI/4,5*Math.PI/4);
        assertEquals(expected, result, 0.55);
    }

    @Test
    void kilometerByHourToMeterBySecond() {
        double expected = 6.25;
        double result = AuxMethods.kilometerByHourToMeterBySecond(22.5d);
        assertEquals(expected, result);
    }

    @Test
    void calculateChargeReportTest() {
        HashMap<Bicycle, String> expected = new HashMap<>();
        Bicycle b1 = new Bicycle(1, 3, 20, 1, 20);
        Bicycle b2 = new Bicycle(1, 3, 50, 1, 20);
        b1.setBicycleID(140);
        b2.setBicycleID(141);
        b1.setAvaliability(true);
        b2.setAvaliability(true);
        String timeB1 = "0:27:16";
        String timeB2 = "0:17:2";
        expected.put(b1, timeB1);
        expected.put(b2, timeB2);
        Set<Bicycle> setB = new HashSet<>();
        setB.add(b1);
        setB.add(b2);
        Map<Bicycle, String> result = AuxMethods.calculateChargingTime(setB, 16, 220);
        assertEquals(expected, result);
    }

    @Test
    void calculateChargeReportOnlyOneOccupiedTest() {
        HashMap<Bicycle, String> expected = new HashMap<>();
        String timeB1 = "0:16:0";
        Bicycle b1 = new Bicycle(1, 3, 20, 1, 20);
        b1.setBicycleID(140);
        b1.setAvaliability(true);
        expected.put(b1, timeB1);
        Set<Bicycle> setB = new HashSet<>();
        setB.add(b1);
        Map<Bicycle, String> result = AuxMethods.calculateChargingTime(setB, 16, 220);
        assertEquals(expected, result);
    }

    @Test
    void calculateCaloriesProjectionTest_ClimbingUp_NoWind() {
        double expected = 152000;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p2, 15, 0.99, 0, 0);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingDown_NoWind() {
        double expected = 0;
        double result = AuxMethods.calculateCaloriesProjection(usr, p2, p1, 15, 0.99, 0, 0);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingUp_TailWind() {
        //mexi aqui
        double expected = 149499;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p2, 15, 0.99, 10, 45);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingUp_HeadWind() {
        double expected = 155465;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p2, 15, 0.99, 10, 315);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingUp_TailWind_ElectricalBike() {
        double expected = 75504;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p2, 15, 0.50, 10,45);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingUp_HeadWind_ElectricalBike() {
        double expected = 78517;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p2, 15, 0.50, 10, 315);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingDown_TailWind() {
        double expected = 0;
        double result = AuxMethods.calculateCaloriesProjection(usr, p2, p1, 15, 0.99, 10, 45);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_RidingDown_HeadWind() {
        double expected = 0;
        double result = AuxMethods.calculateCaloriesProjection(usr, p2, p1, 15, 0.99, 20, 315);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_Plain_NoWind() {
        double expected = 45500;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p3, 15, 0.99, 0, 0);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_Plain_HeadWind() {
        double expected = 51877;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p3, 15, 0.99, 20, 315);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateCaloriesProjectionTest_Plain_TailWind() {
        double expected = 40029.2;
        double result = AuxMethods.calculateCaloriesProjection(usr, p1, p3, 15, 0.99, 20, 45);
        assertEquals(expected, result, 500);
    }

    @Test
    void calculateEnergyInKwhTest() {
        double expected = 0.05288;
        double result = AuxMethods.calculateEnergyToTravelFromAtoBinKwh(usr, p1, p3, 15, 0, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void bestRouteWithIntermediatePOItest1(){
        Location[] expected = new Location[5];
        expected[0]=p1;
        expected[1]=poi1;
        expected[2]=poi2;
        expected[3]=poi3;
        expected[4]=p2;

        Map<Location[],Double> result = AuxMethods.bestRouteWithIntermediatePOI(graph,perm,p1, p2);

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expected[0],array[0]);
        assertEquals(expected[1],array[1]);
        assertEquals(expected[2],array[2]);
        assertEquals(expected[3],array[3]);
        assertEquals(expected[4],array[4]);
    }

    @Test
    void bestRouteWithIntermediatePOItest2(){
        graph.getEdge(p1,poi1).setWeight(300);

        Location[] expected = new Location[5];
        expected[0]=p1;
        expected[1]=poi2;
        expected[2]=poi1;
        expected[3]=poi3;
        expected[4]=p2;

        Map<Location[],Double> result = AuxMethods.bestRouteWithIntermediatePOI(graph,perm,p1, p2);

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expected[0],array[0]);
        assertEquals(expected[1],array[1]);
        assertEquals(expected[2],array[2]);
        assertEquals(expected[3],array[3]);
        assertEquals(expected[4],array[4]);

    }
    
    @Test
    void bearingAngleCalculationtest_sameLatHigerLong(){
        double expeted =Math.PI/2;
        double result = AuxMethods.bearingAngleCalculation(p4, p1);
        assertEquals(expeted, result, 1);
    }
     @Test
    void bearingAngleCalculationtest_sameLatLowerLong(){
        double expeted = 3*Math.PI/2;
        double result = AuxMethods.bearingAngleCalculation(p1, p4);
        assertEquals(expeted, result, 1);
    }

     @Test
    void bearingAngleCalculationtest_LowerLatLowerLong(){
         //Scenario2
        double expeted = 84.444*Math.PI/180;
        double result = AuxMethods.bearingAngleCalculation(p1, p5);
        assertEquals(expeted, result, 1);
    }
    
      @Test
    void bearingAngleCalculationtest_LowerLatHigerLong(){
        //Scenario2
        double expeted = 109.333*Math.PI/180;
        double result = AuxMethods.bearingAngleCalculation(p1, p2);
        assertEquals(expeted, result, 1);
    }
       @Test
    void bearingAngleCalculationtest_HigerLatHigerLong(){
         //Scenario3
        double expeted = 253.067*Math.PI/180;
        double result = AuxMethods.bearingAngleCalculation(p2, p6);
        assertEquals(expeted, result, 1);
    }
       @Test
    void bearingAngleCalculationtest_HigerLatLowerLong(){
        //Scenario4
        double expeted = 289.362*Math.PI/180;
        double result = AuxMethods.bearingAngleCalculation(p2, p1);
        assertEquals(expeted, result, 1);
    }
    
      @Test
    void bearingAngleCalculationtest_LowerLatSameLong(){
          
        double expeted = 0;
        double result = AuxMethods.bearingAngleCalculation(p1, p7);
        assertEquals(expeted, result, 1);
    }
     @Test
    void bearingAngleCalculationtest_HigerLatSameLong(){
        double expeted = Math.PI;
        double result = AuxMethods.bearingAngleCalculation(p7,p1);
        assertEquals(expeted, result, 1);
    }
     @Test
    void bearingAngleCalculationtest_InvalidAngle(){
        double expeted = 0d;
        double result = AuxMethods.bearingAngleCalculation(p8,p8);
        assertEquals(expeted, result, 1);
    }
    @Test
    void andgleConvertionFromDegreesToRad(){
        double expeted = Math.PI;
        double result = AuxMethods.degreesToRad(180);
        assertEquals(expeted, result, 1);
    }
    
    
    
    @Test
    void routeSortingAsc() {

        setUp();
        Map<Location[],Double> result = AuxMethods.bestRouteWithIntermediatePOI(graph,perm,p1, p2);

        double expected2 = 16.0;

        result = AuxMethods.routeSorting(result,1,6);

        Collection<Double> a = result.values();
        Iterator<Double> it = a.iterator();

        Double a1 = it.next();
        Double a2 = it.next();


        assertEquals(expected2,a2,0.5);

    }

    @Test
    void routeSortingDesc() {

        setUp();
        Map<Location[],Double> result = AuxMethods.bestRouteWithIntermediatePOI(graph,perm,p1, p2);
        double expected1 = 22.0;
        double expected2 = 22.0;

        result = AuxMethods.routeSorting(result,2,6);

        Collection<Double> a = result.values();
        Iterator<Double> it = a.iterator();

        Double a1 = it.next();
        Double a2 = it.next();

        assertEquals(expected1,a1,0.5);
        assertEquals(expected2,a2,0.5);

    }

    @Test
    void routeSortingDescRouteNumber() {

        setUp();
        Map<Location[],Double> result = AuxMethods.bestRouteWithIntermediatePOI(graph,perm,p1, p2);

        double expected1 = 22.0;

        result = AuxMethods.routeSorting(result,2,1);

        Collection<Double> a = result.values();
        Iterator<Double> it = a.iterator();

        Double a1 = it.next();


        assertEquals(expected1,a1,0.5);

    }

    @Test
    void getDistance1() {

        double latitude1 = 10.0;
        double longitude1 = 20.0;
        double latitude2 = 20.0;
        double longitude2 = 30.0;

        double expected = 1545.24;
        double instance = AuxMethods.getDistance(latitude1,longitude1,latitude2,longitude2);

        assertEquals(expected,instance,0.5);

    }
    @Test
    void convertDaysToSecondsTest(){
        double expected = 86400d;
        double result = AuxMethods.convertDaysToSeconds(1);
        assertEquals(expected, result);
    }

    @Test
    void earnedPointTest50(){
        double expected = 15;
        double result = AuxMethods.earnedPoints(52);
        assertEquals(expected, result);
    }

    @Test
    void earnedPointTest25(){
        double expected = 5;
        double result = AuxMethods.earnedPoints(32);
        assertEquals(expected, result);
    }
    
    @Test
    void earnedPointTest0(){
        double expected = 0;
        double result = AuxMethods.earnedPoints(21);
        assertEquals(expected, result);
    }
}