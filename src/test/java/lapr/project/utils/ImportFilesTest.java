package lapr.project.utils;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.PoiDB;
import lapr.project.data.UserDB;
import lapr.project.data.WindDB;
import lapr.project.model.*;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Poi;
import lapr.project.model.User;
import lapr.project.model.Wind;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ImportFilesTest {

    @Mock
    private BicycleDB bicycleDB;

    @Mock
    private ParkDB parkDB;

    @Mock
    private PoiDB poiDB;

    @Mock
    private WindDB windDB;

    @Mock
    private UserDB userDB;

    @InjectMocks
    private ImportFiles im;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);

        User newUser = new User();
        newUser.setUserId(1);

        Bicycle temp = new Bicycle(1, 1, 75, 1, 20);
        when(bicycleDB.addBicycle(temp, "other", 41.169420, -8.672861)).thenReturn(temp);
        when(parkDB.addPark(any(Park.class))).thenReturn(new Park());
        when(poiDB.addPoi(any(Poi.class))).thenReturn(new Poi());
        when(windDB.addWind(any(Wind.class))).thenReturn(new Wind());
        when(poiDB.addPoi(any(Poi.class))).thenReturn(new Poi());
        when(userDB.addUser(any(User.class))).thenReturn(new User());
        when(userDB.addUser(anyString(), anyInt(), anyString(), anyDouble(), anyDouble(), anyLong(), anyString(), anyDouble())).thenReturn(newUser);
        Poi poi = new Poi("Serralves", 41.159678,-8.659598,10);
        Set<Poi> set= new HashSet<>();
        set.add(poi);
        when(poiDB.getAllPoi()).thenReturn(set);
    }

    @Test
    void importParkTest() {
        String file = "importFolder/parks_test.csv";
        int parks = im.importParks(file);
        assertEquals(3, parks);
    }

    @Test
    void importParkUnsuccessTest() {
        String file = "not";
        int parks = im.importParks(file);
        assertEquals(0, parks);
    }

    @Test
    void importBicycleTest() {
        String file = "importFolder/bicycles_test.csv";
        int bicycle = im.importBicycles(file);
        assertEquals(1, bicycle);
    }

    @Test
    void importBicycleUnsuccessTest() {
        String file = "not";
        int bicycle = im.importParks(file);
        assertEquals(0, bicycle);
    }

    @Test
    void importPoiTest() {
        String file = "importFolder/pois_test.csv";
        int pois = im.importPois(file);
        assertEquals(1, pois);
    }

    @Test
    void importPoiUnsuccessTest() {
        String file = "not";
        int pois = im.importPois(file);
        assertEquals(0, pois);
    }

    @Test
    void importPathTest() {
        String file = "importFolder/path_test.csv";
        int paths = im.importPaths(file);
        assertEquals(1, paths);
    }

    @Test
    void importUserTestSuccess() {
        String file = "importFolder/users_test.csv";
        int user = im.importUsers(file);
        assertEquals(1, user);
    }
    @Test
    void importUserTestUnsuccessfull() {
        String file = "not";
        int user = im.importUsers(file);
        assertEquals(0, user);
    }

    @Test
    void importPathUnsuccessTest() {
        String file = "not";
        int paths = im.importPaths(file);
        assertEquals(0, paths);
    }

    @Test
    void importIntermediatePoisSuccess() {
        String file = "importFolder/pois_test.csv";
        List<Location> intermediatePois = im.importIntermediatePois(file);
        assertEquals(1, intermediatePois.size());
    }

    @Test
    void importIntermediatePoisUnsuccessfull() {
        String file = "not";
        List<Location> intermediatePois = im.importIntermediatePois(file);
        assertEquals(0, intermediatePois.size());
    }

}
