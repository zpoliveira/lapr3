package lapr.project.utils;

import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmailTest {

    private static Email email;
    private static User validEmailUser;
    private static User invalidEmailUser;
    private static boolean expectedValid = true;
    private static boolean expectedInvalid = false;

    @BeforeAll
    private static void setUp(){
        validEmailUser = new User("Carlos",123456789, "1988-10-12",1.70, 69, 22334455, "grupo1lapr3@gmail.com");
        invalidEmailUser = new User("Carlos",123456789, "1988-10-12",1.70, 69, 22334455, "");
        expectedValid = true;
        expectedInvalid = false;
        email = new Email();
    }

    @Test
    void sendEmail() {
            boolean resultValid = email.sendEmail(validEmailUser);
            boolean resultInvalid = email.sendEmail(invalidEmailUser);
            assertEquals(expectedValid,resultValid);
            assertEquals(expectedInvalid,resultInvalid);
    }
}