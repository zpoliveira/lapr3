package lapr.project.utils;

import lapr.project.model.Bicycle;
import lapr.project.model.Location;
import lapr.project.model.Park;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lapr.project.model.Invoice;
import lapr.project.model.Requisition;
import lapr.project.model.User;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ExportFilesTest {

    private static final String TEST_FILE = "test.csv";
    private static Set<Bicycle> setBikes;
    Map<Park, Double> map;
    Map<Location[], Double> map2;
    Set<Requisition> requisitions;

    private File file;

    private ExportFiles exportFiles;
    Invoice invoice = new Invoice(1, 1, 1);

    @BeforeEach
    void initialize() {
        exportFiles = new ExportFiles();
        Bicycle b1 = new Bicycle(1, 1, 20, 100, 15);
        b1.setTypeDescription("electric");
        b1.setBicycleID(1);
        b1.setBicycleDescription("PT001");
        Bicycle b2 = new Bicycle(1, 1, 20, 100, 15);
        b2.setBicycleID(2);
        b2.setTypeDescription("electric");
        Bicycle b3 = new Bicycle(1, 1, 20, 100, 15);
        b3.setBicycleID(3);
        b3.setTypeDescription("electric");
        

        setBikes = new HashSet<>();
        setBikes.add(b1);
        setBikes.add(b2);
        setBikes.add(b3);

        map = new HashMap<>();
        Park p1 = new Park();
        Park p2 = new Park();
        Park p3 = new Park();
        p1.setParkId(1);
        p2.setParkId(2);
        p3.setParkId(3);
        p1.setLatitude(41.26);
        p2.setLatitude(42.58);
        p3.setLatitude(43.34);
        p1.setLongitude(-8.13);
        p2.setLongitude(-9.26);
        p3.setLongitude(-10.57);
        p1.setAltitude(0d);
        p2.setAltitude(5d);
        p3.setAltitude(10d);
        map.put(p1, 20.0);
        map.put(p2, 15.0);
        map.put(p3, 17.0);

        Location[] locArray = {p1, p2, p3};
        Location[] locArray2 = {p3, p2, p1};

        map2 = new HashMap<>();
        map2.put(locArray, 10d);
        map2.put(locArray2, 20d);

        User user = new User("carlitos", 0, "date", 0, 0, 0, "mail");
        user.setUserId(1);
        
        Date initialDate1 = new Date(1000);
        Date finalDate1 = new Date(3000);
        Date initialDate2 = new Date(2000);
        Date finalDate2 = new Date(4000);
        
        requisitions = new HashSet<>();
        Requisition r1 = new Requisition(1, b1, p1, p2, initialDate1, finalDate1);
        r1.setRequisitionID(1);
        Requisition r2 = new Requisition(1, b2, p3, p2, initialDate2, finalDate2);
        r2.setRequisitionID(2);
        requisitions.add(r1);
        requisitions.add(r2);
    }

    @Test
    void bicyclesTest1() {

        int expected = 3;
        int result = exportFiles.bicycles(TEST_FILE, setBikes);
        assertEquals(expected, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());

    }

    @Test
    void mapParkDouble() {

        int expected = 3;
        int result = exportFiles.mapParkDouble(TEST_FILE, map);
        assertEquals(expected, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());

    }

    @Test
    void exportPathDistance() {

        int expected = 2;
        int result = exportFiles.exportPath(TEST_FILE, map2, 1);
        assertEquals(expected, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());

    }

    @Test
    void exportPathEnergy() {

        int expected = 2;
        int result = exportFiles.exportPath(TEST_FILE, map2, 2);
        assertEquals(expected, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());

    }
    
     @Test
    void getUserCurrentPointsTest() {
        double exp = 2;
        double result = exportFiles.points(TEST_FILE, requisitions);
        assertEquals(exp, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());
    }
        @Test
    void getInvoicesTest() {
        double exp = 2;
        double result = exportFiles.exportInvoice(TEST_FILE, requisitions, "Carlitos", 0, invoice);
        assertEquals(exp, result);
        assertTrue(new File("exportFolder/" + TEST_FILE).exists());
    }
    
}
