/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.sql.Date;
import lapr.project.data.PaymentDB;
import lapr.project.model.Payment;
import lapr.project.utils.AuxMethods;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MakePaymentControllerTest {

    private Payment expected ;
    private Payment p1 ;

    @Mock
    private PaymentDB paymentDB;

    @InjectMocks
    private MakePaymentController makePaymentController;
    
    @BeforeAll
    public void setUpClass() {
        MockitoAnnotations.initMocks(this);

         expected = new Payment (1,2, 1234, AuxMethods.convertDateToSQL(new Date(1000)));
         p1 = new Payment (1,2, 1234, AuxMethods.convertDateToSQL(new Date(1000)));
         
         when(paymentDB.addPayment(0, p1)).thenReturn(p1);
         
    }
    /**
     * Test of makePayment method, of class MakePaymentController.
     */
    @Test
    public void testMakePayment() {
       Payment result = makePaymentController.makePayment(0,1,2, 1234, AuxMethods.convertDateToSQL(new Date(1000)) );
       assertEquals(expected, result);
       
    }
    
}
