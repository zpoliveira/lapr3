package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.ImportFiles;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateNewParkControllerTest {

    private boolean expected;
    private boolean result;
    private Park park;
    private final static String FILETEST="filetest";

    @Mock
    private ParkDB parkDB;

    @Mock
    private ImportFiles importFiles;

    @InjectMocks
    private CreateNewParkController cnpController;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        result = false;
        expected = true;
        Park exp = new Park("nome2",40.0,8.0,25);
        exp.setParkId(1);
        when(parkDB.addPark(new Park("nome2",40.0,8.0,25))).thenReturn(exp);
        when(importFiles.importParks(anyString())).thenReturn(10);
    }

    @Test
    void setData() {
        park = cnpController.setData("nome2",40.0,8.0,25, 100, 20, 220, 16);
        result = park != null;
        assertEquals(expected, result);
    }

    @Test
    void importParksTest(){
        assertEquals(cnpController.importParks(FILETEST),10);
    }
}