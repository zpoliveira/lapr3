package lapr.project.controller;


import javafx.util.Pair;
import lapr.project.data.*;
import lapr.project.model.BicycleType;
import lapr.project.model.Park;
import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CheckCaloriesProjectionControllerTest {

    @Mock
    private ParkDB parkDB;
    @Mock
    private UserDB userDB;
    @Mock
    private BicycleTypeDB bicycleTypeDB;
    @Mock
    private WindDB windDB;

    @InjectMocks
    private CheckCaloriesProjectionController ccpController;

    private Park park1, park2, park3, park4;
    private User user, user1;
    private BicycleType bType;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        user = new User("name1", 123456789, "2000-10-10",1.55d, 80d,4444123443215768l, "mail@mail.com");
        user.setUserId(10);
        user.setState(1);
        user1 = new User("name2", 123456789, "2000-10-10",1.55d, 80d,4444123443215768l, "mail@mail.com");
        user1.setUserId(11);
        user1.setState(0);
        park1 = new Park("Parque",41.169420, -8.672861, 0);
        park2 = new Park("Boavista",41.157883, -8.629217, 122);
        park3 = new Park("Boavista",41.157883, -200, 122);
        park4 = new Park("Boavista",200, -8.629217, 122);
        park1.setParkId(1);
        park2.setParkId(2);
        park3.setParkId(3);
        park4.setParkId(4);
        bType= new BicycleType(1,"road",0.9);

        when(parkDB.getPark(1)).thenReturn(park1);
        when(parkDB.getPark(2)).thenReturn(park2);
        when(parkDB.getPark(3)).thenReturn(park3);
        when(parkDB.getPark(4)).thenReturn(park4);

        when(userDB.getUser(10)).thenReturn(user);
        when(userDB.getUser(11)).thenReturn(user1);

        when(bicycleTypeDB.getBicycleType("road")).thenReturn(bType);

        when(windDB.getWindInfo(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Pair<>(0d,0d));

    }

    @Test
    void calculateCaloriesProjection() throws Exception {

        double expected=Double.MIN_VALUE;
        double result=ccpController.calculateCaloriesProjection(user.getUserId(), park1.getParkId(), park2.getParkId(),15, "road");
        assertTrue(expected!=result);
    }

    @Test
    void calculateCaloriesProjectionInvalidUser() {
        String expected="Invalid user!";
        try {
            ccpController.calculateCaloriesProjection(user1.getUserId(), park1.getParkId(), park2.getParkId(),15, "road");
        } catch (Exception e) {
            assertEquals(expected, e.getMessage());
        }

    }

    @Test
    void calculateCaloriesProjectionInvalidPark1() {
        String expected="Invalid park!";
        try {
            ccpController.calculateCaloriesProjection(user.getUserId(), park3.getParkId(), park2.getParkId(),15, "road");
        } catch (Exception e) {
            assertEquals(expected, e.getMessage());
        }

    }

    @Test
    void calculateCaloriesProjectionInvalidPark2() {
        String expected="Invalid park!";
        try {
            ccpController.calculateCaloriesProjection(user.getUserId(), park4.getParkId(), park2.getParkId(),15, "road");
        } catch (Exception e) {
            assertEquals(expected, e.getMessage());
        }

    }

    @Test
    void calculateCaloriesProjectionInvalidPark3() {
        String expected="Invalid bicycle weight!";
        try {
            ccpController.calculateCaloriesProjection(user.getUserId(), park1.getParkId(), park2.getParkId(),0, "road");
        } catch (Exception e) {
            assertEquals(expected, e.getMessage());
        }

    }

    @Test
    void calculateCaloriesProjectionInvalidBicycleType() {
        String expected="Invalid bicycle type!";
        try {
            ccpController.calculateCaloriesProjection(user.getUserId(), park1.getParkId(), park2.getParkId(),22, " ");
        } catch (Exception e) {
            assertEquals(expected, e.getMessage());
        }

    }


}