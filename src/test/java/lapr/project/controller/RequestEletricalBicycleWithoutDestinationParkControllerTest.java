/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lapr.project.data.BicycleDB;
import lapr.project.data.RequisitionDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RequestEletricalBicycleWithoutDestinationParkControllerTest {

    private boolean expected;
    private boolean result;
    private Park p1;
    private Date d1;

    @Mock
    private RequisitionDB requisitionDB;

    @Mock
    private BicycleDB bicycleDB;


    @InjectMocks
    private RequestEletricalBicycleWithoutDestinationParkController rebwdpController;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);

        Bicycle b1 = new Bicycle(1, 1, 10, 100, 15);
        b1.setBicycleID(1);
        b1.setBicycleDescription("PT001");
        Bicycle b2 = new Bicycle(1, 1, 20, 100, 15);
        b2.setBicycleID(2);
        Bicycle b3 = new Bicycle(1, 1, 30, 100, 15);
        b3.setBicycleID(3);


        Set<Bicycle> setBikes = new HashSet<>();
        setBikes.add(b3);
        setBikes.add(b2);
        setBikes.add(b1);

        p1 = new Park();
        d1 = new Date();

        result = false;
        expected = true;

        p1.setParkId(147);
        Requisition temp = new Requisition(1, b3, p1, d1);
        when(requisitionDB.addRequisition(temp)).thenReturn(temp);
        when(bicycleDB.getAllAvailableBicyclesOrderdByHigherBatteryLevel(anyInt())).thenReturn(setBikes);
    }

    @Test
    void addRequisitionTest() {
        Bicycle r = rebwdpController.setData(1, p1);
        result = r != null;
        assertEquals(expected, result);
    }

}
