package lapr.project.controller;

import lapr.project.data.PoiDB;
import lapr.project.model.Poi;
import lapr.project.utils.ImportFiles;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateNewPoiControllerTest {

    private boolean expected;
    private boolean result;

    @Mock
    private PoiDB poiDB;

    @Mock
    private ImportFiles importFiles;

    @InjectMocks
    private CreateNewPoiController cnpController;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        result = false;
        expected = true;
        Poi poi = new Poi("nome1",41.6,8.1,26);
        when(poiDB.addPoi(poi)).thenReturn(poi);
        when(importFiles.importPois(anyString())).thenReturn(5);
    }

    @Test
    void setData() {
        Poi instance = cnpController.setData("nome1",41.6,8.1,26);
        result = instance != null;
        assertEquals(expected, result);
    }

    @Test
    void addImportPoiTest() {
        int result = cnpController.importPois("importFolder/pois_test.csv");
        assertEquals(expected, result > 0);
    }
    
}
