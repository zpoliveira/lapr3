package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UpdateParkControllerTest {

    private boolean expected, result;

    private Park park;

    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private UpdateParkController updateParkCtrl;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        expected = true;
        park = new Park("Bilhar Mini",40.0,25.0,3);
        park.setParkId(104);
        Park park2 = new Park("Bilhar Super",40.0,25.0,3);
        park2.setParkId(105);
        when(parkDB.updatePark(park)).thenReturn(true);
        when(parkDB.updatePark(park2)).thenReturn(false);
    }

    @Test
    void updateParkSuccess() {
        boolean result = updateParkCtrl.updatePark(105,"Bilhar Super",40.0,25.0,3,10,20,50,70,220,16);
        verify(parkDB, times(1)).updatePark(park);
    }

    @Test
    void updateParkUnsuccess() {
        expected = false;
        result = updateParkCtrl.updatePark(105,"",40.0,25.0,3,10,20,50,70,220,16);
        assertEquals(expected,result);
    }

}