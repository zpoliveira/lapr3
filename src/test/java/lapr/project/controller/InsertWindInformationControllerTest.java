
package lapr.project.controller;
import lapr.project.data.WindDB;
import lapr.project.model.Wind;
import lapr.project.utils.ImportFiles;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InsertWindInformationControllerTest {

    private boolean expected;
    private boolean result;

    @Mock
    private WindDB windDB;

    @Mock
    private ImportFiles im;

    @InjectMocks
    private InsertWindInformationController iwiController;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        result = false;
        expected = true;
        Wind wind = new Wind(1,2, 1.0, 1.5,0.1,"uni");
        when(windDB.addWind(wind)).thenReturn(wind);
        when(iwiController.importPaths("path_test.csv")).thenReturn(3);
    }

    @Test
    void setData() {
        Wind instance = iwiController.setData(1,2, 1.0, 1.5, 0.1,"uni");
        result = instance != null;
        assertEquals(expected, result);
    }

    @Test
    void importPaths() {

        String file = "path_test.csv";
        int expected = 3;

        int result = iwiController.importPaths(file);

        assertEquals(expected,result);
    }
}
