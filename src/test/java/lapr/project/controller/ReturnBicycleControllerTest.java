package lapr.project.controller;

import javafx.util.Pair;
import lapr.project.data.*;
import lapr.project.model.Bicycle;
import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Any;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.booleanThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReturnBicycleControllerTest {

    private int expectedUserId, expectedUserPoints, expectedBicycleParkId;
    private boolean expectedAvailable;

    @Mock
    private BicycleDB bicycleDB;
    @Mock
    private UserDB userDB;
    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private ReturnBicycleController rtrnBicycle;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        User temp = new User ("Carlitos", 13579, "1988-01-01", 1.70, 69, 12344312, "uasdasdasd@gmail.com");
        temp.setUserPoints(15);
        when(userDB.getUser(1)).thenReturn(temp);

        Bicycle bTemp = new Bicycle(1, 2, 55, 1, 20);
        bTemp.setBicycleID(101);
        bTemp.setAvaliability(true);
        bTemp.setParkID(1303);

        Bicycle bTemp2 = new Bicycle("PT050", 75, 1, 12, 1.1);
        bTemp2.setBicycleID(1);
        bTemp2.setAvaliability(true);
        bTemp2.setParkID(1);
        Bicycle bTemp3 = new Bicycle("PT051", 65, 1, 12, 1.1);
        bTemp3.setBicycleID(1);
        bTemp3.setAvaliability(true);
        bTemp3.setParkID(1);

        Set<Bicycle> set = new HashSet<Bicycle>();
        set.add(bTemp2);

        when(bicycleDB.getBicycleById(101)).thenReturn(bTemp);
        when(bicycleDB.returnBicycle(101, 1303)).thenReturn(1);

        when(bicycleDB.getAllBicycles()).thenReturn(set);
        when(parkDB.getParkByCoordinates(anyInt(), anyInt())).thenReturn(1);
        when(userDB.getUser(anyInt())).thenReturn(temp);

        expectedUserId = 1;
        expectedUserPoints = 15;
        expectedAvailable = true;
        expectedBicycleParkId = 1303;
    }

    @Test
    void returnBicycleTest() {
        Pair<Integer, Boolean> resultPair = rtrnBicycle.returnBicycle(101, 1303);
        int resultUserId = resultPair.getKey();
        int resultUserPoints = userDB.getUser(resultUserId).getUserPoints();

        Bicycle b = bicycleDB.getBicycleById(101);
        boolean resultAvailable = b.isAvaliability();
        int resultBicycleParkId = b.getParkID();

        assertEquals(expectedUserId,resultUserId);
        assertEquals(expectedAvailable, resultAvailable);
        assertEquals(expectedUserPoints, resultUserPoints);
    }

    @Test
    void returnBicycleOtherTest() {
        double latitude = -40.2412;
        double longitude = -8.3451;
        String bicDesc = "PT050";

        boolean expected = true;
        long result = -1;
        result = rtrnBicycle.returnBicycle(bicDesc, latitude, longitude);
        assertEquals(expected, result != -1);
    }

}