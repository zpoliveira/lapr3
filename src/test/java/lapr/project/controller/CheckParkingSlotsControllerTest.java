/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.data.ParkDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CheckParkingSlotsControllerTest {

    int bType1, bType2;

    private Park p1;

    @Mock
    private ParkDB pDB;

    @InjectMocks
    private CheckParkingSlotsController cpsController;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);

        p1 = new Park();
        p1.setName("testName");
        p1.setParkId(1);
        p1.setElectricalLotation(33);
        p1.setMaxElectricalLotation(50);
        p1.setNonElectricalLotation(2);
        p1.setMaxNonElectricalLotation(60);

        bType1 = 1;
        bType2 = 2;

        when(pDB.avaliableSlotsInPark("username1")).thenReturn(bType1);
        when(pDB.avaliableSlotsInPark("username2")).thenReturn(bType2);
        when(pDB.getParkByCoordinates(1,2)).thenReturn(1);
        when(pDB.getPark(1)).thenReturn(p1);
    }

    @Test
    void checkFreeSlotsAtParkTest() {

        int result = cpsController.checkFreeSlotsAtPark(1,2,"username1");
        assertEquals(17,result);
    }

    @Test
    void checkFreeSlotsAtParkTest2() {

        int result = cpsController.checkFreeSlotsAtPark(1,2,"username2");
        assertEquals(58,result);
    }

}
