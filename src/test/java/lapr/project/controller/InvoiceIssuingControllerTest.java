/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.Date;
import lapr.project.data.InvoiceDB;
import lapr.project.data.UserDB;
import lapr.project.model.Invoice;
import lapr.project.model.User;
import lapr.project.utils.AuxMethods;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class InvoiceIssuingControllerTest {

    private boolean expected;
    private boolean result;
    private User user;

    @Mock
    private UserDB userDB;

    @Mock
    private InvoiceDB invoiceDB;

    @InjectMocks
    private static InvoiceIssuingController iiController;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        result = false;
        expected = true;
        Invoice temp = new Invoice(5, 1, 1);
        int month = 1;
        user = new User("Carlitos", 13579, "1988-01-01", 1.70, 69, 12344312, "uasdasdasd@gmail.com");
        user.setUserPoints(15);
        user.setUserId(1);
        user.setState(1);
        when(invoiceDB.addInvoice(5, 1, 1)).thenReturn(temp);
        when(invoiceDB.getMonthlyPayments(month, user.getUserId())).thenReturn(Double.parseDouble("50"), 1.0);
        when(userDB.getUser(1)).thenReturn(user);
        when(userDB.getUserPoints(1)).thenReturn(user.getUserPoints());
    }

    /**
     * Test of getSumMonthlyPayments method, of class InvoiceIssuingController.
     */
    @Test
    public void testGetSumMonthlyPayments() {
        int month = 1;
        int userId = 1;
        double result = iiController.getSumMonthlyPayments(month, userId);
        assertEquals(50, result);
    }

    /**
     * Test of addInvoice method, of class InvoiceIssuingController.
     */
    @Test
    public void testAddInvoice() {
        Invoice invoice = iiController.addInvoice(5, 1);
        result = invoice != null;
        assertEquals(expected, result);
    }

    /**
     * Test of getDiscountAvaliable method, of class ExchangePointsController.
     */
    @Test
    public void testGetDiscountAvaliable() {
        System.out.println("getDiscountAvaliable");
        User u = userDB.getUser(1);
        double expResult = 1.0;
        double result = iiController.getDiscountAvaliable((userDB.getUser(1)).getUserId());
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserPoints method, of class ExchangePointsController.
     */
    @Test
    public void testGetUserPoints() {
        System.out.println("getUserPoints");
        User u = userDB.getUser(1);
        int expResult = 15;
        int result = iiController.getUserPoints((userDB.getUser(1)).getUserId());
        assertEquals(expResult, result);
    }

    /**
     * Test of updateUserPoints method, of class ExchangePointsController.
     */
    @Test
    public void testUpdateUserPoints() {
        System.out.println("updateUserPoints");
        User u = userDB.getUser(1);
        int userPoints = 2;
        boolean expResult = false;
        boolean result = iiController.updateUserPoints((userDB.getUser(1)).getUserId(), userPoints);
        assertEquals(expResult, result);
    }
}
