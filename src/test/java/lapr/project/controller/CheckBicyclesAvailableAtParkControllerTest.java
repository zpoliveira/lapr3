/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.HashSet;
import java.util.Set;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CheckBicyclesAvailableAtParkControllerTest {


    private boolean expected;
    private boolean resultAllParks;
    private boolean resultAllAvailableBicycles;

    @Mock
    private ParkDB parkDB;

    @Mock
    private BicycleDB bicycleDB;

    @Mock
    private UserDB userDB;

    @InjectMocks
    private CheckBicyclesAvailableAtParkController checkBicycleAvAtParkCtrl;


    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        Park p1 = new Park("teste1", 12.123, -4.123, 120);
        p1.setParkId(1);
        Park p2 = new Park("teste2", 11.123, -3.123, 120);
        p2.setParkId(2);
        Park p3 = new Park("teste3", 10.123, -2.123, 120);
        p3.setParkId(3);

        Set<Park> setParks = new HashSet<>();
        setParks.add(p1);
        setParks.add(p2);
        setParks.add(p3);
        when(parkDB.getAllParks()).thenReturn(setParks);

        Bicycle b1 = new Bicycle(1, 1, 20, 100, 15);
        b1.setBicycleID(1);
        b1.setBicycleDescription("PT001");
        Bicycle b2 = new Bicycle(1, 1, 20, 100, 15);
        b2.setBicycleID(2);
        Bicycle b3 = new Bicycle(1, 1, 20, 100, 15);
        b3.setBicycleID(3);

        Set<Bicycle> setBikes = new HashSet<>();
        setBikes.add(b1);
        setBikes.add(b2);
        setBikes.add(b3);
        when(bicycleDB.getAllAvailableBicycles()).thenReturn(setBikes);
        when(bicycleDB.getAllBicycles()).thenReturn(setBikes);
        when(bicycleDB.getBicycleTimeUnlocked(1)).thenReturn(1000d);


        double latitude = 12.123;
        double latitude2 = 15.0;
        double longitude = -4.123;
        double longitude2 = -5.0;

        when(parkDB.getParkByCoordinates(latitude,longitude)).thenReturn(1);
        when(parkDB.getParkByCoordinates(latitude2,longitude2)).thenReturn(2);
        when(parkDB.getPark(1)).thenReturn(p1);
        when(parkDB.getPark(2)).thenReturn(p2);

        expected = true;

        String username = "JPO";
        User user = new User();
        user.setUserId(1);
        when(userDB.getUserByUsername(username)).thenReturn(user);
        when(bicycleDB.getAllAvailableBicyclesWithGivenPower(user,p1,p1,0d,0d)).thenReturn(setBikes);
    }


    /**
     * Test of getAllParks method, of class CheckBicyclesAvailableAtParkController.
     */
    @Test
    public void testGetAllParks() {
        Set<Park> parks = checkBicycleAvAtParkCtrl.getAllParks();
        resultAllParks = parks.size() > 0;
        assertEquals(expected, resultAllParks);
    }

    /**
     * Test of getAllAvailableBicyclesAtPark method, of class CheckBicyclesAvailableAtParkController.
     */
    @Test
    public void testGetAllAvailableBicyclesAtPark() {
        Set<Bicycle> setBikes = checkBicycleAvAtParkCtrl.getAllAvailableBicyclesAtPark(1);
        resultAllAvailableBicycles = setBikes.size() > 0;
        assertEquals(expected, resultAllAvailableBicycles);
    }

    @Test
    public void getUnlockedBicTimeTest(){
        long expected = 86400000;
        long result = checkBicycleAvAtParkCtrl.getUnlockedBicTime("PT001");
        assertEquals(expected, result);
    }

    @Test
    void getAllAvailableBicyclesAtParkByCoordinates() {

        double latitude = 12.123;
        double longitude = -4.123;

        Set<Bicycle> setBikes = checkBicycleAvAtParkCtrl.getAllAvailableBicyclesAtParkByCoordinates(latitude,longitude);

        resultAllAvailableBicycles = setBikes.size() > 0;
        assertEquals(expected, resultAllAvailableBicycles);

    }

    @Test
    void suggestEletricalBicyclesToGoFromOneParkToAnother() {

        double latitude = 12.123;
        double latitude2 = 15.0;
        double longitude = -4.123;
        double longitude2 = -5.0;
        String username = "JPO";
        int expected = 3;


        Set<Bicycle> result = checkBicycleAvAtParkCtrl.suggestEletricalBicyclesToGoFromOneParkToAnother(latitude,longitude,latitude2,longitude2,username, "output");

        assertEquals(expected,result.size());
    }
}
