package lapr.project.controller;

import lapr.project.data.UserDB;
import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateNewUserControllerTest {

    private boolean expected;
    private boolean result;

    @Mock
    private UserDB userDB;

    @InjectMocks
    private CreateNewUserController newUserCtrl;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        result = false;
        expected = true;
        User temp = new User ("Carlitos", 13579, "1988-01-01", 1.70, 69, 12344312, "uasdasdasd@gmail.com", 6.25);
        when(userDB.addUser("Carlitos", 13579, "1988-01-01", 1.70, 69, 12344312, "uasdasdasd@gmail.com", 6.25)).thenReturn(temp);
    }

    @Test
    void addUserTest(){
        User us = newUserCtrl.addUser("Carlitos", 13579, "1988-01-01", 1.70, 69, 12344312, "uasdasdasd@gmail.com", 6.25);
        result = us != null ;
        assertEquals(expected, result);
    }

}