package lapr.project.controller;

import lapr.project.data.*;
import lapr.project.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DifferentRoutesBetweenTwoParksControllerTest {

    @Mock
    private ParkDB parkDB;
    @Mock
    private PoiDB poiDB;
    @Mock
    private UserDB userDB;

    @Mock
    private WindDB windDB;

    private Location park1, park2, poi1, poi2, poi3;
    private User user;
    private Wind wind1, wind2, wind3, wind4, wind5, wind6, wind7, wind8, wind9, wind10;

    @InjectMocks
    private DifferentRoutesBetweenTwoParksController meerbtpController;

    @BeforeEach
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        poi1 = new Poi("Serralves",41.159678, -8.659598,10);
        poi2 = new Poi("NorteShopping",41.181149,-8.654071,20);
        poi3 = new Poi("Botanico",41.153300,-8.641213,50);
        poi1.setLocationId(3);
        poi2.setLocationId(4);
        poi3.setLocationId(5);
        ((Poi) poi1).setPoiId(1);
        ((Poi) poi2).setPoiId(2);
        ((Poi) poi3).setPoiId(3);

        park1 = new Park("Parque",41.169420, -8.672861, 0);
        park1.setLocationId(1);
        int pickUp = 1;

        park2 = new Park("Boavista",41.157883, -8.629217, 122);
        park2.setLocationId(2);
        int dropOff = 2;

        wind1 = new Wind(1,3,10.0,220,0.185,"uni");
        wind2 = new Wind(1,4,10.0,220,0.185,"bi");
        wind3 = new Wind(1,5,10.0,220,0.185,"bi");
        wind5 = new Wind(2,3,10.0,220,0.185,"uni");
        wind6 = new Wind(2,4,10.0,220,0.185,"bi");
        wind7 = new Wind(2,5,10.0,220,0.185,"uni");
        wind8 = new Wind(4,3,10.0,220,0.185,"bi");
        wind9 = new Wind(3,5,10.0,220,0.185,"bi");
        wind10 = new Wind(4,5,10.0,220,0.185,"uni");


        ((Park) park1).setParkId(1);
        ((Park) park2).setParkId(2);


        Set<Park> parkSet = new HashSet<>();
        parkSet.add((Park)park1);
        parkSet.add((Park)park2);


        Set<Poi> poiSet = new HashSet<>();
        poiSet.add((Poi) poi1);
        poiSet.add((Poi) poi2);
        poiSet.add((Poi) poi3);

        Set<Wind> allWind = new HashSet<>();
        allWind.add(wind1);
        allWind.add(wind2);
        allWind.add(wind3);
        allWind.add(wind5);
        allWind.add(wind6);
        allWind.add(wind7);
        allWind.add(wind8);
        allWind.add(wind9);
        allWind.add(wind10);

        user = new User("user1",111234521,"2000-07-31",1.78,75,4430178955563528L,"email@mail.com", 6.25);

        when(poiDB.addPoi(new Poi("Serralves",41.159678, -8.659598,10))).thenReturn((Poi) poi1);
        when(poiDB.addPoi(new Poi("NorteShopping",41.181149,-8.654071,20))).thenReturn((Poi)poi2);
        when(poiDB.addPoi(new Poi("Botanico",41.153300,-8.641213,50))).thenReturn((Poi)poi3);
        when(poiDB.getAllPoi()).thenReturn(poiSet);

        when(userDB.addUser("user1",111234521,"2000-07-31",1.78,75,4430178955563528L,"email@mail.com",6.25)).thenReturn(user);

        when(parkDB.getPark(pickUp)).thenReturn((Park) park1);
        when(parkDB.getPark(dropOff)).thenReturn((Park) park2);
        when(parkDB.getAllParks()).thenReturn(parkSet);

        when(poiDB.getPoi(1)).thenReturn((Poi) poi1);
        when(poiDB.getPoi(2)).thenReturn((Poi) poi2);
        when(poiDB.getPoi(3)).thenReturn((Poi) poi3);

        when(windDB.getAllWind()).thenReturn(allWind);

        meerbtpController.setIntermediateLocationArray(new ArrayList<>());

        when(userDB.getUserByUsername("JPO")).thenReturn(user);
        when(parkDB.getParkByCoordinates(12.123,-4.123)).thenReturn(1);
        when(parkDB.getParkByCoordinates(15.0,-5.0)).thenReturn(2);
    }


    @Test
    void mostEnergeticallyEfficientRoute() {

        Map<Location[],Double> result = meerbtpController.mostEnergeticallyEfficientRoute(user,1,2,15.0,0.50, 2);

        int expectedSize = 3;
        double expectedLat1 = 41.16942;
        double expectedLat2 = 41.181149;
        double expectedLat3 = 41.157883;

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expectedSize,array.length);
        assertEquals(expectedLat1,array[0].getLatitude());
        assertEquals(expectedLat2,array[1].getLatitude());
        assertEquals(expectedLat3,array[2].getLatitude());

    }

    @Test
    void shortestRoute() {

        Map<Location[],Double> result = meerbtpController.mostEnergeticallyEfficientRoute(user,1,2,15.0,0.99, 1);

        int expectedSize = 3;
        double expectedLat1 = 41.16942;
        double expectedLat2 = 41.181149;
        double expectedLat3 = 41.157883;

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expectedSize,array.length);
        assertEquals(expectedLat1,array[0].getLatitude());
        assertEquals(expectedLat2,array[1].getLatitude());
        assertEquals(expectedLat3,array[2].getLatitude());

    }

    @Test
    void otherChoices() {
        Map<Location[],Double> result = meerbtpController.mostEnergeticallyEfficientRoute(
                user,1,2,15.0,50.0, 3);
        int expectedSize = 0;

        assertEquals(0,result.size());

    }

    @Test
    void mostEnergeticallyEfficientRouteWithIntermmediateLocationsTest1(){

        meerbtpController.addIntermediatePOI(1);
        meerbtpController.addIntermediatePOI(2);
        meerbtpController.addIntermediatePOI(3);
        meerbtpController.numberOfRoutes(2);
        meerbtpController.addSortingCriteria(1);
        double expectedLenght = 10.68;

        Map<Location[],Double> result = meerbtpController.mostEnergeticallyEfficientRoute(user,1,2,15.0,0.5, 1);

        int expectedSize = 5;

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expectedSize,array.length);
        assertEquals(expectedLenght,result.values().iterator().next(),0.5);

    }

    @Test
    void mostEnergeticallyEfficientRouteWithIntermmediateLocationsTest2(){

        meerbtpController.addIntermediatePOI(1);
        meerbtpController.addIntermediatePOI(2);
        meerbtpController.addIntermediatePOI(3);
        meerbtpController.numberOfRoutes(2);
        meerbtpController.addSortingCriteria(1);
        double expectedEnergy = 129657.98;

        Map<Location[],Double> result = meerbtpController.mostEnergeticallyEfficientRoute(user,1,2,15.0,0.5, 2);

        int expectedSize = 5;

        Set<Location[]> aux = result.keySet();
        Location[] array = aux.iterator().next();

        assertEquals(expectedSize,array.length);
        assertEquals(expectedEnergy,result.values().iterator().next(),0.5);

    }


    @Test
    void calculateEletricalEnergyToTravelFromOneLocationToAnother() {

        String username = "JPO";
        double latitude = 12.123;
        double latitude2 = 15.0;
        double longitude = -4.123;
        double longitude2 = -5.0;

        double expected = 0.19;

        double result = meerbtpController.calculateEletricalEnergyToTravelFromOneLocationToAnother(latitude,longitude,latitude2,longitude2,username);

        assertEquals(expected,result);

    }
}