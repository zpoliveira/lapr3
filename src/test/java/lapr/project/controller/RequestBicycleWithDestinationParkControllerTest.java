/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javafx.util.Pair;
import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.data.WindDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author anacerqueira
 */

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RequestBicycleWithDestinationParkControllerTest {

    private User user;
    private Park park1, park2, park3, park4;
    private Pair<Double, Double> windInfo;
    private Set<Park> allParks;
    private Set<Bicycle> availableBicycles;
    private Requisition requisition;
    private Date initialDate;
    private Bicycle b1, b2, b3, b4;

    @Mock
    private RequisitionDB requisitionDB;
    @Mock
    private ParkDB parkDB;
    @Mock
    private BicycleDB bicycleDB;
    @Mock
    private UserDB userDB;
    @Mock
    private WindDB windDB;
        
    @InjectMocks
    private RequestBicycleWithDestinationParkController rbwdpc;

    @BeforeAll
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        user = new User("name1", 123456789, "2000-10-10", 1.55d, 80d, 4444123443215768l, "mail@mail.com");
        user.setUserId(10);
        user.setState(1);
     
        park1 = new Park("Parque", 41.169420, -8.672861, 0);
        park2 = new Park("Boavista", 41.157883, -8.629217, 122);
        park3 = new Park("Boavista", 41.157883, -200, 122);
        park4 = new Park("Boavista", 200, -8.629217, 122);
        park1.setParkId(1);
        park2.setParkId(2);
        park3.setParkId(3);
        park4.setParkId(4);
        windInfo = new Pair<>(0d, 0d);
        initialDate = new Date(1000);

        b1 = new Bicycle(3, 2, 0.4, 1, 15);
        b2 = new Bicycle(3, 2, 0.6, 1, 15);
        b3 = new Bicycle(3, 2, 0.7, 1, 15);
        b4 = new Bicycle(3, 2, 0.3, 1, 15);

        availableBicycles = new HashSet<>();
        availableBicycles.add(b1);
        availableBicycles.add(b2);
        availableBicycles.add(b3);
        availableBicycles.add(b4);

        allParks = new HashSet<>();
        allParks.add(park1);
        allParks.add(park2);
        allParks.add(park3);
        allParks.add(park4);
        
        requisition = new Requisition(1, b4, park1, park2, initialDate);

        when(parkDB.getAllParks()).thenReturn(allParks);

        when(parkDB.getPark(1)).thenReturn(park1);
        when(parkDB.getPark(2)).thenReturn(park2);
        when(parkDB.getPark(3)).thenReturn(park3);
        when(parkDB.getPark(4)).thenReturn(park4);

        when(userDB.getUser(1)).thenReturn(user);

        when(windDB.getWindInfo(Mockito.anyInt(), Mockito.anyInt())).thenReturn(new Pair<>(0d, 0d));

        when(bicycleDB.getAllAvailableBicyclesWithGivenPower(user, park1, park2, windDB.getWindInfo(1, 2).getKey(), windDB.getWindInfo(1, 2).getValue())).thenReturn(availableBicycles);
        
        when(requisitionDB.addRequisition(requisition)).thenReturn(requisition);

    }

    /**
     * Test of getAllParks method, of class ReturnBicycleWithDestinationPark.
     */
    @Test
    public void testGetAllParks() {
        Set<Park> expeted = allParks;
        Set<Park> result = rbwdpc.getAllParks();
        assertEquals(expeted, result);

    }
     /**
     * Test of getAllParks method, of class ReturnBicycleWithDestinationPark.
     */
    @Test
    public void testGetAllAvailableBicyclesAtPark(){
        Set<Bicycle> expeted = availableBicycles;
        Set<Bicycle> result = rbwdpc.getAvailableBicycle(1, 1, 2);
        assertEquals(expeted, result);
    }
    

    /**
     * Test of newRequisition method, of class ReturnBicycleWithDestinationPark.
     */
    @Test
    public void testNewRequisition() {
        boolean result = rbwdpc.newRequisition(1, b4, park1, park2, initialDate);
        assertTrue(result);
        

    }

}
