/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.HashSet;
import java.util.Set;

import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RemoveBicycleControllerTest {

    private boolean expected;
    private boolean resultAllAvailableBicycles;
    private boolean resultRemoveBicycle;
    private boolean resultRemoveBicycleUnsuccess;

    @Mock
    private BicycleDB bicycleDB;

    @InjectMocks
    private RemoveBicycleController removeBicycleCtrl;
    
    
    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        resultRemoveBicycleUnsuccess = true;
        Bicycle b = new Bicycle(1, 2, 55, 100, 20);
        b.setBicycleID(1);

        when(bicycleDB.removeBicycle(1)).thenReturn(true);
        when(bicycleDB.removeBicycle(0)).thenReturn(false);

        Bicycle b1 = new Bicycle(1, 1, 20, 100, 15);
        b1.setBicycleID(1);
        Bicycle b2 = new Bicycle(1, 1, 20, 100, 15);
        b2.setBicycleID(2);
        Bicycle b3 = new Bicycle(1, 1, 20, 100, 15);
        b3.setBicycleID(3);

        Set<Bicycle> setBikes = new HashSet<>();
        setBikes.add(b1);
        setBikes.add(b2);
        setBikes.add(b3);
        when(bicycleDB.getAllAvailableBicycles()).thenReturn(setBikes);

        expected = true;
    }


    /**
     * Success test of testRemoveBicycle method, of class RemoveBicycleController.
     */
    @Test
    public void testRemoveBicycle() {
        resultRemoveBicycle = removeBicycleCtrl.removeBicycle(1);
        assertEquals(expected, resultRemoveBicycle);
    }

    /**
     * Unsuccess test of testRemoveBicycle method of class RemoveBicycleController.
     */
    @Test
    public void testRemoveBicycleUnsuccess() {
        resultRemoveBicycleUnsuccess = removeBicycleCtrl.removeBicycle(0);
        assertFalse(resultRemoveBicycleUnsuccess);
    }

    /**
     * Test of getAllAvailableBicycles method, of class RemoveBicycleController.
     */
    @Test
    public void testGetAllAvailableBicycles() {
        Set<Bicycle> result = removeBicycleCtrl.getAllAvailableBicycles();
        resultAllAvailableBicycles = result.size() > 0;
        assertEquals(expected, resultAllAvailableBicycles);
    }

}
