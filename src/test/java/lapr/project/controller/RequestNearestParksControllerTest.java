package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RequestNearestParksControllerTest {

    private double latitude, longitude, altitude;
    private Park park1, park2, park3;
    private Set<Park> allPark;

    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private RequestNearestParksController requestNearestParksCntr;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        latitude = 10.0;
        longitude = 5.0;
        altitude = 0.0;
        allPark = new HashSet<>();
        park1 = new Park("1",41.0,8.0,10.0);
        park1.setParkId(1);
        park2 = new Park("2",41.5,8.3,0.0);
        park2.setParkId(2);
        park3 = new Park("3",42.0,9.0,20.0);
        park3.setParkId(3);

        allPark.add(park1);
        allPark.add(park2);
        allPark.add(park3);

        when(parkDB.getAllParks()).thenReturn(allPark);

    }

    @Test
    void listNearestParks() {
        Map<Park,Double> orderedMap = requestNearestParksCntr.listNearestParks(latitude,longitude,altitude);
        double expectedDistance = 3460.70;
        double instanceDistance = orderedMap.values().iterator().next();
        assertEquals(expectedDistance,instanceDistance,0.5);
    }
}