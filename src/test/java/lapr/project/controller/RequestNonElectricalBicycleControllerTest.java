package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RequestNonElectricalBicycleControllerTest {

    private Bicycle b, b2;
    private Park p1, p2;
    private Date d1, d2;
    private User u1;
    private Requisition expected, expected2, requisition;
    private Set<Bicycle> bicycleSet;

    @Mock
    private RequisitionDB requisitionDB;

    @Mock
    private UserDB userDB;

    @Mock
    private ParkDB parkDb;

    @InjectMocks
    private RequestNonElectricalBicycleController rnebController;

    @BeforeEach
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        b = new Bicycle(1,1,50,100,1.2);
        b.setBicycleID(1);
        b.setTypeDescription("non-electric");
        b2 = new Bicycle(2,1,50,100,1.2);
        b2.setBicycleID(1);
        b2.setTypeDescription("electric");
        p1 = new Park();
        p2 = new Park();
        p1.setParkId(1);
        p2.setParkId(2);
        u1 = new User("AA",123456789,"1990-05-05",190.0,80.0,123412341234L,"aaa@aaa.com");
        u1.setUserId(1);
        d1 = new Date(10000);
        d2 = new Date(20000);
        expected = new Requisition(1,b,p1,p2,d1,d2);
        requisition = new Requisition(1,b,p1,p2,d1,d2);
        expected2 = new Requisition(1,b2,p1,p1,d1,d1);

        bicycleSet = new HashSet<>();

        when(requisitionDB.addRequisition(requisition)).thenReturn(expected);
        when(userDB.getUserByUsername("AA")).thenReturn(u1);
        when(parkDb.getPark(b.getParkID())).thenReturn(p1);


    }


    @Test
    void setData() {
        Requisition instance = rnebController.setData(1,b,p1,p2);

        assertEquals(expected.getUserID(),instance.getUserID());
        assertEquals(expected.getBicycle(),instance.getBicycle());
        assertEquals(expected.getPickUpPark(),instance.getPickUpPark());
        assertEquals(expected.getDropOffPark(),instance.getDropOffPark());
    }

    @Test
    void unlockAnyBicycleAtParkTest(){
        Requisition expected = null;
        Requisition result = rnebController.unlockAnyBicycleAtPark(bicycleSet,"AA");
        assertEquals(expected,result);
    }

    @Test
    void unlockAnyBicycleAtParkTest2(){
        bicycleSet.add(b);
        bicycleSet.add(b2);
        Requisition expected = expected2;
        Requisition result = rnebController.unlockAnyBicycleAtPark(bicycleSet,"AA");
        assertEquals(expected,result);
    }

    @Test
    void unlockAnyBicycleAtParkTest3(){
        bicycleSet.add(b);
        bicycleSet.add(b2);
        u1.setUserId(0);
        Requisition expected = null;
        Requisition result = rnebController.unlockAnyBicycleAtPark(bicycleSet,"AA");
        assertEquals(expected,result);
    }
}