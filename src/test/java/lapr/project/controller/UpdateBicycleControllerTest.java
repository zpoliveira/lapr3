/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.BicycleTypeDB;
import lapr.project.model.Bicycle;
import lapr.project.model.BicycleType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UpdateBicycleControllerTest {

    private Bicycle expected, b1;
    private BicycleType bType;

    @Mock
    private BicycleTypeDB bicycleTypeDB;

    @Mock
    private BicycleDB bicycleDB;

    @InjectMocks
    private UpdateBicycleController updateBicycleController;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        expected = new Bicycle(1, 104, 20, 100, 15);
        b1 = new Bicycle(1, 104, 20, 100, 15);
        expected.setBicycleID(125);
        b1.setBicycleID(125);
        bType = new BicycleType(1, "eletric", 0.9);

        when(bicycleDB.getBicycleById(125)).thenReturn(b1);
        when(bicycleTypeDB.getBicycleType("eletric")).thenReturn(bType);
        when(bicycleDB.updateBicycle(b1)).thenReturn(true);

    }

    /**
     * Test of getBicycleById method, of class UpdateBicycleController.
     */
    @Test
    public void testGetBicycleById() {
        Bicycle result = updateBicycleController.getBicycleById(125);
        assertEquals(expected, result);
    }

    /**
     * Test of getBicycleById method, of class UpdateBicycleController.
     */
    @Test
    public void testGetBicycleType() {
        int result = updateBicycleController.getBicycleType("eletric");
        assertEquals(1, result);
    }

    /**
     * Test of updateBicycle method, of class UpdateBicycleController.
     */
    @Test
    public void testUpdateBicycle() {
        setUp();
        boolean result = updateBicycleController.updateBicycle(125, 1, 104, 20, 100, 15, "new Bike", 1.12);
        assertEquals(true, result);
    }

}
