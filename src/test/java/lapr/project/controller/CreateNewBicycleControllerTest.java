package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;
import static org.junit.jupiter.api.Assertions.*;

import lapr.project.utils.ImportFiles;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateNewBicycleControllerTest {

    private boolean expected;
    private boolean result;

    @Mock
    private BicycleDB bicycleDB;

    @Mock
    private ImportFiles importFiles;
    
    @InjectMocks
    private static CreateNewBicycleController cnbController;
    
    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        result=false;
        expected=true;
        Bicycle temp = new Bicycle(1, 1, 75, 1, 20);
        when(bicycleDB.addBicycle(temp,"other", 41.169420, -8.672861)).thenReturn(temp);
        when(importFiles.importBicycles(anyString())).thenReturn(5);
    }
    

    @Test
    void addBicycleTest(){
        Bicycle b = cnbController.addBicycle("PT050",75, 1, 20,
                1.10, "other",41.169420, -8.672861);
        result = b != null;
        assertEquals(expected, result);
    }

    @Test
    void addImportBicycleTest(){
        int result = cnbController.importBicycles("importFolder/bicycles_test.csv");
        assertEquals(expected, result > 0);
    }
    
}
