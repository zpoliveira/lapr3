package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import lapr.project.utils.AuxMethods;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CheckDistandBetweenParksControllerTest {

    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private CheckDistandBetweenParksController cdbpController;

    private Park parkOrigin, parkDestiny1, parkDestiny2;
    private int parkId1, parkId2, parkId3;


    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        parkOrigin= new Park("Parque",41.169420, -8.672861, 0);
        parkOrigin.setParkId(1);
        parkId1=parkOrigin.getParkId();
        parkDestiny1=new Park("Boavista",41.157883, -8.629217, 122);
        parkDestiny1.setParkId(2);
        parkId2=parkDestiny1.getParkId();
        parkDestiny2 = new Park("Quinta", 41.195207, -8.687552, 0);
        parkDestiny2.setParkId(3);
        parkId3=parkDestiny2.getParkId();

        when(parkDB.getPark(parkId1)).thenReturn(parkOrigin);
        when(parkDB.getPark(parkId2)).thenReturn(parkDestiny1);
        when(parkDB.getPark(parkId3)).thenReturn(parkDestiny2);

    }


    @Test
    void getDistanceBetweenParksNoAltitude() {
        double expectedDist=3.12;
        double resultDist = cdbpController.getDistanceBetweenParks(parkId1, parkId3);
        assertEquals(expectedDist, resultDist,0.05);
    }

    @Test
    void getDistanceBetweenParksWithAltitude() {
        double expectedDist=3.88;
        double resultDist = cdbpController.getDistanceBetweenParks(parkId1, parkId2);
        assertEquals(expectedDist, resultDist,0.05);
    }


    @Test
    void getDistanceFromLocationToAnother() {

        double latitude1 = 10.0;
        double longitude1 = 20.0;
        double latitude2 = 20.0;
        double longitude2 = 30.0;

        int expected = 1545242;
        int instance = cdbpController.getDistanceFromLocationToAnother(latitude1,longitude1,latitude2,longitude2);

        assertEquals(expected,instance);

    }
}