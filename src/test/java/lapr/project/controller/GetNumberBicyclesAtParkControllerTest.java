package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anySet;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetNumberBicyclesAtParkControllerTest {

    private boolean expected = true;
    private int result = -1;

    @Mock
    private BicycleDB bicycleDB;

    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private GetNumberBicyclesAtParkController gtp;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);

        Bicycle bTemp2 = new Bicycle("PT050", 75, 1, 12, 1.1);
        bTemp2.setBicycleID(1);
        bTemp2.setAvaliability(true);
        bTemp2.setParkID(1);

        Bicycle bTemp3 = new Bicycle("PT051", 65, 1, 12, 1.1);
        bTemp3.setBicycleID(2);
        bTemp3.setAvaliability(true);
        bTemp3.setParkID(1);

        Set<Bicycle> set = new TreeSet<>();
        set.add(bTemp2);
        set.add(bTemp3);
        Park p = new Park();
        p.setParkId(1);

        when(bicycleDB.getAllBicycles()).thenReturn(set);
        when(parkDB.getPark(1)).thenReturn(p);
        when(parkDB.getParkByCoordinates(anyDouble(), anyDouble())).thenReturn(1);

    }



    @Test
    void getNumberBicyclesAtPark(){
        double latitude = 41.16942;
        double longitude = -8.672861;
        String outputFileName = "teste.csv";

        result = gtp.getNumberOfBicyclesAtPark(latitude, longitude, outputFileName);
        assertEquals(expected, result != -1);
    }
}