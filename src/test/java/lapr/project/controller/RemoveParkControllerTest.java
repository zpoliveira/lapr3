package lapr.project.controller;

import lapr.project.data.ParkDB;
import lapr.project.model.Park;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RemoveParkControllerTest {

    private Park before;

    @Mock
    private ParkDB parkDB;

    @InjectMocks
    private RemoveParkController removeParkCtrl;

    @BeforeAll
    private void setUp(){
        MockitoAnnotations.initMocks(this);
        before = new Park("teste1", 12.123, -4.123, 120);
        before.setParkId(1);

        when(parkDB.removePark(1)).thenReturn(true);
        when(parkDB.removePark(0)).thenReturn(false);
    }

    @Test
    void removeParkControllerSuccess() {
        boolean call;
        int parkId= before.getParkId();
        call = removeParkCtrl.removePark(parkId);
        assertTrue(call);
    }

    @Test
    void removeParkControllerUnsuccess() {
        boolean call;
        call = removeParkCtrl.removePark(0);
        assertFalse(call);
    }
}