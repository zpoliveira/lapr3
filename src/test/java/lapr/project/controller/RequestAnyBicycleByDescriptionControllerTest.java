package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.RequisitionDB;
import lapr.project.data.UserDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Requisition;
import lapr.project.model.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RequestAnyBicycleByDescriptionControllerTest {

    private static boolean expected = true;
    private static boolean result;
    private static Bicycle b;
    private static User u;

    @Mock
    private RequisitionDB requisitionDB;
    @Mock
    private ParkDB parkDB;
    @Mock
    private BicycleDB bicycleDB;
    @Mock
    private UserDB userDB;

    @InjectMocks
    private  RequestAnyBicycleByDescriptionController reqAny;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        b = new Bicycle();
        b.setBicycleDescription("PT051");
        b.setAvaliability(true);
        b.setParkID(1);
        u = new User("Carlitos", 247250970, "", 1.6 , 68, 1234567,
        "", 1.19);

        when(requisitionDB.addRequisition(any())).thenReturn(new Requisition());
        when(parkDB.getPark(b.getParkID())).thenReturn(new Park());
        when(userDB.getUserByUsername(anyString())).thenReturn(u);
        when(bicycleDB.getBicycleByDescription(anyString())).thenReturn(b);
    }

    @Test
    void requestyAnyBicycleByDescription() {
        long response = -1;
        response = reqAny.requestyAnyBicycleByDescription(b.getBicycleDescription(), u.getName());
        assertEquals(expected, response != -1);
    }
}