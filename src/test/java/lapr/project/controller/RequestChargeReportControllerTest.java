package lapr.project.controller;

import lapr.project.data.BicycleDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RequestChargeReportControllerTest {

    private boolean expected;
    private int result;
    private int parkId;
    private int parkId3;
    Park park;

    @Mock
    private BicycleDB bicycleDB;

    @InjectMocks
    private RequestChargeReportController requestChrgRptController;

    @BeforeAll
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        expected = true;
        result = -1;
        Bicycle b1 = new Bicycle(1, 2, 55, 1, 20);
        b1.setAvaliability(true);
        Bicycle b2 = new Bicycle(1, 2, 20, 1, 19);
        b2.setAvaliability(true);
        Bicycle b3 = new Bicycle(1, 2, 10, 1, 15);
        b3.setAvaliability(true);

        Set<Bicycle> sBic = new HashSet<>();
        sBic.add(b1);
        sBic.add(b2);
        sBic.add(b3);

        park = new Park();
        park.setParkId(2);
        park.setInputCurrent(16);
        park.setInputVoltage(220);

        when(bicycleDB.getAllAvailableBicyclesOrderdByHigherBatteryLevel(park.getParkId())).thenReturn(sBic);
    }


    @Test
    void requestChargeReport() {
        Map<Bicycle, String> map = requestChrgRptController.requestChargeReport(park);
        result = map.size();
        assertEquals(expected, result > 0);
    }

    @Test
    void requestBicycleThatAreChargingReport() {

        int expected = 2;

        Bicycle b4 = new Bicycle();
        b4.setBicycleDescription("aa");
        Bicycle b5 = new Bicycle();
        b5.setBicycleDescription("bb");
        Bicycle b6 = new Bicycle();
        b6.setBicycleDescription("cc");
        String s1 = "0:0:0";
        String s2 = "1:2:3";
        String s3 = "4:5:6";

        Map<Bicycle, String> map = new TreeMap<>();
        Map<Bicycle, String> newMap = new TreeMap<>();

        map.put(b4,s1);
        map.put(b5,s2);
        map.put(b6,s3);

        RequestChargeReportController rr = new RequestChargeReportController();

        newMap = rr.requestBicycleThatAreChargingReport(map);

        assertEquals(expected, newMap.size());


    }
}