package lapr.project.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PoiTest {

    private static Poi p1;
    private static Poi p2;
    private static Poi p3;
    private static Poi p4;
    private static Poi p5;

    @BeforeAll
    private static void setUp(){
        p1 = new Poi("name1", 1, 1, 1);
        p2 = new Poi("name2", -91, 1, 1);
        p3 = new Poi("name3", 22, 181, 1);
        p4 = new Poi("name4", -91, 181, -100);
        p5 = new Poi("name5", 0, 0, -0);
    }

    @Test
    void getPoiId() {
        p1.setPoiId(1);
        assertEquals(1, p1.getPoiId());
    }

    @Test
    void getName() {
        assertEquals("name1", p1.getName());
    }

    @Test
    void getLatitude() {
        assertEquals(1, p1.getLatitude());
    }

    @Test
    void getLongitude() {
        assertEquals(1, p1.getLongitude());
    }

    @Test
    void getAltitude() {
        assertEquals(1, p1.getAltitude());
    }

    @Test
    void getLocationId() {
        p5.setLocationId(20);
        assertEquals(20, p5.getLocationId());
    }

    @Test
    void equalsTest() {
        p1.setPoiId(1);
        Poi p2 = new Poi("", 2, 2, 2);
        p2.setName("nome2");
        Poi p3 = p1;
        p2.setPoiId(1);

        assertEquals(p1, p2);
        assertEquals(p1, p3);
        assertNotEquals(p1, null);
        assertNotEquals(p1, new Object());
    }

    @Test
    void isValid() {
        assertEquals(true, p1.isValid());
    }

    @Test
    void isValid_notValid1() {
        assertEquals(false, p2.isValid());
    }

    @Test
    void isValid_notValid2() {
        assertEquals(false, p3.isValid());
    }

    @Test
    void isValid_notValid3() {
        assertEquals(false, p4.isValid());
    }


    @Test
    void hashCodeTest() {
        int hash = p1.hashCode();
        assertTrue(hash>0);
    }

    @Test
    void EmptyConstructorTest(){
        Poi p0 = new Poi();
        if(p0==null){
            assertTrue(false);
        }else{
            assertTrue(true);
        }
            
    }


}
