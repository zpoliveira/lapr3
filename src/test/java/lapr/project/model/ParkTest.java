package lapr.project.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ParkTest {

    Park p, p2;

    @BeforeAll
    void setUp(){
        p = new Park("nome",1,1,1);
        p2 = new Park("name2", 2.0, 2.0, 200, 20, 30, 220, 16);
        p2.setLocationId(123);
    }

    @Test
    void getParkId() {
        p.setParkId(1);
        assertEquals(1,p.getParkId());
    }

    @Test
    void getName() {
        p.setName("nome");
        assertEquals("nome",p.getName());
    }

    @Test
    void getLatitude() {
        assertEquals(1,p.getLatitude());
    }

    @Test
    void getLongitude() {
        assertEquals(1,p.getLongitude());
    }

    @Test
    void getAltitude() {
        assertEquals(1,p.getAltitude());
    }

    @Test
    void equalsTest() {

        p.setParkId(1);
        Park p2 = new Park("nome2",2,2,2);
        Park p3 = p;
        p2.setParkId(1);

        assertEquals(p,p2);
        assertEquals(p,p3);
        assertNotEquals(p,null);
        assertNotEquals(p,new Object());
    }

    @Test
    void hashCodeTest() {
        int hash = p.hashCode();
        assertTrue(hash>0);
    }
    
    @Test
    void getMaxElectricalLotationTest(){
        p.setMaxElectricalLotation(22);
        assertEquals(22, p.getMaxElectricalLotation());
    }

    @Test
    void getMaxNonElectricalLotationTest(){
        p.setMaxNonElectricalLotation(22);
        assertEquals(22, p.getMaxNonElectricalLotation());
    }

    @Test
    void getMaxLotationsTest(){
        p.setMaxNonElectricalLotation(50);
        p.setMaxElectricalLotation(20);
        assertEquals(70,p.getMaxLotation());
    }

    @Test
    void getElectricalLotationTest(){
        setUp();
        assertEquals(20, p2.getMaxElectricalLotation());
    }

    @Test
    void getNonElectricalLotationTest(){
        setUp();
        assertEquals(30, p2.getMaxNonElectricalLotation());
    }

    @Test
    void setInputVoltageTest(){
        p2.setInputVoltage(200);
        assertEquals(200, p2.getInputVoltage());
    }

    @Test
    void setInputCurrentTest(){
        p2.setInputCurrent(20);
        assertEquals(20, p2.getInputCurrent());
    }

    @Test
    void setElectricalLotationTest(){
        p2.setElectricalLotation(100);
        assertEquals(100, p2.getElectricalLotation());
    }

    @Test
    void setNonElectricalLotationTest(){
        p2.setNonElectricalLotation(100);
        assertEquals(100, p2.getNonElectricalLotation());
    }


    @Test
    void getAvailableElectricalSlotsTest(){
        p2.setElectricalLotation(70);
        p2.setMaxElectricalLotation(100);
        assertEquals(30, p2.getAvailableElectricalSlots());
    }

    @Test
    void getAvailableNonElectricalSlotsTest(){
        p2.setNonElectricalLotation(20);
        p2.setMaxNonElectricalLotation(100);
        assertEquals(80, p2.getAvailableNonElectricalSlots());
    }

}