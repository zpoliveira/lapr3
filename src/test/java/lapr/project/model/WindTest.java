package lapr.project.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WindTest {
    
    private static Wind w1;
    private static Wind w2;
    private static Wind w3;
    private static Wind w4;
    
    public WindTest() {
    }
    
    @BeforeAll
    private static void setUp(){
        w1 = new Wind(1,2, 1.0, 1.5, 0.1,"uni");
        w2 = new Wind(2,3, 1.5, 1.6, 0.1,"uni");
        w3 = new Wind(3,4, 2.1, 3.1, 0.1,"uni");
        w4 = new Wind();

    }    

    @Test
    public void testGetInitiallocationId() {
        System.out.println("testGetInitiallocationId");
        assertEquals(1, w1.getInitiallocationId());

    }

    @Test
    public void testGetFinallocationId() {
        System.out.println("testGetFinallocationId");
        assertEquals(2, w1.getFinallocationId());

    }

    @Test
    public void testGetWindspeed() {
        System.out.println("getWindspeed");
        assertEquals(1.5, w2.getWindspeed());
    }


    @Test
    public void testGetWindfactor() {
        System.out.println("getWindfactor");
        assertEquals(1.6, w2.getWindfactor());
    }

    @Test
    public void testEquals() {
        System.out.println("equals");      
        w4.setInitiallocationId(3);
        w4.setFinallocationId(4);
        w4.setWindspeed(2.1);
        w4.setWindfactor(3.1);
        
        Wind w5 = w3;
        
        assertEquals(w3, w4);
        assertEquals(w3, w5);
        assertEquals(w3.getInitiallocationId(), w4.getInitiallocationId());
        assertEquals(w3.getFinallocationId(), w4.getFinallocationId());
        assertNotEquals(w5.getInitiallocationId(), w2.getInitiallocationId());
        assertNotEquals(w5.getFinallocationId(),w2.getFinallocationId());
        assertNotEquals(w4, new Object());

    }


    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int hash = w1.hashCode();
        assertEquals( 994, hash);
    }

    @Test
    public void setAerodynamicCoefficientTest() {
        w1.setAerodynamicCoefficient(0.2);
        assertEquals( 0.2, w1.getAerodynamicCoefficient());
    }

    @Test
    public void setsetPathDirectionTest() {
        w2.setPathDirection("bi");
        assertEquals( "bi", w2.getPathDirection());
    }

    
}
