package lapr.project.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class RequisitionTest {

    static Requisition one = null;
    static Requisition two = null;

    @BeforeAll
    private static void setUp() {
        Bicycle b1 = new Bicycle();
        Bicycle b2 = new Bicycle();
        Park pu1 = new Park();
        Park pu2 = new Park();
        Park dp1 = new Park();
        Park dp2 = new Park();
        Date pd1 = new Date(2018);
        Date pd2 = new Date(2020);
        Date dd1 = new Date(2018);
        Date dd2 = new Date(2020);
        one = new Requisition(1, b1, pu1, dp1, pd1, dd1);
        two = new Requisition(2, b2, pu2, dp2, pd2, dd2);
        one.setRequisitionID(1);
        two.setRequisitionID(2);

    }

    @Test
    void getRequisitionID() {

        int expectedId = 1;

        assertEquals(expectedId, one.getRequisitionID());

    }

    @Test
    void getUserID() {

        int expectedId = 1;

        assertEquals(expectedId, one.getUserID());

    }

    @Test
    void getBicycle() {

        Bicycle expected = new Bicycle();

        assertEquals(expected, one.getBicycle());
    }

    @Test
    void getPickUpPark() {

        Park expected = new Park();

        assertEquals(expected, one.getPickUpPark());

    }

    @Test
    void getDropOffPark() {

        Park expected = new Park();

        assertEquals(expected, one.getDropOffPark());
    }

    @Test
    void getInitialDate() {

        Date expected = new Date(2018);

        assertEquals(expected, one.getInitialDate());

    }

    @Test
    void getFinalDate() {

        Date expected = new Date(2018);

        assertEquals(expected, one.getFinalDate());
    }

    @Test
    void emptyConstruct() {
        new Requisition();
    }

    @Test
    void equals() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park dp3 = new Park();
        Date pd3 = new Date(2018);
        Date dd3 = new Date(2020);
        Requisition expected = new Requisition(1, b3, pu3, dp3, pd3, dd3);
        expected.setRequisitionID(1);

        Requisition three = expected;

        assertEquals(expected, one);
        assertEquals(expected, three);
        assertNotEquals(three, null);
        assertNotEquals(three, new Object());
    }

    @Test
    void hashCodeTest() {

        int expected = 32;

        assertEquals(expected, one.hashCode());

    }

    @Test
    void setRequisitionID() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Date pd1 = new Date(2018);
        Requisition r1 = new Requisition(1, b3, pu3, pd1);
        r1.setRequisitionID(1);
        int expected = 1;

        assertEquals(expected, r1.getRequisitionID());
    }

    @Test
    void setUserID() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park dp3 = new Park();
        Date pd1 = new Date(2018);
        Requisition r1 = new Requisition(1, b3, pu3, dp3, pd1);
        r1.setUserID(2);
        int expected = 2;

        assertEquals(expected, r1.getUserID());
    }

    @Test
    void setBicycle() {

        Bicycle b3 = new Bicycle();
        Bicycle b4 = new Bicycle();
        b4.setBicycleID(3);
        Park pu3 = new Park();
        Park dp3 = new Park();
        Date pd1 = new Date(2018);
        Requisition r1 = new Requisition(1, b3, pu3, dp3, pd1);
        r1.setBicycle(b4);
        int expected = 3;

        assertEquals(expected, r1.getBicycle().getBicycleID());

    }

    @Test
    void setPickUpPark() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park pu4 = new Park();
        pu4.setParkId(4);
        Park dp3 = new Park();
        Date pd1 = new Date(2018);
        Requisition r1 = new Requisition(1, b3, pu3, dp3, pd1);
        r1.setPickUpPark(pu4);
        int expected = 4;

        assertEquals(expected, r1.getPickUpPark().getParkId());
    }

    @Test
    void setDropOffPark() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park pu4 = new Park();
        pu4.setParkId(4);
        Park dp3 = new Park();
        Date pd1 = new Date(2018);
        Requisition r1 = new Requisition(1, b3, pu3, dp3, pd1);
        r1.setDropOffPark(pu4);
        int expected = 4;

        assertEquals(expected, r1.getDropOffPark().getParkId());
    }

    @Test
    void setInitialDate() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park dp3 = new Park();
        Date pd1 = new Date(2018);
        Date dp2 = new Date(5000);
        Requisition r1 = new Requisition(1, b3, pu3, dp3, pd1);
        r1.setInitialDate(dp2);
        Date expected = dp2;

        assertEquals(expected, r1.getInitialDate());

    }

    @Test
    void setFinalDate() {

        Bicycle b3 = new Bicycle();
        Park pu3 = new Park();
        Park dp4 = new Park();
        Date pd1 = new Date(2018);
        Date dp2 = new Date(5000);
        Date dp3 = new Date(10000);
        Requisition r1 = new Requisition(1, b3, pu3, dp4, pd1, dp2);
        r1.setFinalDate(dp3);
        Date expected = dp3;

        assertEquals(expected, r1.getFinalDate());
    }
}
