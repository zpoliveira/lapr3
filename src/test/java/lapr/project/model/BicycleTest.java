/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.Test;

public class BicycleTest {

    Bicycle b1 = new Bicycle(1, 2, 0.75, 1, 20);
   

    @Test
    void testGetBicycleID() {
        b1.setBicycleID(1);
        assertEquals(1, b1.getBicycleID());
    }
    
    @Test
    void testGetTypeID() {
        assertEquals(1, b1.getTypeID());
    }

    @Test
    void testGetParkID() {
        assertEquals(2, b1.getParkID());
    }

    @Test
    void testGetBatteryLevel() {
        b1.setBatteryLevel(0.75);
        assertEquals(0.75, b1.getBatteryLevel());
    }

    @Test
    void testGetFullBatteryCapacity() {
        b1.setFullBatteryCapacity(1);
        assertEquals(1, b1.getFullBatteryCapacity());
    }

    @Test
    void TestGetWeight() {
        b1.setWeight(20);
        assertEquals(20, b1.getWeight());
    }

    @Test
    void getBicycleDescription() {
        b1.setBicycleDescription("Teste");
        assertEquals("Teste", b1.getBicycleDescription());
    }

    @Test
    void getAerodynamicCoefficient() {
        b1.setAerodynamicCoefficient(1.2);
        assertEquals(1.2, b1.getAerodynamicCoefficient());
    }

    @Test
    void TestEquals() {

        b1.setBicycleID(1);

        Bicycle b2 = new Bicycle(1, 2, 0.75, 1, 20);
        Bicycle b3 = b1;
        b2.setBicycleID(1);

        assertEquals(b1, b2);
        assertEquals(b1, b3);
        assertNotEquals(b1, null);
        assertNotEquals(b1, new Object());

    }
  @Test
    void hashCodeTest() {

        int hash = b1.hashCode();

        assertEquals(31,hash);
    }
}
