package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LocationTest {

    private Park p;

    @BeforeAll
    void initialize(){
        p = new Park();
        p.setName("name");
        p.setLocationId(1);
        p.setParkId(1);
    }

    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        double latitude = 1.0;
        Location instance = new Location(0, 1, 1) {
            @Override
            public boolean equals(Object o) {
                return true;
            }
        };
        instance.setLatitude(1);
        assertEquals(latitude, instance.getLatitude());
    }

    /**
     * Test of setLongitude method, of class Location.
     */
    @Test
    public void testSetLongitude() {
        System.out.println("setLongitude");
        double longitude = 1.0;
        Location instance = new Location(1, 0, 1) {
            @Override
            public boolean equals(Object o) {
                return true;
            }
        };
        instance.setLongitude(1);
        assertEquals(longitude, instance.getLongitude());

    }

    /**
     * Test of setAltitude method, of class Location.
     */
    @Test
    public void testSetAltitude() {
        System.out.println("setAltitude");
        double altitude = 1.0;
        Location instance = new Location(1, 1, 0) {
            @Override
            public boolean equals(Object o) {
                return true;
            }
        };
        instance.setAltitude(1);
        assertEquals(altitude, instance.getAltitude());

    }

    @Test
    public void isValidTest(){

        p.setLatitude(1);
        p.setLongitude(1);
        boolean expected=true;
        boolean result = p.isValid();
        assertEquals(expected, result);
    }

    @Test
    public void isValidTest2(){

        p.setLatitude(-91);
        p.setLongitude(1);
        boolean expected=false;
        boolean result = p.isValid();
        assertEquals(expected, result);
    }

    @Test
    public void isValidTest3(){

        p.setLatitude(1);
        p.setLongitude(-181);
        boolean expected=false;
        boolean result = p.isValid();
        assertEquals(expected, result);
    }

    @Test
    public void isValidTest4(){

        p.setLatitude(91);
        p.setLongitude(1);
        boolean expected=false;
        boolean result = p.isValid();
        assertEquals(expected, result);
    }

    @Test
    public void isValidTest5(){

        p.setLatitude(1);
        p.setLongitude(181);
        boolean expected=false;
        boolean result = p.isValid();
        assertEquals(expected, result);
    }
}
