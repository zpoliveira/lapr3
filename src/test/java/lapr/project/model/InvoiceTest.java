/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Ferreira
 */
public class InvoiceTest {

    Invoice i1 = new Invoice(50,20,1);

    /**
     * Test of getInvoiceID method, of class Invoice.
     */
    @Test
    public void testGetInvoiceID() {

        i1.setInvoiceID(2);
        int result = i1.getInvoiceID();
        int expected = 2;
        assertEquals(expected, result);

    }

    /**
     * Test of getTotal method, of class Invoice.
     */
    @Test
    public void testGetTotal() {
        i1.setTotal(60);
        double result = i1.getTotal();
        double expected = 60;
        assertEquals(expected, result);

    }

    @Test
    public void testGetDiscount(){
        i1.setDiscount(20);
        double result= i1.getDiscount();
        double expected = 20;
        assertEquals(expected, result);
    }
    
    @Test
    public void testGetUserID(){
        i1.setUserID(1);
        int result = i1.getUserID();
        int expected=1;
        assertEquals(expected, result);
    }
    
    @Test
    void emptyConstruct() {
        new Invoice();
    }

}
