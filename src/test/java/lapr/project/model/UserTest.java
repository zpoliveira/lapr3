package lapr.project.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    static User u = new User("Carlos",123456789, "1988-10-12",1.70, 69, 22334455, "carlosmoreira16@gmail.com", 6.25);

    @BeforeAll
    private static void setUp(){
        u.setUserPoints(15);
    }

    @Test
    void getUserId() {
        u.setUserId(1);
        assertEquals(1,u.getUserId());
    }

    @Test
    void getName() {
        assertEquals("Carlos",u.getName());
    }

    @Test
    void getNif() {
        assertEquals(123456789 ,u.getNif());
    }

    @Test
    void getBirthDate() {
        assertEquals("1988-10-12",u.getBirthDate());
    }

    @Test
    void getHeigth() {
        assertEquals(1.70, u.getHeigth());
    }

    @Test
    void getWeight() {
        assertEquals(69, u.getWeight());
    }

    @Test
    void getCreditCardNumber() {
        assertEquals(22334455, u.getCreditCardNumber());
    }

    @Test
    void getUserPoints() {
        assertEquals(15, u.getUserPoints());
    }

    @Test
    void getEmail() {
        assertEquals("carlosmoreira16@gmail.com", u.getEmail());
    }
    @Test
    void getAverageSpeed() {
        assertEquals(6.25, u.getUserAverageSpeed());
    }

    @Test
    void equals() {
        u.setUserId(1);
        User u2 = new User("Carlos",123456789, "1988-10-12",1.70, 69, 22334455, "carlosmoreira16@gmail.com");
        User u3 = u;
        u2.setUserId(1);
        assertEquals(u,u2);
        assertEquals(u,u3);
        assertNotEquals(u,null);
        assertNotEquals(u,new Object());
    }

    @Test
    void hashCodeTest() {
        int hash = u.hashCode();
        assertEquals(123457781,hash);
    }

    @Test
    void isValidTest(){
        u.setState(1);
        assertEquals(true, u.isValid());
    }

    @Test
    void isValidTestInvalid1(){
        u.setState(0);
        assertEquals(false, u.isValid());
    }

    @Test
    void isValidTestInvalid2(){
        User u4 = new User("test", 123456789,"1990-01-01", -0.9, 50, 1234123412341234L, "mail@mail.com");
        assertEquals(false, u4.isValid());
    }

    @Test
    void isValidTestInvalid3(){
        User u4 = new User("test", 123456789,"1990-01-01", 150, -0.99, 1234123412341234L, "mail@mail.com");
        assertEquals(false, u4.isValid());
    }

    @Test
    void isValidTestInvalid4(){
        User u4 = new User("test", 123456789,"1990-01-01", 150, 0, 1234123412341234L, "mail@mail.com");
        assertEquals(false, u4.isValid());
    }

    @Test
    void isValidTestInvalid5(){
        User u4 = new User("test", 123456789,"1990-01-01", 0, 26, 1234123412341234L, "mail@mail.com");
        assertEquals(false, u4.isValid());
    }

}