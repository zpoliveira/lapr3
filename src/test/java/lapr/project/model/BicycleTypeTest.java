/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author anacerqueira
 */
public class BicycleTypeTest {
    
    BicycleType bType = new BicycleType(1, "descricao", 0.99);

    /**
     * Test of getTypeId method, of class BicycleType.
     */
    @Test
    public void testGetTypeId() {
        int expResult = 1;
        int result = bType.getTypeId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getDescription method, of class BicycleType.
     */
    @Test
    public void testGetDescription() {
        
        String expResult = "descricao";
        String result = bType.getDescription();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setTypeId method and setDescription, of class BicycleType.
     */
    @Test
    public void testSetTypeId() {
       
        int typeId = 2;
        bType.setTypeId(2);
        assertEquals(typeId, bType.getTypeId());
        
        
    }
    
    /**
     * Test of setDescription method of class BicycleType.
     */
    @Test
    public void testSetDesciption() {
        
        String description = "descricao2";
        bType.setDescription(description);
        assertEquals(description, bType.getDescription());
    }
    
    @Test
    public void TesteEmptyConstructor(){
    new BicycleType();
    }

    @Test
    public void isValid(){
        assertEquals(true, bType.isValid());
    }

    @Test
    public void isValidNotValid(){
        bType.setWorkRatio(0);
        assertEquals(false, bType.isValid());
    }

}
