/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.sql.Date;
import lapr.project.model.Payment;
import lapr.project.utils.AuxMethods;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author anacerqueira
 */
public class PaymentTest {

    Date d = new Date(1000);
    Payment p = new Payment(1, 1, 1, d);
    Payment p1 = new Payment(1, 1, 1, d);
    Payment p4 = new Payment();

    /**
     * Test of getPaymentId method, of class Payment.
     */
    @Test
    public void testGetPaymentId() {
        p.setPaymentId(1);
        int expected = 1;
        int result = p.getPaymentId();
        assertEquals(expected, result);
    }

    /**
     * Test of getUserId method, of class Payment.
     */
    @Test
    public void testGetUserId() {
        int expected = 1;
        int result = p.getUserId();
        assertEquals(expected, result);
    }

    /**
     * Test of getServiceTypeId method, of class Payment.
     */
    @Test
    public void testGetServiceTypeId() {
        int expected = 1;
        int result = p.getServiceTypeId();
        assertEquals(expected, result);
    }

    /**
     * Test of getInvoiceId method, of class Payment.
     */
    @Test
    public void testGetInvoiceId() {
        int expected = 1;
        int result = p.getInvoiceId();
        assertEquals(expected, result);
    }

    /**
     * Test of getDate method, of class Payment.
     */
    @Test
    public void testGetDate() {
        Date expected = new Date(1000);
        Date result = p.getDate();
        assertEquals(expected, result);
    }

    /**
     * Test of getAmount method, of class Payment.
     */
    @Test
    public void testGetAmount() {
        p.setAmount(209);
        double expected = 209;
        double result = p.getAmount();
        assertEquals(expected, result);
    }

    /**
     * Test of setUserId method, of class Payment.
     */
    @Test
    public void testSetUserId() {
         p4.setUserId(2);
        assertEquals(2, p4.getUserId());
    }

    /**
     * Test of setServiceTypeId method, of class Payment.
     */
    @Test
    public void testSetServiceTypeId() {
         p4.setServiceTypeId(3);
        assertEquals(3, p4.getServiceTypeId());
    }

    /**
     * Test of setInvoiceId method, of class Payment.
     */
    @Test
    public void testSetInvoiceId() {
        p4.setInvoiceId(1);
        assertEquals(1, p4.getInvoiceId());
    }

    /**
     * Test of setDate method, of class Payment.
     */
    @Test
    public void testSetDate() {
        p4.setDate(d);
        assertEquals(d, p4.getDate());

    }

    /**
     * Test of hashCode method, of class Payment.
     */
    @Test
    public void testHashCode() {
        int hash = p.hashCode();
        assertEquals(69, hash);
    }

    /**
     * Test of equals method, of class Payment.
     */
    @Test
    public void testEquals() {
        Payment p3 = p;
        assertEquals(p, p1);
        assertEquals(p, p3);
        assertNotEquals(p, null);
        assertNotEquals(p, new Object());
    }

}
