# README #

This is the repository template used for student repositories in LAPR Projets.

#Java source files

Java source and test files are located in folder src.

# Maven files #

Pom.xml file controls the project build.
## Observations
In this file, DO NOT EDIT the following elements:

* groupID
* artifactID
* version
* properties

Also, students can only add dependencies to the specified section on this file.

# Eclipse files #

The following files are solely used by Eclipse IDE:

* .classpath
* .project

# IntelliJ Idea IDE files #

The following folder is solely used by Intellij Idea IDE :

* .idea

## How was the .gitignore file generated? ##
.gitignore file was generated based on https://www.gitignore.io/ with the following keywords:
  - Java
  - Maven
  - Eclipse
  - NetBeans
  - Intellij

## Who do I talk to? ##
In case you have any problem, please email Nuno Bettencourt (nmb@isep.ipp.pt).

## How do I use Maven? ##

### How to run unit tests? ###
Execute the "test" goals.
`$ mvn test`

### How to generate the javadoc for source code? ###
Execute the "javadoc:javadoc" goal.

`$ mvn javadoc:javadoc`

This generates the source code javadoc in folder "target/site/apidocs/index.html".

### How to generate the javadoc for test cases code? ###
Execute the "javadoc:test-javadoc" goal.

`$ mvn javadoc:test-javadoc`

This generates the test cases javadoc in folder "target/site/testapidocs/index.html".

### How to generate Jacoco's Code Coverage Report? ###
Execute the "jacoco:report" goal.

`$ mvn test jacoco:report`

This generates a jacoco code coverage report in folder "target/site/jacoco/index.html".

### How to generate PIT Mutation Code Coverage? ###
Execute the "org.pitest:pitest-maven:mutationCoverage" goal.

`$ mvn test org.pitest:pitest-maven:mutationCoverage`

This generates a PIT Mutation coverage report in folder "target/pit-reports/YYYYMMDDHHMI".

### How to combine different maven goals in one step? ###
You can combine different maven goals in the same command. For example, to locally run your project just like on jenkins, use:

`$ mvn clean test jacoco:report org.pitest:pitest-maven:mutationCoverage`

# Oracle repository

If you get the following error:

```
[ERROR] Failed to execute goal on project
bike-sharing: Could not resolve dependencies for project
lapr3:bike-sharing:jar:1.0-SNAPSHOT:
Failed to collect dependencies at
com.oracle.jdbc:ojdbc7:jar:12.1.0.2:
Failed to read artifact descriptor for
com.oracle.jdbc:ojdbc7:jar:12.1.0.2:
Could not transfer artifact
com.oracle.jdbc:ojdbc7:pom:12.1.0.2
from/to maven.oracle.com (https://maven.oracle.com):
Not authorized , ReasonPhrase:Authorization Required.
-> [Help 1]
```

Follow these steps:

https://blogs.oracle.com/dev2dev/get-oracle-jdbc-drivers-and-ucp-from-oracle-maven-repository-without-ides

You do not need to set a proxy.





# Report #

## Abstract ##

Nowadays, it became more and more common for people to travel by bicycle inside big cities either in tourist travels or to travel between two places. For this reason, it is important to be able to manage them. 

The purpose of this work was the development of a software solution that allows the management of bicycle parks, bicycle and users. This application is responsible for, among other things, bicycle renting, add, remove or update parks and bicycles characteristics, rout estimations and so on and so forth. 

The work developed reflected on this paper, aims to be in compliance with the best software engineering practices in order to give a sharp response to the given problem.


Keywords: Bicycle rental Management, Bicycle Parks, Database, Java Software 


## Introduction ##

### Project context ###

With increasing knowledge of air pollution and in order to improve citizens quality of life, there are some cities, like Oslo, Madrid, Paris, Chengdu, and others [1] the reduce or forbidden the use of cars in city centers. Besides this, tourists want more and more to travel by bicycle in city centers. Due to this bicycle has become one of the main vehicles in this cities and bike renting one business opportunity.

This report and the respective developed software solution were a result of a 4 weeks project developed in LAPR3 classes. The goal of this project was to give students the opportunity of developing a solution that was based mainly on the curricular units of 2nd year 1st semester BDDAD, FISIAP and ESINF. The main objective of the developed software is helping in bike renting management: parks management, bicycles management, bicycle availability in parks, and so on and so forth.

This report has three more chapters:

	In chapter 2 – Problem Statement – is presented the problem that the team had to solve
    
	In chapter 3 – Solution – is presented the adopted solution to the problem presented
    
	In chapter 4 – Conclusion – is presented an overview of the project as well as a reflection of the work done and possible improvements.
    

### State of art ###

The management of bicycle parks faces several challenges, mainly due to the geographical line up of parks. Keep an actual database with real time information about bicycle requisitions and availability in parks is a major concern, so a software solution in essential. Bicycle renting parks management is and activity that, among other, includes the creation, update and elimination of parks and bicycles as well as users/user information. Besides this, it also includes the parking spots management and requisition management activities.
Nowadays, there are several solutions available in the market. A bief description of two examples are presented in table 1.

| Software solution |Description |Main Clients|
|:-------|:------|:--------|
| Booqable [2,3]| This is an integrated web solution that allows users to make online reservations, ask for budgets or make online payments. It integrates with any website and ca be fully customized. |Sennheiser® Vintage Suit Hire® Leica® NSCAD University |
|EZ Rent Out[2,4]|This solution allows quickly creation and orders update, ensuring conflict-free booking, provide discounts, design customized invoices or set up monthly billing. Besides this provides real time equipment tracking and quick asset on stock level or stock optimization. Beside others features it also provides a track on equipment maintenance, report and report analysis, and others.| Universidad de La Sabana Affairs IT® Rockharbor®|


## Problem Statement ##

A bicycle sharing company needs a software solution that allows the management of its bicycles, bicycle parks and users.  
This company has several bicycle parks all over town, that are used for bicycle parking. Each park has different spots for the different bicycles that the company own – mountain bicycles, road bicycle and electrical assisted bicycles. Mountain and road bicycles can be parked at the same spots however, electrical assisted bicycles should be placed in appropriate spots. For this reason, it must be possible to control park actual capacity and parks total capacity for mountains/road bicycles and for electrical assisted bicycles. In other hand, the system must be capable of show which are the available bicycles at a given park.
In order to have access to the system, users must have a register which has a cost. When requesting a bicycle, depending on the time of the requested, a fee could be applied. When returning a bicycle, users should be notified by e-mail of the proper devolution at the dropping off park.
There is also a system for crediting point based on certain user actions which can be used to partially pay for monthly costs. These costs are issued to users every months and, after paying the invoices, userd should get a receipt.

## Solution ##

In order to respond to all client's requests, it was developed a software to manage bicycle parks, bicycles, bicycle requests and users.

The team decided to use the Scrum methodology associated with Jira Issues to be able to track the team workflow and every task was assign according to technical and personal skills. During the development of the application, the team also adopted the iterative and incremental model, which consists in regular, and functional and evolutional deliveries.

SonarQube and Jenkins were the two tools used to ensure the code quality and validation of the project build and bitbucket was the chosen Git repository.

### Technical solution ###

The implemented solution covers all use cases proposed by the client. In this section it is possible to consult the analysis and design used to develop the US: for each case is presented a brief description and the respective analysis and design diagrams.

The approached business model used as a base to the application is presented in the image below as well as the relational model diagram.  



#### Domain Model ####

![Domain Model](Diagrams/DomainModel.jpg)

#### Relational Model ####

![Relational Model](Diagrams/ORM_LAPR3G001.jpg)

#### LAPR3G001-4 - Add new Bicycle ####

This User Story was implemented using a public and a private method and using the BicycleDB class of Data package so that the administrator could insert a Bicycle in the database. This method returns the Bicycle that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-4](Diagrams/UCD_LAPR3G001-4.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-4](Diagrams/SSD_LAPR3G001-4.jpg)

##### Class Diagram #####

![CD_LAPR3G001-4](Diagrams/CD_LAPR3G001-4.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-4](Diagrams/SD_LAPR3G001-4.jpg)


#### LAPR3G001-5 - Update Bicycle ####

In this US, the administrator can update any field (or all) of a given bicycle.
The bicycle to be updated is identified by the administrator with its ID. In the development of this US, the administrator inserts the identifier of the bicycle that will be updated and the system search for it in the database. In case this identifier is not a valid one an Oracle exception is raised and no field is updated. In case it is a valid identifier, the system updates the bicycle attributes.


##### Use Case Diagram #####

![UCD_LAPR3G001-5](Diagrams/UCD_LAPR3G001-5.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-5](Diagrams/SSD_LAPR3G001-5.jpg)

##### Class Diagram #####

![CD_LAPR3G001-5](Diagrams/CD_LAPR3G001-5.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-5](Diagrams/SD_LAPR3G001-5.jpg)


#### LAPR3G001-6 - Remove Bicycle ####

This User Story was implemented using a public and a private method and using the BicycleDB class of Data package so that the administrator could remove a Bicycle in the database. This method returns a boolean that confirms the remove success.

##### Use Case Diagram #####

![UCD_LAPR3G001-6](Diagrams/UCD_LAPR3G001-6.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-6](Diagrams/SSD_LAPR3G001-6.jpg)

##### Class Diagram #####

![CD_LAPR3G001-6](Diagrams/CD_LAPR3G001-6.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-6](Diagrams/SD_LAPR3G001-6.jpg)


#### LAPR3G001-7 - Add new park ####

This User Story was implemented using a public and a private method and using the ParkDB class of Data package so that the administrator could insert a park in the database. This method returns the park that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-7](Diagrams/UCD_LAPR3G001-7.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-7](Diagrams/SSD_LAPR3G001-7.jpg)

##### Class Diagram #####

![CD_LAPR3G001-7](Diagrams/CD_LAPR3G001-7.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-7](Diagrams/SD_LAPR3G001-7.jpg)


#### LAPR3G001-8 - Update park ####

This User Story was implemented using a public and a private method and using the ParkDB class of Data package so that the administrator could update a Park in the database. This method returns a boolean that confirms the update success.

##### Use Case Diagram #####

![UCD_LAPR3G001-8](Diagrams/UCD_LAPR3G001-8.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-8](Diagrams/SSD_LAPR3G001-8.jpg)

##### Class Diagram #####

![CD_LAPR3G001-8](Diagrams/CD_LAPR3G001-8.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-8](Diagrams/SD_LAPR3G001-8.jpg)


#### LAPR3G001-9 - Remove park ####

This User Story was implemented using a public and a private method and using the ParkDB class of Data package so that the administrator could remove a Park in the database. This method returns a boolean that confirms the remove success.

##### Use Case Diagram #####

![UCD_LAPR3G001-9](Diagrams/UCD_LAPR3G001-9.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-9](Diagrams/SSD_LAPR3G001-9.jpg)

##### Class Diagram #####

![CD_LAPR3G001-9](Diagrams/CD_LAPR3G001-9.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-9](Diagrams/SD_LAPR3G001-9.jpg)


#### LAPR3G001-10 - Add user ####

This User Story was implemented using a public  method and using the UserDB class of Data package so that the administrator could insert a User in the database. This method returns the User that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-10](Diagrams/UCD_LAPR3G001-10.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-10](Diagrams/SSD_LAPR3G001-10.jpg)

##### Class Diagram #####

![CD_LAPR3G001-10](Diagrams/CD_LAPR3G001-10.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-10](Diagrams/SD_LAPR3G001-10.jpg)


#### LAPR3G001-11 - Make Payment ####

In this US user can insert in the system a payment relative to a service of his own (bicycle rental or the initial fee). In this process the system asks user for the payment detail in which is specified the service. The system inserts this payment on the database and, if the service if initial fee type, an trigger updates the user state to an active user.

##### Use Case Diagram #####

![UCD_LAPR3G001-11](Diagrams/UCD_LAPR3G001-11.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-11](Diagrams/SSD_LAPR3G001-11.jpg)

##### Class Diagram #####

![CD_LAPR3G001-11](Diagrams/CD_LAPR3G001-11.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-11](Diagrams/SD_LAPR3G001-11.jpg)


#### LAPR3G001-13 - Given users location return list of nearest bicycle parks ####

In this User Story the User's coordinates and altitude are supplied and the distance to all parks is calculated. The result is returned as a sorted map with the Park as key and the distance and value.

##### Use Case Diagram #####

![UCD_LAPR3G001-13](Diagrams/UCD_LAPR3G001-13.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-13](Diagrams/SSD_LAPR3G001-13.jpg)

##### Class Diagram #####

![CD_LAPR3G001-13](Diagrams/CD_LAPR3G001-13.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-13](Diagrams/SD_LAPR3G001-13.jpg)


#### LAPR3G001-14 - Check for available bicycles at a given park ####

In this User Story the User's check for all  bicycles at a given park. The result is returned as a set with the Available Bicycles at park ID.

##### Use Case Diagram #####

![UCD_LAPR3G001-14](Diagrams/UCD_LAPR3G001-14.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-14](Diagrams/SSD_LAPR3G001-14.jpg)

##### Class Diagram #####

![CD_LAPR3G001-14](Diagrams/CD_LAPR3G001-14.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-14](Diagrams/SD_LAPR3G001-14.jpg)


#### LAPR3G001-15 - Check how far is another park ####

In this User Story the User's check how far one park is for another park.  The result is returned the distance in meters (double).

##### Use Case Diagram #####

![UCD_LAPR3G001-15](Diagrams/UCD_LAPR3G001-15.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-15](Diagrams/SSD_LAPR3G001-15.jpg)

##### Class Diagram #####

![CD_LAPR3G001-15](Diagrams/CD_LAPR3G001-15.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-15](Diagrams/SD_LAPR3G001-15.jpg)


#### LAPR3G001-16 - Request electrical bicycle without destination park  ####

This User Story was developed so that an User can request a electrical bicycle, creating a requisition where only can insert pickUp park can be inserted, and inserting it in the database.

##### Use Case Diagram #####

![UCD_LAPR3G001-16](Diagrams/UCD_LAPR3G001-16.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-16](Diagrams/SSD_LAPR3G001-16.jpg)

##### Class Diagram #####

![CD_LAPR3G001-16](Diagrams/CD_LAPR3G001-16.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-16](Diagrams/SD_LAPR3G001-16.jpg)


#### LAPR3G001-18 - Request non-electrical bicycle ####

This User Story was developed so that an User can request a non-electrical bicycle, creating a requisition where pickUp and dropOff park can be inserted, and inserting it in the database.

##### Use Case Diagram #####

![UCD_LAPR3G001-18](Diagrams/UCD_LAPR3G001-18.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-18](Diagrams/SSD_LAPR3G001-18.jpg)

##### Class Diagram #####

![CD_LAPR3G001-18](Diagrams/CD_LAPR3G001-18.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-18](Diagrams/SD_LAPR3G001-18.jpg)


#### LAPR3G001-19 - Check if a destination park as any free parkinhg places ####

!Here we should write a brief description!

##### Use Case Diagram #####

![UCD_LAPR3G001-19](Diagrams/UCD_LAPR3G001-19.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-19](Diagrams/SSD_LAPR3G001-19.jpg)

##### Class Diagram #####

![CD_LAPR3G001-19](Diagrams/CD_LAPR3G001-19.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-19](Diagrams/SD_LAPR3G001-19.jpg)


#### LAPR3G001-21 - Return a projection for the amount of calories burnt between two bicycle parks ####

In this US an estimate of the projection of clories needed for a user to go from a pickUp park to a dropOff park is provided. The calculations is made using the following data: User weight, bicycle weight, bicycle work ratio, wind velocity and direction.
The work ration consists on a value between 0 and 0.99 that represents the percentage of the final energy value needed to travel from pickUp park to a dropOff park i.e. this value represents the percentage of work made by the bicycle.
The method return the amount of calories.

##### Use Case Diagram #####

![UCD_LAPR3G001-21](Diagrams/UCD_LAPR3G001-21.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-21](Diagrams/SSD_LAPR3G001-21.jpg)

##### Class Diagram #####

![CD_LAPR3G001-21](Diagrams/CD_LAPR3G001-21.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-21](Diagrams/SD_LAPR3G001-21.jpg)


#### LAPR3G001-20 - Return a bicycle ####

!Here we should write a brief description!

##### Use Case Diagram #####

![UCD_LAPR3G001-20](Diagrams/UCD_LAPR3G001-20.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-20](Diagrams/SSD_LAPR3G001-20.jpg)

##### Class Diagram #####

![CD_LAPR3G001-20](Diagrams/CD_LAPR3G001-20.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-20](Diagrams/SD_LAPR3G001-20.jpg)

### LAPR3G001-82 - Request electrical bicycle with given destination park ###

In this US users can request an electrical bicycle at a given park indicating which is the destination park. The system returns a set of a bicycle that have enough energy to do this travel and ten percent more. The calculation of the necessary energy to do this travel is based on the worst-case scenario for the heaviest bicycle and for the case that all the effort to do the travel came from the electrical bicycle.

##### Use Case Diagram #####

![UCD_LAPR3G001-82](Diagrams/UCD_LAPR3G001-82.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-82](Diagrams/SSD_LAPR3G001-82.jpg)

##### Class Diagram #####

![CD_LAPR3G001-82](Diagrams/CD_LAPR3G001-82.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-82](Diagrams/SD_LAPR3G001-82.jpg)


#### LAPR3G001-92 - Turistic POI loading ####

This User Story was implemented using a public  method and using the PoiDB class of Data package so that the administrator could insert a Poi in the database. This method returns the Poi that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-92](Diagrams/UCD_LAPR3G001-92.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-92](Diagrams/SSD_LAPR3G001-92.jpg)

##### Class Diagram #####

![CD_LAPR3G001-92](Diagrams/CD_LAPR3G001-92.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-92](Diagrams/SD_LAPR3G001-92.jpg)



#### LAPR3G001-93 - Shortest route between two parks ####
#### LAPR3G001-94 - Most energetically efficient route between two parks ####
#### LAPR3G001-95 - Shortest route with intermediate POI ####
#### LAPR3G001-135 - Suggestion of several routes ####
#### LAPR3G001-136 - Route sorting ####

All the route suggestion User Stories where condensed in this written report section, as all of them shared similarities. The User is the actor that requests several types of route suggestions, that can be constructed considering distance between locations or the energy needed to travel between them. In both this cases intermediate POIs can be added and, in this case, a specific number of routes can be displayed. Ascending or descending sorting of routes can alse be selected.

**Important note: It was our group undertanding that, according to the project assignment text, only when the user inserted intermediate POIs, more than one route could be selected (and, evidently, when more than one sorted). This question was discussed with the product owner in our last meeting, and we were informed that our understanding was w plausible one and, therefore, our soluction would be accepted without penalties. For this reason the Facade's suggestRoutesBetweenTwoLocations, that has no intermediate POIs, could not be answered.**   

##### Use Case Diagram #####

![UCD_LAPR3G001-93_94_95_135_136](Diagrams/UCD_LAPR3G001-93_94_95_135_136.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-93_94_95_135_136](Diagrams/SSD_LAPR3G001-93_94_95_135_136.jpg)

##### Class Diagram #####

![CD_LAPR3G001-93_94_95_135_136](Diagrams/CD_LAPR3G001-93_94_95_135_136.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-93_94_95_135_136](Diagrams/SD_LAPR3G001-93_94_95_135_136.jpg)


#### LAPR3G001-96 - Request charging report ####

!Here we should write a brief description!

##### Use Case Diagram #####

![UCD_LAPR3G001-96](Diagrams/UCD_LAPR3G001-96.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-96](Diagrams/SSD_LAPR3G001-96.jpg)

##### Class Diagram #####

![CD_LAPR3G001-96](Diagrams/CD_LAPR3G001-96.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-96](Diagrams/SD_LAPR3G001-96.jpg)


#### LAPR3G001-97 - Inserting Wind information between two locations ####

This User Story was implemented using a public  method and using the WindDB class of Data package so that the administrator could insert a the wind information between two locations in the database. This method returns the Wind that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-97](Diagrams/UCD_LAPR3G001-97.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-97](Diagrams/SSD_LAPR3G001-97.jpg)

##### Class Diagram #####

![CD_LAPR3G001-97](Diagrams/CD_LAPR3G001-97.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-97](Diagrams/SD_LAPR3G001-97.jpg)


#### LAPR3G001-137 - Invoice issuing ####

This User Story was implemented using a public  method and using the InvoiceDB class of Data package so that the administrator could insert a Invoice in the database. This method returns the Invoice that was inserted.

##### Use Case Diagram #####

![UCD_LAPR3G001-137](Diagrams/UCD_LAPR3G001-137.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-137](Diagrams/SSD_LAPR3G001-137.jpg)

##### Class Diagram #####

![CD_LAPR3G001-137](Diagrams/CD_LAPR3G001-137.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-137](Diagrams/SD_LAPR3G001-137.jpg)



#### LAPR3G001-138 - Importing files ####

!Here we should write a brief description!

##### Use Case Diagram #####

![UCD_LAPR3G001-138](Diagrams/UCD_LAPR3G001-138.jpg)

##### System Sequence Diagram #####

![SSD_LAPR3G001-138](Diagrams/SSD_LAPR3G001-138.jpg)

##### Class Diagram #####

![CD_LAPR3G001-138](Diagrams/CD_LAPR3G001-138.jpg)

##### Sequence Diagram ####

![SD_LAPR3G001-138](Diagrams/SD_LAPR3G001-138.jpg)


## Conlusion ##

Overall the team managed to implement all the features that were proposed by the client in the project within the deadline and accomplished all the technical parameters in SonarQube.

Although the team considers that a good job has been done, the team dealed with time issues because some features were proposed very close to the deadline and had a major impact in the project.

In sum, this project helped to improve the team's programming, design and soft skills as well  as dabatase skills.

## References ##

[1] http://yogui.co/7-cidades-sem-carro/

[2] https://www.capterra.com/sem-compare/rental-software?gclid=EAIaIQobChMI-Yaf4_DC3wIV7b3tCh3kcg9aEAAYAiAAEgJG3_D_BwE&gclsrc=aw.ds

[3] https://booqable.com/?source=capterra

[4] https://www.ezrentout.com/?r=capterra&gartner=capterra
