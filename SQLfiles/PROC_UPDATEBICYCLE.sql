CREATE or REPLACE PROCEDURE PROC_UPDATEBICYCLE(iD_bicycle Bicycle.BICYCLEID%Type, 
                                                bic_type Bicycle.TYPEID%Type, 
                                                park_id bicycle.parkID%Type, 
                                                batery_level bicycle.BATTERYLEVEL%Type, 
                                                full_capacity_level bicycle.FULLBATTERYCAPACITY%Type, 
                                                new_weight bicycle.weight%Type
                                                )

IS
    bicycle_count               number;
    park_count                  number;
    bikeType_count              number;
    bicycle_exception           exception;
    park_exception              exception;
    bikeType_exception          exception;

BEGIN
    Select count(*) into bicycle_count 
    from bicycle 
    where bicycleid = iD_bicycle;
    
    if (bicycle_count = 0 or id_bicycle = null) then
        raise bicycle_exception; 
    end if;

    Select count(*) into park_count 
    from Parks 
    where parkid = park_id;
    
    if (park_count = 0 or park_id = null) then 
        raise park_exception; 
    end if;
    
    Select count(*) into bikeType_count 
    from BicycleType 
    where typeId = bic_type;
    
    if (bikeType_count = 0 or park_id = null) then 
        raise bikeType_exception; 
    end if;
    
    Update bicycle set typeid = bic_type, 
                        parkid=park_id, 
                        BATTERYLEVEL = batery_level,
                        FULLBATTERYCAPACITY= full_capacity_level, 
                        weight = new_weight
    where bicycleid = id_bicycle;

EXCEPTION 

    when bicycle_exception then RAISE_APPLICATION_ERROR (-20002, 'Bicycle Id does not exists.') ;
    when park_exception then RAISE_APPLICATION_ERROR (-20003, 'Park Id does not exists.') ;
    when bikeType_exception then RAISE_APPLICATION_ERROR (-20004, 'Bicycle type Id does not exists.') ;

END;