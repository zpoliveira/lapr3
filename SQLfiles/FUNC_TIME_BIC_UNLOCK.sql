create or replace FUNCTION FUNC_TIME_BIC_UNLOCK (p_bId BICYCLE.bicycleid%Type)
RETURN number
IS

    v_time number(10,2);
    v_maxDate   date;

BEGIN
        select max(initialdate)
        into v_maxDate
        from requisition
        where requisition.bicycleid=p_bId;

        select case when finaldate is null 
               then SYSDATE-initialdate
               else finaldate-initialdate end 
        into v_time
        from requisition
        where requisition.bicycleid=p_bId and requisition.initialdate=v_maxDate;
        
        return v_time;

END FUNC_TIME_BIC_UNLOCK;