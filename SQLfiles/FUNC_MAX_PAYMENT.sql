create or replace function FUNC_MAX_PAYMENT

return number

is

v_max number;

begin


select max(paymentId) into v_max from Payment;

return v_max;

end;