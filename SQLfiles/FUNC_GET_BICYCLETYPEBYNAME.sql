create or replace FUNCTION FUNC_GET_BICYCLETYPEBYNAME
(f_bikeType BICYCLETYPE.DESCRIPTION%TYPE) RETURN NUMBER
AS 

V_TYPEID                NUMBER;
V_COUNT                 NUMBER;
V_TYPE                  BICYCLETYPE.DESCRIPTION%TYPE;
BIKE_EXCEPTION          EXCEPTION;  

BEGIN

    IF (f_bikeType = 'mtb' OR f_bikeType = 'road') THEN
        V_TYPE := 'other';
    ELSE 
        IF (f_bikeType != 'eletric') THEN
            RAISE BIKE_EXCEPTION;
        ELSE
            V_TYPE := 'eletric';
        END IF;
    END IF;

--    SELECT COUNT(TYPEID) INTO V_COUNT FROM BICYCLETYPE WHERE DESCRIPTION = f_bikeType;
--        IF(V_COUNT <= 0 OR f_bikeType = NULL) THEN
--            RAISE BIKE_EXCEPTION;
--        END IF;
        
    SELECT TYPEID INTO V_TYPEID FROM BICYCLETYPE WHERE DESCRIPTION = V_TYPE;
      
    RETURN V_TYPEID;
      
    EXCEPTION
        WHEN BIKE_EXCEPTION THEN RAISE_APPLICATION_ERROR(-20015, 'Bicycle type does not exist');

END FUNC_GET_BICYCLETYPEBYNAME;