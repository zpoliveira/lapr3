create or replace FUNCTION FUNC_GETUSERPOINTS(p_userid number)

return number

is

v_points number;

begin


select userpoints into v_points from Users
where Users.userid = p_userid;

return v_points;

end;