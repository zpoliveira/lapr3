CREATE OR REPLACE FUNCTION FUNC_GET_LOCATIONID_BY_COORDINATES
                        (p_latitude location.latitude%Type,
                            p_longitude location.longitude%type) RETURN NUMBER
AS 

V_LOCATIONID                NUMBER;
V_COUNT                     NUMBER;
LOCATION_EXCEPTION          EXCEPTION;  

BEGIN

    SELECT COUNT(LOCATIONID) INTO V_COUNT FROM LOCATION WHERE LATITUDE = p_latitude AND LONGITUDE = p_longitude;
        IF(V_COUNT <= 0 OR p_latitude = NULL OR p_longitude = NULL) THEN
            RAISE LOCATION_EXCEPTION;
        END IF;
        
    SELECT LOCATIONID INTO V_LOCATIONID FROM LOCATION WHERE LATITUDE = p_latitude AND LONGITUDE = p_longitude;
      
    RETURN V_LOCATIONID;
      
    EXCEPTION
        WHEN LOCATION_EXCEPTION THEN RAISE_APPLICATION_ERROR(-20099, 'Location does not exist');

END FUNC_GET_LOCATIONID_BY_COORDINATES;