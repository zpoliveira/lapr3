create or replace FUNCTION FUNC_AVALIABLE_SLOTS(p_username USERS.name%TYPE)
return number
is

v_bic_type number;
v_bic_id number;
v_userid USERS.userid%TYPE;

BEGIN

    select userid into v_userid 
    from users 
    where upper(name) like upper(p_username);
    
    select bicycleid into v_bic_id
    from requisition
    where userid=v_userid;
    
    select typeid into v_bic_type
    from bicycle
    where bicycleid = v_bic_id;
    
return v_bic_type;

END FUNC_AVALIABLE_SLOTS;