--------------------------------------------------------
-- LAPR3-G001 DATABASE SCRIPT 05-01-2019
--------------------------------------------------------

DROP TABLE "LAPR3_G1"."BICYCLE" cascade constraints;
DROP TABLE "LAPR3_G1"."BICYCLETYPE" cascade constraints;
DROP TABLE "LAPR3_G1"."LOCATION" cascade constraints;
DROP TABLE "LAPR3_G1"."LOCATIONTYPE" cascade constraints;
DROP TABLE "LAPR3_G1"."PARKS" cascade constraints;
DROP TABLE "LAPR3_G1"."PAYMENT" cascade constraints;
DROP TABLE "LAPR3_G1"."REQUISITION" cascade constraints;
DROP TABLE "LAPR3_G1"."SERVICE" cascade constraints;
DROP TABLE "LAPR3_G1"."USERS" cascade constraints;
DROP TABLE "LAPR3_G1"."LOTATIONBYTYPE" cascade constraints;
DROP TABLE "LAPR3_G1"."POI" cascade constraints;
DROP TABLE "LAPR3_G1"."WIND" cascade constraints;
DROP TABLE "LAPR3_G1"."INVOICE" cascade constraints;

--------------------------------------------------------
--  DDL for Table BICYCLE
--------------------------------------------------------
  CREATE TABLE "LAPR3_G1"."BICYCLE" 
   (	"BICYCLEID" NUMBER(*,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"TYPEID" NUMBER(*,0), 
	"PARKID" NUMBER(*,0), 
	"BATTERYLEVEL" NUMBER(*,0), 
	"FULLBATTERYCAPACITY" NUMBER(*,0), 
	"WEIGHT" NUMBER(*,0), 
	"AVAILABLE" NUMBER(*,0) DEFAULT 0,
  "DESCRIPTION" VARCHAR2(50 BYTE), 
  "AERODYNAMIC_COEFFICIENT" NUMBER(5,2) DEFAULT 0
   ) ;
   
--------------------------------------------------------
--  DDL for Table BICYCLETYPE
--------------------------------------------------------
  CREATE TABLE "LAPR3_G1"."BICYCLETYPE" 
   (	"TYPEID" NUMBER(*,0),
  "DESCRIPTION" VARCHAR2(50 BYTE), 
	"WORKRATIO" NUMBER(2,2)
   ) ;
   
--------------------------------------------------------
--  DDL for Table LOCATIONTYPE
--------------------------------------------------------
CREATE TABLE LocationType (
  typeid      number(10)  GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
  description VARCHAR2(50 BYTE), 
  PRIMARY KEY (typeid));

--------------------------------------------------------
--  DDL for Table LOCATION
--------------------------------------------------------
CREATE TABLE Location (
  locationId         number(10)  GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
  typeId number(10) NOT NULL, 
  latitude           number(9, 6) NOT NULL, 
  longitude          number(9, 6) NOT NULL, 
  altitude           number(6, 2) NOT NULL, 
  PRIMARY KEY (locationId));
  
--------------------------------------------------------
--  DDL for Table PARKS
--------------------------------------------------------

  CREATE TABLE "LAPR3_G1"."PARKS" 
   (	"PARKID" NUMBER(*,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	  locationId number(10) NOT NULL, 
	  "NAME" VARCHAR2(30 BYTE),
    "INPUTVOLTAGE" NUMBER (8, 2),
    "INPUTCURRENT" NUMBER (8, 2) 
   ) ;
   
--------------------------------------------------------
--  DDL for Table POI
--------------------------------------------------------
  CREATE TABLE "LAPR3_G1"."POI" 
(	poiId      number(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	  locationId number(10) NOT NULL, 
	  name       varchar2(30) NOT NULL UNIQUE, 
  PRIMARY KEY (poiId));
  
--------------------------------------------------------
--  DDL for Table PAYMENT
--------------------------------------------------------

 CREATE TABLE "LAPR3_G1"."PAYMENT"
  (    "PAYMENTID" NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE ,
    "USERID" NUMBER(10),
    "SERVICETYPE" NUMBER(10),
    "INVOICEID" NUMBER(10),
    "PAYMENTDATE" DATE,
    "UNITARYPRICE"    NUMBER(8,2),
    "QUANTITY"    NUMBER(8,1),
    "CONDITION"    VARCHAR2(10),
    "AMOUNT" NUMBER(8,2)
  ) ;

  --------------------------------------------------------
  --  DDL for Table INVOICE
  --------------------------------------------------------
  CREATE TABLE "LAPR3_G1"."INVOICE" (
    "INVOICEID" NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE,
    "TOTAL"        NUMBER(8, 2) NOT NULL);

--------------------------------------------------------
--  DDL for Table REQUISITION
--------------------------------------------------------

  CREATE TABLE "LAPR3_G1"."REQUISITION" 
   (	"REQUISITIONID" NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"USERID" NUMBER(10), 
	"BICYCLEID" NUMBER(10), 
	"PICKUPPARK" NUMBER(10), 
	"DROPPARK" NUMBER(10), 
	"INITIALDATE" DATE, 
	"FINALDATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table SERVICE
--------------------------------------------------------

  CREATE TABLE "LAPR3_G1"."SERVICE" 
   (	"SERVICETYPEID" NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"DESCRIPTION" VARCHAR2(50 BYTE), 
	"UNITARYPRICE" NUMBER(8,2)
   ) ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "LAPR3_G1"."USERS" 
   (	"USERID" NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"NAME" VARCHAR2(100 BYTE), 
	"NIF" NUMBER(9), 
	"BIRTHDATE" DATE, 
	"USERPOINTS" NUMBER(10) DEFAULT 0, 
	"HEIGHT" FLOAT(126), 
	"WEIGTH" FLOAT(126), 
	"CREDITCARDNUMBER" NUMBER(20), 
	"EMAIL" VARCHAR2(50 BYTE), 
	"STATE" NUMBER(10) DEFAULT 0,
  "AVERAGE_SPEED" NUMBER(10,2)
   ) ;

--------------------------------------------------------
--  DDL for Table WIND
--------------------------------------------------------
CREATE TABLE Wind (
  initiallocationId number(10) NOT NULL, 
  finallocationId   number(10) NOT NULL, 
  windspeed         number(8,2) DEFAULT 0, 
  windFactor        number(8,2) DEFAULT 0,
  AERODYNAMIC_COEFFICIENT NUMBER(2,3) DEFAULT 0,
  PATH_DIRECTION    varchar2(10),
  PRIMARY KEY (initiallocationId, finallocationId));


--------------------------------------------------------
--  DDL for Table LOTATIONBYTYPE
--------------------------------------------------------
CREATE TABLE LotationByType (
  parkId            number(10) NOT NULL, 
  typeId            number(10) NOT NULL, 
  lotation          number(10), 
  maxLotation       number(10));

--------------------------------------------------------
--  Constraints for Table INVOICE
--------------------------------------------------------
  ALTER TABLE "LAPR3_G1"."INVOICE" ADD PRIMARY KEY ("INVOICEID");

--------------------------------------------------------
--  Constraints for Table PAYMENT
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."PAYMENT" MODIFY ("USERID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PAYMENT" MODIFY ("SERVICETYPE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PAYMENT" MODIFY ("INVOICEID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PAYMENT" MODIFY ("PAYMENTDATE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PAYMENT" MODIFY ("AMOUNT" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PAYMENT" ADD CHECK (amount>0) ENABLE;
  ALTER TABLE "LAPR3_G1"."PAYMENT" ADD PRIMARY KEY ("PAYMENTID");
  ALTER TABLE "LAPR3_G1"."PAYMENT" ADD CHECK(CONDITION IN('PAID','PENDING'));
  ALTER TABLE "LAPR3_G1"."PAYMENT" ADD CONSTRAINT FKPayment_invoiceId FOREIGN KEY ("INVOICEID") REFERENCES "LAPR3_G1"."INVOICE" ("INVOICEID");

--------------------------------------------------------
--  Constraints for Table PARKS
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."PARKS" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."PARKS" ADD PRIMARY KEY ("PARKID")
  USING INDEX  ENABLE;
  ALTER TABLE "LAPR3_G1"."PARKS" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table BICYCLETYPE
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."BICYCLETYPE" MODIFY ("TYPEID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLETYPE" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLETYPE" MODIFY ("WORKRATIO" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLETYPE" ADD CHECK (WORKRATIO>0 AND WORKRATIO<1) ENABLE;
  ALTER TABLE "LAPR3_G1"."BICYCLETYPE" ADD PRIMARY KEY ("TYPEID")
  USING INDEX  ENABLE;

--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."USERS" ADD CONSTRAINT "EMAIL" CHECK (regexp_like(email, '[[:alnum:]]+@[[:alnum:]]+\.[[:alnum:]]')) ENABLE;
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("NIF" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("BIRTHDATE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("HEIGHT" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("WEIGTH" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("CREDITCARDNUMBER" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" MODIFY ("STATE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."USERS" ADD CHECK (userPoints>=0) ENABLE;
  ALTER TABLE "LAPR3_G1"."USERS" ADD CHECK (height>0) ENABLE;
  ALTER TABLE "LAPR3_G1"."USERS" ADD CHECK (weigth>0) ENABLE;
  ALTER TABLE "LAPR3_G1"."USERS" ADD CHECK (state>=0) ENABLE;
  ALTER TABLE "LAPR3_G1"."USERS" ADD PRIMARY KEY ("USERID");
  ALTER TABLE "LAPR3_G1"."USERS" ADD UNIQUE ("CREDITCARDNUMBER");
  ALTER TABLE "LAPR3_G1"."USERS" ADD UNIQUE ("EMAIL");

--------------------------------------------------------
--  Constraints for Table REQUISITION
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."REQUISITION" ADD CONSTRAINT "FKREQUISITIO587020" CHECK (finalDate>=initialDate) ENABLE;
  ALTER TABLE "LAPR3_G1"."REQUISITION" MODIFY ("USERID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."REQUISITION" MODIFY ("BICYCLEID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."REQUISITION" MODIFY ("PICKUPPARK" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."REQUISITION" MODIFY ("INITIALDATE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."REQUISITION" ADD PRIMARY KEY ("REQUISITIONID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SERVICE
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."SERVICE" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."SERVICE" MODIFY ("UNITARYPRICE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."SERVICE" ADD CHECK (unitaryPrice>0) ENABLE;
  ALTER TABLE "LAPR3_G1"."SERVICE" ADD PRIMARY KEY ("SERVICETYPEID");


--------------------------------------------------------
--  Constraints for Table BICYCLE
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."BICYCLE" MODIFY ("TYPEID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLE" MODIFY ("PARKID" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLE" MODIFY ("WEIGHT" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLE" MODIFY ("AVAILABLE" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."BICYCLE" ADD CHECK (weight>0) ENABLE;
  ALTER TABLE "LAPR3_G1"."BICYCLE" ADD CHECK (available>=0) ENABLE;
  ALTER TABLE "LAPR3_G1"."BICYCLE" ADD PRIMARY KEY ("BICYCLEID")
  USING INDEX  ENABLE;

--------------------------------------------------------
--  Constraints for Table LOTATIONBYTYPE
--------------------------------------------------------

  ALTER TABLE "LAPR3_G1"."LOTATIONBYTYPE" MODIFY ("LOTATION" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."LOTATIONBYTYPE" MODIFY ("MAXLOTATION" NOT NULL ENABLE);
  ALTER TABLE "LAPR3_G1"."LOTATIONBYTYPE" ADD CHECK (LOTATION BETWEEN 0 and MAXLOTATION) ENABLE;
  ALTER TABLE "LAPR3_G1"."LOTATIONBYTYPE" ADD CHECK (MAXLOTATION>=0) ENABLE;
  ALTER TABLE "LAPR3_G1"."LOTATIONBYTYPE" ADD PRIMARY KEY ("TYPEID", "PARKID")
  USING INDEX  ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table LOCATION
--------------------------------------------------------
ALTER TABLE Location ADD CONSTRAINT FKLocation828312 FOREIGN KEY (typeId) REFERENCES LocationType (typeid);

--------------------------------------------------------
--  Ref Constraints for Table WIND
--------------------------------------------------------
ALTER TABLE Wind ADD CONSTRAINT FKWind185780 FOREIGN KEY (finallocationId) REFERENCES Location (locationId);
ALTER TABLE Wind ADD CONSTRAINT FKWind246930 FOREIGN KEY (initiallocationId) REFERENCES Location (locationId);

--------------------------------------------------------
--  Ref Constraints for Table PARK
--------------------------------------------------------
ALTER TABLE Parks ADD CONSTRAINT FKParks279133 FOREIGN KEY (locationId) REFERENCES Location (locationId);

--------------------------------------------------------
--  Ref Constraints for Table POI
--------------------------------------------------------
ALTER TABLE Poi ADD CONSTRAINT FKPoi475089 FOREIGN KEY (locationId) REFERENCES Location (locationId);

--------------------------------------------------------
--  Ref Constraints for Table BICYCLE
--------------------------------------------------------
  ALTER TABLE "LAPR3_G1"."BICYCLE" ADD CONSTRAINT "FKBICYCLE217667" FOREIGN KEY ("TYPEID")
	  REFERENCES "LAPR3_G1"."BICYCLETYPE" ("TYPEID") ENABLE;
  ALTER TABLE "LAPR3_G1"."BICYCLE" ADD CONSTRAINT "FKBICYCLE61405" FOREIGN KEY ("PARKID")
	  REFERENCES "LAPR3_G1"."PARKS" ("PARKID") ENABLE;
      

--------------------------------------------------------
--  Ref Constraints for Table REQUISITION
--------------------------------------------------------
ALTER TABLE Requisition ADD CONSTRAINT FKRequisitio587019 FOREIGN KEY (dropPark) REFERENCES Parks (parkId);
ALTER TABLE Requisition ADD CONSTRAINT FKRequisitio7536 FOREIGN KEY (pickUpPark) REFERENCES Parks (parkId);
ALTER TABLE Requisition ADD CONSTRAINT FKRequisitio760629 FOREIGN KEY (bicycleId) REFERENCES Bicycle (bicycleId);
ALTER TABLE Requisition ADD CONSTRAINT FKRequisitio376899 FOREIGN KEY (userId) REFERENCES Users (userId);

--------------------------------------------------------
--  Ref Constraints for Table LOTATIONBYTYPE
--------------------------------------------------------
ALTER TABLE LOTATIONBYTYPE ADD CONSTRAINT FKRequisitio587120 FOREIGN KEY (parkId) REFERENCES Parks (parkId);
ALTER TABLE LOTATIONBYTYPE ADD CONSTRAINT FKRequisitio587121 FOREIGN KEY (typeId) REFERENCES BicycleType (typeId);

--------------------------------------------------------
--  Ref Constraints for Table PAYMENT
--------------------------------------------------------

ALTER TABLE Payment ADD CONSTRAINT FKPayment467977 FOREIGN KEY ("SERVICETYPE") REFERENCES SERVICE ("SERVICETYPEID");
ALTER TABLE Payment ADD CONSTRAINT FKPayment444666 FOREIGN KEY (userId) REFERENCES Users (userId);



--------------------------------------------------------
--  Usable Inserts
--------------------------------------------------------
insert into bicycletype (typeid, description, workratio) values(1,'eletric',0.5);
insert into bicycletype (typeid, description, workratio) values(2,'other',  0.99);

insert into locationtype(description) values ('PARK');
insert into locationtype(description) values ('POI');