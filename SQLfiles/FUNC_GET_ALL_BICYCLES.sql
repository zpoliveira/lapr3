create or replace FUNCTION FUNC_GET_ALL_BICYCLES

   RETURN SYS_REFCURSOR
AS
    all_Bicycles SYS_REFCURSOR;
BEGIN
    OPEN all_Bicycles FOR 
    SELECT BICYCLE.*, bicycletype.description
    FROM BICYCLE
    left join bicycletype on BICYCLE.typeid = bicycletype.typeid;
    RETURN all_Bicycles;
END FUNC_GET_ALL_BICYCLES;