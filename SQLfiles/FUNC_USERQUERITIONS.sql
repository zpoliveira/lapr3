create or replace function FUNC_USERQUERITIONS(user_id Users.userid%type) 

RETURN SYS_REFCURSOR
AS
    allUserRequisitions SYS_REFCURSOR;
    userId_exceptions   Exception;
    user_count          Number ;
Begin

Select count(*) into user_count from users where userid =userId;

if(user_count = 0) then
     raise userId_exceptions;
end if;

OPEN allUserRequisitions FOR 
    Select r.requisitionid, b.description, r.initialdate, r.finaldate , l1.latitude, l1.longitude, l1.altitude,
            l2.latitude, l2.longitude, l2.altitude
    from bicycle b,requisition r, location l1, location l2, parks p1, parks p2
    where r.userId=user_id and r.bicycleid=b.bicycleId and r.pickuppark = p1.parkid and p1.locationid = l1.locationid and
            r.droppark = p2.parkid and p2.locationid = l2.locationid order by initialDate asc;
return allUserRequisitions;

Exception 
    when userId_exceptions then  RAISE_APPLICATION_ERROR(-20050, 'Invalid user id'); 
end;