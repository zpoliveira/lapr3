create or replace function FUNC_MAX_PARKID

return number

is

v_max number;

begin


select max(parkID) into v_max from Parks;

return v_max;

end;