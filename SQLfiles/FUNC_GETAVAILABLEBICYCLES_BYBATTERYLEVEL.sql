create or replace FUNCTION FUNC_GETAVAILABLEBICYCLES_BYBATTERYLEVEL
(f_parkid PARKS.PARKID%TYPE)

   RETURN SYS_REFCURSOR
AS
    higherBatteryBicycles   SYS_REFCURSOR;
    count_park              number;
    park_exception          exception;
    
BEGIN
    Select count(*) into count_park from Parks where parkid = f_parkid;
    if (count_park = 0 or f_parkid =null) then 
        raise park_exception;
    end if;
        

    OPEN higherBatteryBicycles FOR SELECT * FROM BICYCLE WHERE available = 1 AND TYPEID = 1 AND PARKID=f_parkid ORDER BY batterylevel ASC;
    
    RETURN higherBatteryBicycles;
    
EXCEPTION
    WHEN   park_exception THEN RAISE_APPLICATION_ERROR(-20011, 'Park Does not exist');
END FUNC_GETAVAILABLEBICYCLES_BYBATTERYLEVEL;