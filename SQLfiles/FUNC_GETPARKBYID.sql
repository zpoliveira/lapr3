create or replace FUNCTION FUNC_GETPARKBYID (p_parkID number)
RETURN SYS_REFCURSOR
IS
    cur_allParks  SYS_REFCURSOR;
    v_park_location number;
    ex_parkid_nao_existe EXCEPTION;
    num_park number;


BEGIN

    SELECT COUNT(*) into num_park from parks where parkid = p_parkID;
    
    if 
    num_park = 0 then raise ex_parkid_nao_existe;
    end if;

    SELECT locationid into v_park_location from parks where parkid = p_parkID;

    OPEN cur_allParks FOR 
    
        with x as(
    SELECT  parks.parkid parkid,
                    parks.name name,
                    parks.inputvoltage inputvoltage,
                    parks.inputcurrent inputcurrent,
                    location.locationid locationid,
                    location.latitude latitude,
                    location.longitude longitude,
                    location.altitude altitude,
                    case when lotationbytype.typeid=1
                        then lotationbytype.lotation else 0 end electricalLotation,
                    case when lotationbytype.typeid=1
                        then lotationbytype.maxlotation else 0 end maxElectricalLotation,
                    case when lotationbytype.typeid=2
                        then lotationbytype.lotation else 0 end nonElectricalLotation,
                    case when lotationbytype.typeid=2
                        then lotationbytype.maxlotation else 0 end maxNonElectricalLotation
              FROM parks
              inner join location on parks.locationid = location.locationid
              inner join lotationbytype on parks.locationid =lotationbytype.parkid
              WHERE parks.locationid = p_parkID)
          
            select parkid, name, latitude, longitude, altitude, 
                    inputvoltage, inputcurrent,  sum(maxElectricalLotation), sum(maxNonElectricalLotation), 
                    locationid, sum(electricalLotation),sum(nonElectricalLotation)
            from x
            group by parkid, name, inputvoltage, 
                     inputcurrent, locationid, latitude, longitude, altitude;    
    
    RETURN cur_allParks;
    
    EXCEPTION 
    WHEN ex_parkid_nao_existe THEN DBMS_OUTPUT.PUT_LINE('ParkID does not exist');
    return null;

END;