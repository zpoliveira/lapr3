create or replace function FUNC_MAX_REQID

return number

is

v_max number;

begin


select max(requisitionID) into v_max from requisition;

return v_max;

end;