create or replace PROCEDURE PROC_UPDATEPARK (idPark_new PARKS.PARKID%TYPE,
                                            name_new Parks.name%TYPE,
                                            latitude_new location.latitude%TYPE, 
                                            longitude_new location.longitude%TYPE,
                                            altitude_new location.altitude%TYPE,
                                            electricalLotation_new lotationbytype.lotation%type,
                                            nonElectricalLotation_new lotationbytype.lotation%type,
                                            maxElectricalLotation_new lotationbytype.lotation%type,
                                            maxNonElectricalLotation_new lotationbytype.lotation%type,
                                            inputVoltage_new Parks.inputvoltage%type,
                                            inputCurrent_new Parks.inputcurrent%type)
IS
  s_name        Parks.name%TYPE;
  p_locID       parks.locationid%type;
  V_COUNT       Parks.parkid%TYPE;
  E_EXCEPTION   EXCEPTION;
BEGIN
    SELECT COUNT(*) INTO V_COUNT FROM PARKS WHERE parkid = idpark_new;
    IF V_COUNT=0 or idPark_new = null THEN
        RAISE E_EXCEPTION;
    END IF;
    
    SELECT name INTO s_name FROM Parks WHERE parkid = idPark_new;
    IF(s_name = name_new) THEN
        UPDATE Parks SET name = 'TEMP' WHERE parkid = idPark_new;
        UPDATE Parks SET name = s_name
        WHERE idPark_new=parkid;
        COMMIT;
    ELSE
        UPDATE Parks SET name = name_new
        WHERE idPark_new=parkid;
        COMMIT;
    END IF;
    
    UPDATE Parks SET
        Parks.inputvoltage = inputVoltage_new,
        Parks.inputcurrent = inputCurrent_new
        WHERE idPark_new=parkid;
        commit;
    
    select locationid into p_locID from parks where parkid = idPark_new;
    
    update location set
        latitude=latitude_new, 
        longitude=longitude_new, 
        altitude=altitude_new
    where locationId = p_locID;
    commit;
    
    update lotationbytype set
        lotation = electricalLotation_new,
        maxlotation = maxElectricalLotation_new
    where lotationbytype.parkid = idPark_new and typeid=1;
    commit;
    
    update lotationbytype set
        lotation = nonElectricalLotation_new,
        maxlotation = maxNonElectricalLotation_new
    where lotationbytype.parkid = idPark_new and typeid=2;
    commit;
    
EXCEPTION 
    WHEN E_EXCEPTION THEN RAISE_APPLICATION_ERROR (-20001, 'Park Id does not exists.');
END PROC_UPDATEPARK;