CREATE or REPLACE TRIGGER TRIG_UPDATEUSERSTATE
AFTER INSERT ON payment FOR EACH ROW

DECLARE

BEGIN

    if(:new.servicetype = 1) then 
        UPDATE users Set state = 1 where userid = :new.userid;
    end if;
    
END;