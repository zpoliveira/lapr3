create or replace PROCEDURE PROC_ADDUSER(name_new IN USERS.NAME%TYPE,
                                         nif_new IN USERS.NIF%TYPE,
                                         birthdate_new IN USERS.name%TYPE,
                                         height_new IN USERS.HEIGHT%TYPE,
                                         weigth_new IN USERS.WEIGTH%TYPE,
                                         creditcardnumber_new IN USERS.CREDITCARDNUMBER%TYPE,
                                         email_new IN USERS.EMAIL%TYPE,
                                         averageSpeed_new IN USERS.AVERAGE_SPEED%TYPE)
IS
  V_COUNT               USERS.nif%TYPE;
  V_CHANGED_DATE        USERS.birthdate%TYPE;
  E_EXCEPTION           EXCEPTION;
  
BEGIN

    SELECT COUNT(*) 
    INTO V_COUNT 
    FROM USERS 
    WHERE nif = nif_new;
    
    IF V_COUNT>0 or nif_new = null THEN
        RAISE E_EXCEPTION;
    END IF;
    
    V_CHANGED_DATE:=TO_DATE(birthdate_new);
    INSERT INTO USERS(name, nif, birthdate, userpoints, HEIGHT, WEIGTH, creditcardnumber, email, state, AVERAGE_SPEED)
    VALUES (name_new, nif_new, V_CHANGED_DATE, 0, height_new, weigth_new, creditcardnumber_new, email_new, 0, averageSpeed_new);
    COMMIT;
    dbms_output.put_line('Inserted successfully!');
EXCEPTION 
    WHEN E_EXCEPTION THEN RAISE_APPLICATION_ERROR (-20001, 'NIF is already in use or is invalid.');
END PROC_ADDUSER;