create or replace function FUNC_PAYMENTAMOUT(serviceId service.servicetypeid%type,
                                            reqid requisition.requisitionid%type)

return number is 

    amount                          number(38,2) :=0 ;
    requisitionhours                number(38,2);
    unitaryPrice                    number(38,0);
    
    count_requisitions              int;
    count_services                  int;
    
    servicedescription              Varchar(50 Byte);

    requisitioninitialdate          date;
    requisitionfinadate             date; 
    
    return_date_exception           exception;   
    not_valid_requisition_exception exception;
    not_valid_service_exception     exception;

begin
    

    Select count(*) into count_services   
    from service 
    where serviceTypeId = serviceId;
    
    if(count_services = 0 or serviceId = null) then 
        raise not_valid_service_exception;
    end if;
    
    
    Select count(*) into count_requisitions  
    from requisition
    where requisitionid = reqid;
    
    if(count_requisitions = 0 or reqid = null) then 
        raise not_valid_requisition_exception;
    end if;


    Select description into servicedescription 
    from service 
    where serviceTypeId = serviceId;
    
    Select unitaryprice into amount 
    from service 
    where serviceTypeId = serviceId;

    IF(servicedescription ='Alugar Bicicleta') then
    
        Select finaldate into requisitionfinadate from requisition where requisitionid = reqid;
        
        if(requisitionfinadate = null) then 
            raise return_date_exception;
        end if;
        
        Select initialDate into requisitioninitialdate from requisition where requisitionid = reqid;
        
        requisitionhours := (requisitionfinadate - requisitioninitialdate)*24-1;
        
        if(requisitionhours > 0) then
            amount := requisitionhours * amount;
        end if;
        
    end if;
    
    return amount;

exception 
    when return_date_exception then raise_application_error(-20008, 'The requisition is not close yet');
    when not_valid_requisition_exception THEN RAISE_APPLICATION_ERROR (-20009, 'There is no requisition with that id');
    when not_valid_service_exception then raise_application_Error(-20010, 'There is no service with that id');
end;