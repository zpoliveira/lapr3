create or replace FUNCTION FUNC_GETALLPOI
RETURN SYS_REFCURSOR
IS
    cur_allPoi  SYS_REFCURSOR;

BEGIN

    OPEN cur_allPoi FOR 
        SELECT poi.poiid,poi.name,location.latitude,location.longitude,location.altitude
          FROM poi,location
          where poi.locationid = location.locationid;
    RETURN cur_allPoi;

END;