create or replace function FUNC_MAX_LOCATIONID

return number

is

v_max number;

begin


select max(locationID) into v_max from Location;

return v_max;

end;