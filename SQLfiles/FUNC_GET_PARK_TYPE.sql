create or replace FUNCTION FUNC_GET_PARK_TYPE (p_name locationtype.description%type)
RETURN number
IS
    park_type number(10);

BEGIN
    SELECT typeid into park_type
    FROM locationtype
    where upper(description) like upper(p_name);

    return park_type;
END;