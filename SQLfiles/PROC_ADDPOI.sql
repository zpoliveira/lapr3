create or replace PROCEDURE PROC_ADDPOI(p_type in number, p_name POI.name%type, p_latitude location.latitude%Type,p_longitude location.longitude%type, p_altitude location.altitude%type) is

max_location number(10);

begin

ins_location(p_type, p_latitude,p_longitude,p_altitude);

select max(locationID) into max_location from location;

insert into poi (locationid,name) values (max_location,p_name);

end;