create or replace FUNCTION FUNC_GET_WIND (p_initiallocationId number, p_finallocationId number)
RETURN SYS_REFCURSOR
IS
    cur_wind  SYS_REFCURSOR;

BEGIN

    OPEN cur_wind FOR 
        SELECT wind.INITIALLOCATIONID, wind.FINALLOCATIONID, wind.WINDSPEED, wind.WINDFACTOR,
               wind.AERODYNAMIC_COEFFICIENT, wind.PATH_DIRECTION
          FROM wind
          where wind.INITIALLOCATIONID = p_initiallocationId and wind.FINALLOCATIONID = p_finallocationId;
    RETURN cur_wind;

END;