create or replace PROCEDURE PROC_INS_UPD_WIND(
p_initiallocationId WIND.initiallocationid%type,p_finallocationId WIND.finallocationid%type, p_windspeed WIND.windspeed%type, p_windfactor WIND.windfactor%type
) is
begin
    merge into wind wd using dual on (initiallocationid = p_initiallocationId and finallocationid = p_finallocationId)
         when not matched then insert (initiallocationid, finallocationid, windspeed, windfactor) 
           values (p_initiallocationId, p_finallocationId, p_windspeed, p_windfactor)
         when matched then update set 
           windspeed = p_windspeed, windfactor = p_windfactor;
end PROC_INS_UPD_WIND;