create or replace function FUNC_MAX_BICYCLEID

return number

is

v_max number;

begin


select max(bicycleID) into v_max from Bicycle;

return v_max;

end;