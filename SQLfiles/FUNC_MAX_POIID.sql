create or replace function FUNC_MAX_POIID

return number

is

v_max number;

begin


select max(poiID) into v_max from Poi;

return v_max;

end;