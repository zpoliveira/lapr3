create or replace FUNCTION FUNC_GET_WINDINFO (id_orig IN INTEGER, id_dest IN INTEGER )
RETURN SYS_REFCURSOR
IS
    cur_wind  SYS_REFCURSOR;

BEGIN

    OPEN cur_wind FOR 
        SELECT WINDSPEED, WINDFACTOR
          FROM WIND W
          INNER JOIN PARKS P1 ON P1.LOCATIONID=W.INITIALLOCATIONID
          INNER JOIN PARKS P2 ON P2.LOCATIONID=W.FINALLOCATIONID
          AND P1.PARKID=id_orig and P2.PARKID=id_dest;
    RETURN cur_wind;

END;