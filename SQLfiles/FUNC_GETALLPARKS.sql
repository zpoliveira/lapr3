create or replace FUNCTION FUNC_GETALLPARKS
RETURN SYS_REFCURSOR
IS
    cur_allParks  SYS_REFCURSOR;

BEGIN

    OPEN cur_allParks FOR 
    with x as(
    SELECT  parks.parkid parkid,
                    parks.name name,
                    parks.inputvoltage inputvoltage,
                    parks.inputcurrent inputcurrent,
                    location.locationid locationid,
                    location.latitude latitude,
                    location.longitude longitude,
                    location.altitude altitude,
                    case when lotationbytype.typeid=1
                        then lotationbytype.lotation else 0 end electricalLotation,
                    case when lotationbytype.typeid=1
                        then lotationbytype.maxlotation else 0 end maxElectricalLotation,
                    case when lotationbytype.typeid=2
                        then lotationbytype.lotation else 0 end nonElectricalLotation,
                    case when lotationbytype.typeid=2
                        then lotationbytype.maxlotation else 0 end maxNonElectricalLotation
              FROM parks
              inner join location on parks.locationid = location.locationid
              inner join lotationbytype on parks.locationid =lotationbytype.parkid)
          
    select parkid, name, latitude, longitude, altitude, 
            inputvoltage, inputcurrent, sum(maxElectricalLotation), sum(maxNonElectricalLotation), 
            locationid, sum(electricalLotation),sum(nonElectricalLotation)
    from x
    group by parkid, name, inputvoltage, 
             inputcurrent, locationid, latitude, longitude, altitude;

    RETURN cur_allParks;

END;