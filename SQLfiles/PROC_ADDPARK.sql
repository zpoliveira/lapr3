create or replace PROCEDURE PROC_ADDPARK(p_name Parks.name%type, 
                p_latitude location.latitude%Type,p_longitude location.longitude%type, p_altitude location.altitude%type,
                p_maxelectrical lotationbytype.lotation%Type, p_maxnonelectrical lotationbytype.lotation%Type,
                p_inputvoltage parks.INPUTVOLTAGE%Type, p_inputcurrent parks.INPUTCURRENT%Type) is

max_location number(10);
park_type    number(10);
max_parkid   number(10);

begin

park_type := FUNC_GET_PARK_TYPE('PARK');
ins_location(park_type, p_latitude,p_longitude,p_altitude);

select max(locationID) into max_location from location;

insert into parks (locationid,name, INPUTVOLTAGE, INPUTCURRENT) values (max_location,p_name,p_inputvoltage,p_inputcurrent);

max_parkid := func_max_parkid();

INSERT INTO LOTATIONBYTYPE VALUES(max_parkid, 1, 0, p_maxelectrical);
INSERT INTO LOTATIONBYTYPE VALUES(max_parkid, 2, 0, p_maxnonelectrical);

end;