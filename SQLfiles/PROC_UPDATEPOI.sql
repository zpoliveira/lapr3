create or replace PROCEDURE PROC_UPDATEPOI(idPoi_new POI.POIID%TYPE,
                                            name_new Poi.name%TYPE,
                                            latitude_new location.latitude%TYPE, 
                                            longitude_new location.longitude%TYPE,
                                            altitude_new location.altitude%TYPE)
IS
  s_name        Poi.name%TYPE;
  p_locID       poi.locationid%type;
  V_COUNT       Poi.poiid%TYPE;
  E_EXCEPTION   EXCEPTION;
BEGIN
    SELECT COUNT(*) INTO V_COUNT FROM POI WHERE poiid = idpoi_new;
    IF V_COUNT=0 or idPoi_new = null THEN
        RAISE E_EXCEPTION;
    END IF;

    SELECT name INTO s_name FROM Poi WHERE poiid = idPoi_new;
    IF(s_name = name_new) THEN
        UPDATE Poi SET name = 'TEMP' WHERE poiid = idPoi_new;
        UPDATE Poi SET name = s_name
        WHERE idPoi_new=poiid;
        COMMIT;
    ELSE
        UPDATE Poi SET name = name_new
        WHERE idPoi_new=poiid;
        COMMIT;
    END IF;

    select locationid into p_locID from poi where poiid = idPoi_new;

    update location set
        latitude=latitude_new, 
        longitude=longitude_new, 
        altitude=altitude_new
    where locationId = p_locID;
    commit;


EXCEPTION 
    WHEN E_EXCEPTION THEN RAISE_APPLICATION_ERROR (-20001, 'Poi Id does not exists.');
END PROC_UPDATEPOI;